package by.training.bnm.basics.linear.task8;

import by.training.bnm.basics.linear.task.Task;

import java.util.Scanner;

public class Task8 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n8. Вычислить значение выражения по формуле (все переменные" +
                " принимают действительные значения):" +
        "((b + Math.sqrt(b * b + 4 * a * c))/(2 * a) - a * a * a * c + 1 / (b * b))");
    }

    public void runTask() {
        double a = 0;
        double b = 0;
        double c = 0;
        double result = 0;
        Scanner in = new Scanner(System.in);

        System.out.println("Calculate: " +
                "((b + Math.sqrt(b * b + 4 * a * c))/(2 * a) - a * a * a * c + 1 / (b * b))");

        System.out.print("Enter a: ");
        a = in.nextDouble();
        System.out.print("Enter b: ");
        b = in.nextDouble();
        System.out.print("Enter c: ");
        c = in.nextDouble();

        result = ((b + Math.sqrt(b * b + 4 * a * c))/(2 * a) - a * a * a * c + 1 / (b * b));
        System.out.println("Result: " + result);
    }
}
