package by.training.bnm.arrays.arraysimple;

import java.util.Arrays;
import java.util.Scanner;

public class Task11 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n11. Даны натуральные числа а1, а2, ..., аn. Указать те из них,\n" +
                " у которых остаток от деления на М равен L (0 < L < М-1).");
    }

    public void runTask() {
        int[] a = Task1.createArrayNatural();
        int m = 0;
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter M: ");
        m = scanner.nextInt();

        System.out.println("Array is: " + Arrays.toString(a));

        System.out.print("New array is: [");
        for (int i = 0; i < a.length; i++) {
            if (a[i] % m > 0 && a[i] % m < (m - 1))
                if (i == a.length - 1) {
                    System.out.print(a[i]);
                    break;
                }
                System.out.print(a[i] + ", ");
        }
        System.out.println("]");

    }
}
