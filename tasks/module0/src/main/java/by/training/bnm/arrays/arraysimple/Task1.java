package by.training.bnm.arrays.arraysimple;

import java.util.Arrays;
import java.util.Scanner;

public class Task1 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n1. В массив A [N] занесены натуральные числа. Найти сумму тех элементов" +
                ", которые кратны данному К.");
    }

    public void runTask() {
        int[] a = createArrayNatural();

        int k = 0;
        int sum = 0;

        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter K: ");
        k = scanner.nextInt();

        for (int i = 0; i < a.length; i++) {
            if (a[i] % k == 0)
                sum += a[i];
        }

        System.out.println("Array: " + Arrays.toString(a));

        System.out.println("Sum = " + sum);
    }

    public static int[] createArrayNatural() {
        int max = 0;
        int n = 0;
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter max number for array: ");
        max = scanner.nextInt();
        System.out.print("Enter size of array: ");
        n = scanner.nextInt();

        int[] a = new int[n];
        // заполнение массива
        for (int i = 0; i < n; i++) {
            a[i] = (int)(Math.random() * max);
        }

        return a;
    }

    public static int[] createArray() {
        int max = 0;
        int n = 0;
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter max number for array: ");
        max = scanner.nextInt();
        max *= 2;
        System.out.print("Enter size of array: ");
        n = scanner.nextInt();

        int[] a = new int[n];
        // заполнение массива
        for (int i = 0; i < n; i++) {
            a[i] = (int)(Math.random() * max) - max / 2;
        }

        return a;
    }
}
