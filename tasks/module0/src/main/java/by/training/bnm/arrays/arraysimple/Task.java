package by.training.bnm.arrays.arraysimple;

public interface Task {
    public void showTask();
    public void runTask();
}
