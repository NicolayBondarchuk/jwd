package by.training.bnm.oop.basics.task1.service;

import by.training.bnm.oop.basics.task1.model.File;

public class ChangeText implements ActionText {

    File file;

    public ChangeText() {
    }

    public ChangeText(File file) {
        this.file = file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    @Override
    public boolean addText(String text) {
        if (file.isReadOnly()) {
            return false;
        }
        StringBuilder stringBuilder;
        if (file.getContent() == null) {
            stringBuilder = new StringBuilder();
        } else {
            stringBuilder = new StringBuilder(file.getContent());
        }
        String string = new String(stringBuilder.append(text));
        file.setContent(string);
        return true;
    }

    @Override
    public void showText() {
        System.out.println(file.getName() + ":\n" + file.getContent());
    }
}
