package by.training.bnm.decomposition;

import java.util.Scanner;

public class Task1 implements Task {

    @Override
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n1. Треугольник задан координатами своих вершин. Написать метод " +
                "для вычисления его площади.");
    }

    @Override
    public void runTask() {
        Scanner scanner = new Scanner(System.in);
        Point[] points = new Point[3];

        double x = 0, y = 0, z = 0;
        // enter coordinates
        for (int i = 0; i < 3; i++) {
            System.out.println("Enter point " + (i + 1) + ": ");
            System.out.print("x: ");
            x = scanner.nextDouble();
            System.out.print("y: ");
            y = scanner.nextDouble();
            System.out.print("z: ");
            z = scanner.nextDouble();
            points[i] = new Point(x, y, z);
        }

        // вычисляем длину каждой стороны
        double a = calcLength(points[0], points[1]);
        double b = calcLength(points[1], points[2]);
        double c = calcLength(points[0], points[2]);

        // вычисляем площадь треугольника
        double area = calcTriangleArea(a, b, c);
        System.out.println("Area of triangle is " + area);

    }

    public static class Point {
        private double x;
        private double y;
        private double z;

        public Point(double x, double y, double z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public double getX() {
            return x;
        }

        public double getY() {
            return y;
        }

        public double getZ() {
            return z;
        }

        @Override
        public String toString() {
            return "{" +
                     x +
                    ", " + y +
                    ", " + z +
                    '}';
        }
    }

    // вычисление длины прямой между двумя точками
    public static double calcLength(Point point1, Point point2) {
        double dx, dy, dz;
        dx = (point1.x - point2.x);
        dy = (point1.y - point2.y);
        dz = (point1.z - point2.z);
        double sum = Math.pow(dx, 2) + Math.pow(dy, 2) + Math.pow(dz, 2);
        double length = Math.sqrt(sum);
        return length;
    }

    // метод вычисления площади треугольника по формуле Герона
    public static double calcTriangleArea(double a, double b, double c) {
        double halfPerimeter = 0;
        halfPerimeter = (a + b + c) / 2;
        double multiple = halfPerimeter * (halfPerimeter - a) * (halfPerimeter - b) *
                (halfPerimeter - c);
        double area = Math.sqrt(multiple);
        return area;
    }
}
