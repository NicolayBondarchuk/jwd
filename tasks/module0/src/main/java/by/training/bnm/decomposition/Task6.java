package by.training.bnm.decomposition;

import java.util.Scanner;

public class Task6 implements Task {
    @Override
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n6. Вычислить площадь правильного шестиугольника со стороной а, " +
                "используя метод вычисления площади\n треугольника.");
    }

    @Override
    public void runTask() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a: ");
        double a = scanner.nextDouble();

        // используем метод нахождения площади треугольника из Task1
        Task1 task1 = new Task1();
        double triangleArea = task1.calcTriangleArea(a, a, a);

        // находим площадь шестиугольника как сумму шести площадей треугольников
        double hexArea = 6 * triangleArea;

        System.out.println("The area of hexagon is " + hexArea);
    }

}
