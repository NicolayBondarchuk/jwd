package by.training.bnm.basics.linear.task9;

import by.training.bnm.basics.linear.task.Task;

import java.util.Scanner;

public class Task9 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n9. Вычислить значение выражения по формуле " +
                "(все переменные принимают действительные значения):" +
                "(a / c) * (b / d) - (a * b - c) / (c * d)");
    }

    public void runTask() {
        double a = 0;
        double b = 0;
        double c = 0;
        double d = 0;
        double result = 0;
        Scanner in = new Scanner(System.in);

        System.out.print("Enter a: ");
        a = in.nextDouble();
        System.out.print("Enter b: ");
        b = in.nextDouble();
        System.out.print("Enter c: ");
        c = in.nextDouble();
        System.out.print("Enter d: ");
        d = in.nextDouble();

        result = ((a / c) * (b / d) - (a * b - c) / (c * d));
        System.out.println("Result: " + result);
    }
}
