package by.training.bnm.oop.simple.task4.group;

import by.training.bnm.oop.simple.task4.data.Train;

public class Group {
    private Train[] trains;

    public Group() {
    }

    public Group(Train[] trains) {
        this.trains = trains;
    }

    public Train[] getTrains() {
        return trains;
    }

    public Train getTrains(int element) {
        return trains[element];
    }

    public void setTrains(Train[] trains) {
        this.trains = trains;
    }

    public void setTrains(int element, Train trains) {
        this.trains[element] = trains;
    }
}
