package by.training.bnm.arrays.sort;

import java.util.Arrays;
import java.util.Scanner;

public class Task2 implements Task {
    @Override
    public void showTask() {
        System.out.println("-----------------------------------------------");
        System.out.println("\n2. Даны две последовательности a1 <= a2 <= ... <= an и b1 <= b2 <= ... <= bm. " +
                "Образовать из них новую последовательность чисел\n" +
                "так, чтобы она тоже была неубывающей. Примечание. Дополнительный массив не использовать.");
    }

    @Override
    public void runTask() {
        int n = 0;
        int m = 0;
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter n: ");
        n = scanner.nextInt();
        System.out.print("Enter m: ");
        m = scanner.nextInt();

        int[] a = new int[n];
        int[] b = new int[m];
        int[] c = new int[n + m];

        // заполнение массива a[]
        for (int i = 1; i < n; i++) {
            a[i] = i * 10 + i;
        }
        System.out.println("Array a[]: " + Arrays.toString(a));

        // заполнение массива b[]
        for (int i = 1; i < m; i++) {
            b[i] = i * 10 - 3 * i;
        }
        System.out.println("Array b[]: " + Arrays.toString(b));

        int j = 0;
        int k = 0;
        for (int i = 0; i < n + m; i++) {
            if (j >= n){
                c[i] = b[k];
                k++;
            } else if (k >= m) {
                c[i] = a[j];
                j++;
            } else if (a[j] <= b[k]){
                c[i] = a[j];
                j++;
            } else if (b[k] < a[j]){
                c[i] = b[k];
                k++;
            }
        }
        System.out.println("Array c[]: " + Arrays.toString(c));

    }
}
