package by.training.bnm.basics.branching;

import java.util.Scanner;

public class Task13 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n13. Даны две точки А(х1, у1) и В(х2, у2). Составить алгоритм," +
                " определяющий, которая из точек находится ближе к началу координат.");
    }

    public void runTask() {
        double x1 = 0;
        double y1 = 0;
        double x2 = 0;
        double y2 = 0;
        double dictanceA = 0;
        double dictanceB = 0;
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter x1: ");
        x1 = scanner.nextDouble();
        System.out.print("Enter y1: ");
        y1 = scanner.nextDouble();
        System.out.print("Enter x2: ");
        x2 = scanner.nextDouble();
        System.out.print("Enter y2: ");
        y2 = scanner.nextDouble();

        dictanceA = Math.sqrt(x1 * x1 + y1 * y1);
        dictanceB = Math.sqrt(x2 * x2 + y2 * y2);

        if (dictanceA < dictanceB)
            System.out.println("Point A is closer than point B");
        else if (dictanceA > dictanceB)
            System.out.println("Point B is closer than point A");
    }
}
