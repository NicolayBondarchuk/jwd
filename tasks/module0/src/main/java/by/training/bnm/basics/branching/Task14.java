package by.training.bnm.basics.branching;

import java.util.Scanner;

public class Task14 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n14. Даны два угла треугольника (в градусах). Определить, " +
                "существует ли такой треугольник, и если да, то будет ли он " +
                "прямоугольным.");

    }

    public void runTask() {
        double degree1 = 0;
        double degree2 = 0;
        double sumDegree = 0;
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter degree 1: ");
        degree1 = scanner.nextDouble();
        System.out.print("Enter degree 2: ");
        degree2 = scanner.nextDouble();

        sumDegree = degree1 + degree2;
        if (sumDegree < 180) {
            System.out.println("This triangle can exist.");
            if (sumDegree == 90 || degree1 == 90 || degree2 == 90)
                System.out.println("And that is a right triangle.");
        }
    }
}
