package by.training.bnm.basics.branching;

import java.util.Scanner;

public class Task9 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n9. Составить программу, которая определит " +
                "по трем введенным сторонам, является ли данный треугольник " +
                "равносторонним.");
    }

    public void runTask() {
        int a = 0;
        int b = 0;
        int c = 0;
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter side a: ");
        a = scanner.nextInt();
        System.out.print("Enter side b: ");
        b = scanner.nextInt();
        System.out.print("Enter side c: ");
        c = scanner.nextInt();

        if (a == b && b == c)
            System.out.println("triangle is equilateral");
        else
            System.out.println("triangle is not equilateral");
    }
}
