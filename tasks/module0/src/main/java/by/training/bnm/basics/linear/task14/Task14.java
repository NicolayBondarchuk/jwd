package by.training.bnm.basics.linear.task14;

import by.training.bnm.basics.linear.task.Task;

import java.util.Scanner;

/*
14. Вычислить длину окружности и площадь круга одного и того же заданного радиуса R.
 */
public class Task14 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n14. Вычислить длину окружности и площадь круга одного " +
                "и того же заданного радиуса R.");
    }

    public void runTask() {
        int R = 0;
        Scanner in = new Scanner(System.in);

        System.out.print("Enter radius: ");
        R = in.nextInt();

        System.out.println("Circumference: " + (2 * Math.PI * R));
        System.out.println("Area: " + (Math.PI * R * R));

    }
}
