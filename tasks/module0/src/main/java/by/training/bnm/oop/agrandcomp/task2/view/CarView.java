package by.training.bnm.oop.agrandcomp.task2.view;

import by.training.bnm.oop.agrandcomp.task2.data.Car;

public class CarView {
    private Car car;

    public CarView() {
    }

    public CarView(Car car) {
        this.car = car;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    // вывод марки автомобиля
    public void showBrand() {
        System.out.print("The brand of car is '");
        System.out.print(car.getBrand());
        System.out.println("'.");
    }
}
