package by.training.bnm.basics.cycles;


public class Task1 implements Task {

    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n1. Необходимо вывести на экран числа от 1 до 5.");
    }

    public void runTask() {
        for (int i = 0; i < 5; i++) {
            System.out.println((i + 1));
        }
    }
}
