package by.training.bnm.basics.cycles;


public class Task4 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n4. С помощью оператора while напишите программу вывода " +
                "всех четных чисел в диапазоне от 2 до 100\n" +
                "включительно.");
    }

    public void runTask() {
        int i = 2;
        while (i <= 100) {
            System.out.print(i + " ");
            i += 2;
        }
        System.out.println();
    }
}
