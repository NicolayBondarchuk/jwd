package by.training.bnm.decomposition;

import java.util.Scanner;

public class Task9 implements Task {
    @Override
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n9. Написать метод(методы), проверяющий, являются ли данные три числа " +
                "взаимно простыми.");
    }

    @Override
    public void runTask() {
        int size = 3;
        int[] numbers = new int[size];
        Scanner scanner = new Scanner(System.in);

        for (int i = 0; i < size; i++) {
            System.out.print("Enter number " + (i + 1) + ": ");
            numbers[i] = scanner.nextInt();
        }

        if (isCoprimeNumbers(numbers))
            System.out.println("This numbers is coprime");
        else
            System.out.println("This numbers isn't coprime");

    }

    public boolean isCoprimeNumbers(int[] numbers) {
        for (int i = 0; i < (numbers.length - 1); i++) {
            for (int j = i + 1; j < numbers.length; j++) {
                // используем статический метод NOD из Task2
                if (Task2.NOD(numbers[i], numbers[j]) != 1)
                    return false;
            }
        }
        return true;
    }
}
