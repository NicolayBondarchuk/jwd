package by.training.bnm.oop.basics.task1.controller;

import by.training.bnm.oop.basics.task1.service.ActionFile;

public class RemoveFileCommand implements FileCommand {

    ActionFile actionFile;

    public RemoveFileCommand(ActionFile actionFile) {
        this.actionFile = actionFile;
    }

    @Override
    public void execute(String name) {
        actionFile.remove(name);
    }
}
