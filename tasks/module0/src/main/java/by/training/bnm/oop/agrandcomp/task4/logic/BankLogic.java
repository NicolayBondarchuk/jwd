package by.training.bnm.oop.agrandcomp.task4.logic;

import by.training.bnm.oop.agrandcomp.task4.data.Account;
import by.training.bnm.oop.agrandcomp.task4.group.Bank;

public class BankLogic {
    Bank bank;

    public BankLogic() {
    }

    public BankLogic(Bank bank) {
        this.bank = bank;
    }

    // поиск всех счетов данного клиента
    public Account[] searchName(String customerName) {
        int count = 0;
        // ищем все счета с данным именем клиента
        for (int i = 0; i < bank.getAccounts().length; i++) {
            if (customerName.equals(bank.getAccount(i).getCustomerName())) {
                count++;
            }
        }
        if (count == 0)
            return null;
        // создаем и заполняем массив счетов данного клиента
        Account[] array = new Account[count];
        int j = 0;
        for (int i = 0; i < bank.getAccounts().length; i++) {
            if (bank.getAccount(i).getCustomerName().equals(customerName)) {
                array[j] = bank.getAccount(i);
                j++;
            }
        }
        return array;
    }

    // поиск по номеру счета
    public Account searchAccountNumber(String accountNumber) {
        for (Account account : bank.getAccounts()) {
            if (account.getAccountNumber().equals(accountNumber))
                return account;
        }
        return null;
    }

    // поиск заблокированных счетов
    public Account[] searchBlockedAccount() {
        int count = 0;
        for (Account account : bank.getAccounts()) {
            if (account.isBlockAccount())
                count++;
        }
        if (count == 0)
            return null;
        // создаем и заполняем массив заблокированных счетов
        Account[] array = new Account[count];
        int j = 0;
        for (Account account : bank.getAccounts()) {
            if (account.isBlockAccount()) {
                array[j] = account;
                j++;
            }
        }
        return array;
    }

    // поиск незаблокированных счетов
    public Account[] searchNotBlockedAccount() {
        int count = 0;
        for (Account account : bank.getAccounts()) {
            if (!account.isBlockAccount())
                count++;
        }
        if (count == 0)
            return null;
        // создаем и заполняем массив заблокированных счетов
        Account[] array = new Account[count];
        int j = 0;
        for (Account account : bank.getAccounts()) {
            if (!account.isBlockAccount()) {
                array[j] = account;
                j++;
            }
        }
        return array;
    }

    // сортировка счетов по именам клиентов
    public void sortCustomerNames() {
        int j = 0;
        Account temp;
        int n = bank.getAccounts().length;
        int c = 0; // переменная для перехода к следующему символу
        // реализация сортировки Шелла
        while (j < n - 1) {
            if (!bank.getAccount(j).getCustomerName().equals(bank.getAccount(j + 1).getCustomerName())) {
                if (bank.getAccount(j).getCustomerName().charAt(c) >
                        bank.getAccount(j + 1).getCustomerName().charAt(c)) {
                    temp = bank.getAccount(j);
                    bank.setAccount(bank.getAccount(j + 1), j);
                    bank.setAccount(temp,j + 1);
                    if (j > 0) {
                        j--;
                        c = 0;
                    } else {
                        j++;
                        c = 0;
                    }
                    // если символы совпадают, сравниваем следующие
                } else if (bank.getAccount(j).getCustomerName().charAt(c) ==
                        bank.getAccount(j + 1).getCustomerName().charAt(c)) {
                    c++;
                } else {
                    j++;
                    c = 0;
                }
            } else {
                j++;
                c = 0;
            }
        }
    }

    // сортировка счетов по номеру счета
    public void sortAccountNumber() {
        int j = 0;
        Account temp;
        int n = bank.getAccounts().length;
        int c = 0; // переменная для перехода к следующему символу
        // реализация сортировки Шелла
        while (j < n - 1) {
            if (bank.getAccount(j).getAccountNumber().charAt(c) >
                    bank.getAccount(j + 1).getAccountNumber().charAt(c)) {
                temp = bank.getAccount(j);
                bank.setAccount(bank.getAccount(j + 1), j);
                bank.setAccount(temp, j + 1);
                if (j > 0) {
                    j--;
                    c = 0;
                } else {
                    j++;
                    c = 0;
                }
                // если символы совпадают, сравниваем следующие
            } else if (bank.getAccount(j).getAccountNumber().charAt(c) ==
                    bank.getAccount(j + 1).getAccountNumber().charAt(c)) {
                c++;
            } else {
                j++;
                c = 0;
            }
        }
    }

    // сортировка счетов по сумме, находящейся на счету
    public void sortAmount() {
        int j = 0;
        Account temp;
        int n = bank.getAccounts().length;
        // реализация сортировки Шелла
        while (j < n - 1) {
            if (bank.getAccount(j).getAmount() >
                    bank.getAccount(j + 1).getAmount()) {
                temp = bank.getAccount(j);
                bank.setAccount(bank.getAccount(j + 1), j);
                bank.setAccount(temp, j + 1);
                if (j > 0) {
                    j--;
                } else {
                    j++;
                }
            } else {
                j++;
            }
        }
    }

    // вычисление общей суммы по счетам
    public int calcBalance() {
        int sum = 0;
        for (Account account : bank.getAccounts()) {
            sum += account.getAmount();
        }
        return sum;
    }

    // вычисление общей суммы по счетам, имеющим отрицательный баланс
    public int calcNegativeBalance() {
        int sum = 0;
        for (Account account : bank.getAccounts()) {
            if (account.getAmount() < 0)
                sum += account.getAmount();
        }
        return sum;
    }

    // вычисление общей суммы по счетам, имеющим положительный баланс
    public int calcPositiveBalance() {
        int sum = 0;
        for (Account account : bank.getAccounts()) {
            if (account.getAmount() > 0)
                sum += account.getAmount();
        }
        return sum;
    }

    // вычисление суммы по любым набором счетов если счет не блокирован
    public int calcSomeBalance(Account[] accounts) {
        int sum = 0;
        for (Account account : accounts) {
            if (!account.isBlockAccount())
                sum += account.getAmount();
        }
        return sum;
    }
}
