package by.training.bnm.arrays.sort;

public class Main {

    private Task[] task;

    public Main() {
        task = new Task[19];
        task[0] = new Task1();
        task[1] = new Task2();
        task[2] = new Task3();
        task[3] = new Task4();
        task[4] = new Task5();
        task[5] = new Task6();
        task[6] = new Task7();
    }

    public Task[] getTask() {
        return task;
    }

    public Task getTask(int index) {
        return task[index];
    }

    public static void main(String[] args) {
        Main main = new Main();
        for (int i = 0; i < main.task.length; i++) {
            main.getTask(i).showTask();
            main.getTask(i).runTask();
        }

    }
}
