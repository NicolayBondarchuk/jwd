package by.training.bnm.oop.simple.task9.logic;

import by.training.bnm.oop.simple.task9.group.Library;

public class LibraryLogic {

    public int[] getId(Library library) {
        int[] array = new int[library.getLength()];
        for (int i = 0; i < array.length; i++) {
            array[i] = library.getBook(i).getId();
        }
        return array;
    }

    public String[] getTitle(Library library) {
        String[] array = new String[library.getLength()];
        for (int i = 0; i < array.length; i++) {
            array[i] = library.getBook(i).getTitle();
        }
        return array;
    }

    public String[] getAuthor(Library library) {
        String[] array = new String[library.getLength()];
        for (int i = 0; i < library.getLength(); i++) {
            array[i] = library.getBook(i).getAuthor();
        }
        return array;
    }

    public String[] getPublishingHouse(Library library) {
        String[] array = new String[library.getLength()];
        for (int i = 0; i < array.length; i++) {
            array[i] = library.getBook(i).getPublishingHouse();
        }
        return array;
    }

    public int[] getYear(Library library) {
        int[] array = new int[library.getLength()];
        for (int i = 0; i < array.length; i++) {
            array[i] = library.getBook(i).getYear();
        }
        return array;
    }

    public int[] getNumberOfPages(Library library) {
        int[] array = new int[library.getLength()];
        for (int i = 0; i < array.length; i++) {
            array[i] = library.getBook(i).getNumberOfPages();
        }
        return array;
    }

    public double[] getPrice(Library library) {
        double[] array = new double[library.getLength()];
        for (int i = 0; i < array.length; i++) {
            array[i] = library.getBook(i).getPrice();
        }
        return array;
    }

    public String[] getBinding(Library library) {
        String[] array = new String[library.getLength()];
        for (int i = 0; i < array.length; i++) {
            array[i] = library.getBook(i).getBinding();
        }
        return array;
    }

    // вывод массива названий книг по имени автора
    public String[] getBookOfAuthor(String author, Library library) {
        int count = 0;
        for (int i = 0; i < library.getLength(); i++) {
            if (author.equals(library.getBook(i).getAuthor()))
                count++;
        }
        String[] array = new String[count];
        int j = 0;
        for (int i = 0; i < library.getLength(); i++) {
            if (author.equals(library.getBook(i).getAuthor())) {
                array[j] = library.getBook(i).getTitle();
                j++;
            }
        }
        return array;
    }

    // вывод массива названий книг по названию издательства
    public String[] getBooksOfPublish(String publishHouse, Library library) {
        int count = 0;
        for (int i = 0; i < library.getLength(); i++) {
            if (publishHouse.equals(library.getBook(i).getPublishingHouse()))
                count++;
        }
        String[] array = new String[count];
        int j = 0;
        for (int i = 0; i < library.getLength(); i++) {
            if (publishHouse.equals(library.getBook(i).getPublishingHouse())) {
                array[j] = library.getBook(i).getTitle();
                j++;
            }
        }
        return array;
    }

    // вывод массива названий книг выпущенных после года "year"
    public String[] getBooksAfterYear(int year, Library library) {
        int count = 0;
        for (int i = 0; i < library.getLength(); i++) {
            if (year < library.getBook(i).getYear())
                count++;
        }
        String[] array = new String[count];
        int j = 0;
        for (int i = 0; i < library.getLength(); i++) {
            if (year < library.getBook(i).getYear()) {
                array[j] = library.getBook(i).getTitle();
                j++;
            }
        }
        return array;
    }
}
