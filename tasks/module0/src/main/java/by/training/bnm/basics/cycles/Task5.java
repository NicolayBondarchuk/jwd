package by.training.bnm.basics.cycles;


public class Task5 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n5. С помощью оператора while напишите программу определения " +
                "суммы всех нечетных чисел в\n" + "диапазоне от 1 до 99 включительно.");
    }

    public void runTask() {
        int sum = 0;
        int i = 1;
        while (i < 100) {
            sum += i;
            i += 2;
        }
        System.out.println("sum = " + sum);
    }
}
