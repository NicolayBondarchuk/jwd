package by.training.bnm.arrays.arraysimple;

import java.util.Arrays;
import java.util.Scanner;

public class Task12 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n12. Задана последовательность N вещественных чисел. Вычислить сумму\n" +
                "чисел, порядковые номера которых являются простыми числами.");

    }

    public void runTask() {
        int[] a = Task1.createArrayNatural();
        int sum = 0;
        Scanner scanner = new Scanner(System.in);

        System.out.println("Array is: " + Arrays.toString(a));

        System.out.print("New array is: [");
        for (int i = 0; i < a.length; i++) {
            if (isPrime(a[i])) {
                sum += a[i];
                if (i == a.length - 1)
                    System.out.print(a[i]);
                System.out.print(a[i] + ", ");
            }
        }
        System.out.println("]");

        System.out.println("Sum = " + sum);

    }

    public boolean isPrime(int number) {
        for (int i = 2; i < number; i++) {
            if (number % i == 0)
                return false;
        }
        return true;
    }
}
