package by.training.bnm.basics.cycles;

import java.util.Scanner;

public class Task14 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n21. Составить программу для вычисления значений функции F(x) на отрезке [а, b] с шагом h. Результат\n" +
                "представить в виде таблицы, первый столбец которой – значения аргумента, второй - соответствующие\n" +
                "значения функции:\n" +
                "F (x) = x − sin(x)");
    }

    public void runTask() {
        double a = 0;
        double b = 0;
        double h = 0;
        double F = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a: ");
        a = scanner.nextInt();
        System.out.print("Enter b: ");
        b = scanner.nextInt();
        System.out.print("Enter h: ");
        h = scanner.nextInt();

        while (a < b) {
            F = a - Math.sin(a);
            System.out.println("|\t" + a + "\t|\t" + F + "\t|");
            a += 1.0;
        }
    }
}
