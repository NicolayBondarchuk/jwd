package by.training.bnm.basics.linear.task12;

import by.training.bnm.basics.linear.task.Task;

import java.util.Scanner;

/*
12. Вычислить расстояние между двумя точками с данными координатами (х1, у1)и (x2, у2).
 */
public class Task12 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n12. Вычислить расстояние между двумя точками " +
                "с данными координатами (х1, у1)и (x2, у2).");
    }

    public void runTask() {
        double x1 = 0;
        double y1 = 0;
        double x2 = 0;
        double y2 = 0;
        double distance = 0;
        Scanner in = new Scanner(System.in);

        System.out.println("Enter coordinates:");

        System.out.print("Enter x1: ");
        x1 = in.nextDouble();
        System.out.print("Enter y1: ");
        y1 = in.nextDouble();
        System.out.print("Enter x2: ");
        x2 = in.nextDouble();
        System.out.print("Enter y2: ");
        y2 = in.nextDouble();

        distance = (Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)));
        System.out.println("Distance is " + distance);
    }
}
