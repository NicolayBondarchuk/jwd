package by.training.bnm.basics.cycles;


public class Task9 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n9. Найти сумму квадратов первых ста чисел.");
    }

    public void runTask() {
        int max = 100;
        int i = 0;
        int sum = 0;

        while (i <= 100) {
            sum += i * i;
            i++;
        }
        System.out.println("Sum = " + sum);
    }
}
