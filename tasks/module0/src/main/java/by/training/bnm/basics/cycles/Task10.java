package by.training.bnm.basics.cycles;


import java.math.BigDecimal;

public class Task10 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n10. Составить программу нахождения произведения квадратов первых двухсот чисел.");
    }

    public void runTask() {
        BigDecimal mul = new BigDecimal(1);
        int max = 200;
        int i = 1;

        while (i <= 200) {
            mul = mul.multiply(new BigDecimal(i * i ));
            i++;
        }
        System.out.println("Multiply = " + mul);
    }
}
