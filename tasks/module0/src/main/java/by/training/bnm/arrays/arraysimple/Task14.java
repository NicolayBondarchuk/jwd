package by.training.bnm.arrays.arraysimple;

import java.util.Arrays;

public class Task14 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n14. Дан одномерный массив A[N]. Найти:\n" +
                "max( a2 , a4 , ... , a2k ) + min( a1 , a3 , ..., a2k + 1 )");
    }

    public void runTask() {
        int max = Integer.MIN_VALUE;
        int min = Integer.MAX_VALUE;

        int[] a = Task1.createArray();
        System.out.println("A:" + Arrays.toString(a));

        for (int i = 0; i < a.length; i++) {
            if (a[i] % 2 == 0) {
                if (max < a[i])
                    max = a[i];
            } else {
                if (min > a[i])
                    min = a[i];
            }
        }

        System.out.println("max of even elements is " + max);
        System.out.println("min of odd elements is " + min);


    }
}
