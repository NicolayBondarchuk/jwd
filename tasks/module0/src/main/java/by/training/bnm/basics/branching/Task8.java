package by.training.bnm.basics.branching;

import java.util.Scanner;

public class Task8 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n8. Составить программу нахождения наименьшего из квадратов двух чисел а и b.");
    }

    public void runTask() {
        double a = 0;
        double b = 0;
        double a2 = 0;
        double b2 = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a: ");
        a = scanner.nextDouble();
        System.out.print("Enter b: ");
        b = scanner.nextDouble();

        a2 = a * a;
        b2 = b * b;

        if (a2 < b2)
            System.out.println(a + " has the smallest of the squarest");
        else if (a2 > b2)
            System.out.println(b + " has the smallest of the squarest");
        else
            System.out.println(a + " == " + b);


    }

}
