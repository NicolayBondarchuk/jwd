package by.training.bnm.basics.cycles;

import java.math.BigDecimal;

public class Task13 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n16. Вычислить: (1+2)*(1+2+3)*...*(1+2+...+10).");
    }

    public void runTask() {
        int max = 10;
        int i = 2;
        BigDecimal mul = new BigDecimal(1);

        while (i <= max) {
            int j = 0;
            int sum = 0;
            while (j < i) {
                sum += j;
                j++;
            }
            mul = mul.multiply(new BigDecimal(sum));
            i++;
        }

        System.out.println("Mul = " + mul);
    }
}
