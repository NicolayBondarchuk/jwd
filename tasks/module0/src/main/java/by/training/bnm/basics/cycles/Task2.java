package by.training.bnm.basics.cycles;


public class Task2 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n2. Необходимо вывести на экран числа от 5 до 1.");
    }

    public void runTask() {
        for (int i = 5; i > 0; i--) {
            System.out.println((i));
        }
    }
}
