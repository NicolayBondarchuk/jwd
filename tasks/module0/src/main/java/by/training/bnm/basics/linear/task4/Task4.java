package by.training.bnm.basics.linear.task4;

/*
 Найдите значение функции: z = ( (a – 3 ) * b / 2) + c.
 */

import by.training.bnm.basics.linear.task.Task;

import java.util.Scanner;

public class Task4 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\nНайдите значение функции: z = ( (a – 3 ) * b / 2) + c.");
    }

    public void runTask() {
        int a = 0;
        int b = 0;
        int c = 0;
        Scanner in = new Scanner(System.in);
        System.out.print("Enter a: ");
        a = in.nextInt();
        System.out.print("Enter b: ");
        b = in.nextInt();
        System.out.print("Enter c: ");
        c = in.nextInt();
        System.out.println("z = " + (((a - 3) * b / 2) + c));

    }
}
