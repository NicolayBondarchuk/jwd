package by.training.bnm.arrays.arraysimple;

import java.util.Arrays;
import java.util.Scanner;

public class Task13 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n13. Определить количество элементов последовательности натуральных чисел,\n" +
                " кратных числу М и заключенных в промежутке от L до N.");

    }

    public void runTask() {
        // create array (static method)
        int[] array = Task1.createArray();

        System.out.println("Array: " + Arrays.toString(array));

        int M = 0;
        int L = 0;
        int N = array.length;
        int counter = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter M: ");
        M = scanner.nextInt();
        System.out.print("Enter L: ");
        L = scanner.nextInt();

        for (int i = 0; i < N; i++) {
            if (array[i] % M == 0) {
                if (array[i] > L && array[i] < N)
                    counter++;
            }
        }

        System.out.println("Amount of elements is " + counter);

    }
}
