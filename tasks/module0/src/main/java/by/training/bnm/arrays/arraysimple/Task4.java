package by.training.bnm.arrays.arraysimple;

import java.util.Arrays;
import java.util.Scanner;

public class Task4 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n4. Дана последовательность действительных чисел а1, а2 ,..., аn . " +
                "Выяснить, будет ли она возрастающей.");
    }

    public void runTask() {
        int[] a = Task1.createArray();
        boolean isIncreasing = true;
        Scanner scanner = new Scanner(System.in);

        System.out.println("Array: " + Arrays.toString(a));

        for (int i = 0; i < (a.length - 1); i++) {
            if (a[i] > a[i + 1]) {
                isIncreasing = false;
                System.out.println("Sequence is not increasing");
            }
        }
        if (isIncreasing)
            System.out.println("Sequence is increasing");
    }
}
