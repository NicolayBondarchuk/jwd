package by.training.bnm.decomposition;

import java.util.Scanner;

public class Task5 implements Task {
    @Override
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n5. Написать метод(методы) для нахождения суммы, большего и меньшего из трех чисел.");
    }

    @Override
    public void runTask() {
        Scanner scanner = new Scanner(System.in);
        int[] numbers = new int[3];
        for (int i = 0; i < 3; i++) {
            System.out.print("Enter number " + (i + 1) + ": ");
            numbers[i] = scanner.nextInt();
        }

        System.out.println("Sum: " + sum(numbers));
        System.out.println("Max: " + max(numbers));
        System.out.println("Min: " + min(numbers));
    }
    // нахождение суммы
    public int sum(int[] number) {
        int sum = 0;
        for (int i = 0; i < number.length; i++) {
            sum += number[i];
        }
        return sum;
    }

    // нахождение наибольшего значения
    public int max(int[] number) {
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < number.length; i++) {
            if (max < number[i])
                max = number[i];
        }
        return max;
    }

    // нахождение наименьшего значения
    public int min(int[] number) {
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < number.length; i++) {
            if (min > number[i])
                min = number[i];
        }
        return min;
    }

}
