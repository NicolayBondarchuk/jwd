package by.training.bnm.basics.linear.task13;

/*
13. Заданы координаты трех вершин треугольника (х1 у2),(х2, у2),(х3, у3).
Найти его периметр и площадь.
 */

import by.training.bnm.basics.linear.task.Task;

import java.util.Scanner;

public class Task13 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n13. Заданы координаты трех вершин треугольника " +
                        "(х1 у2),(х2, у2),(х3, у3). Найти его периметр и площадь.");
    }

    public void runTask() {
        double x1 = 0;
        double y1 = 0;
        double x2 = 0;
        double y2 = 0;
        double x3 = 0;
        double y3 = 0;
        double Square = 0;
        double Perimeter = 0;
        Scanner in = new Scanner(System.in);

        System.out.println("Enter coordinates:");

        System.out.print("Enter x1: ");
        x1 = in.nextDouble();
        System.out.print("Enter y1: ");
        y1 = in.nextDouble();
        System.out.print("Enter x2: ");
        x2 = in.nextDouble();
        System.out.print("Enter y2: ");
        y2 = in.nextDouble();
        System.out.print("Enter x3: ");
        x3 = in.nextDouble();
        System.out.print("Enter y3: ");
        y3 = in.nextDouble();

        Perimeter = Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)) +
                 Math.sqrt((x1 - x3) * (x1 - x3) + (y1 - y3) * (y1 - y3)) +
                 Math.sqrt((x3 - x2) * (x3 - x2) + (y3 - y2) * (y3 - y2));

        System.out.println("Perimeter of the triangle is " + Perimeter);

        Square = 1.0 / 2.0 * ((x1 - x3) * (y2 - y3) - (x2 - x3) * (y1 - y3));
        if (Square < 0)
            Square = -1 * Square;

        System.out.println("Square of the triangle is " + Square);
    }
}
