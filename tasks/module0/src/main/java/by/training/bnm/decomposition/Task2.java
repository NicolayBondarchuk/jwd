package by.training.bnm.decomposition;

import java.util.Scanner;

public class Task2 implements Task {
    @Override
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n2. Написать метод(методы) для нахождения наибольшего общего делителя " +
                "и наименьшего общего кратного двух\n натуральных чисел:" +
                "HOK(A, B) = A * B / НОД(А, В)");
    }

    @Override
    public void runTask() {
        int numberA = 0;
        int numberB = 0;
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter number A: ");
        numberA = scanner.nextInt();
        System.out.print("Enter number B: ");
        numberB = scanner.nextInt();

        System.out.println("NOK = " + NOK(numberA, numberB));

    }

    // нахождение НОД по алгоритму Евклида
    public static int NOD(int A, int B) {
        int max = 0;
        int min = 0;
        max = Math.max(A, B);
        min = Math.min(A, B);
        int nod = 0;
        nod = max % min;
        while (nod != 0) {
            max = min;
            min = nod;
            nod = max % min;
        }
            return min;
    }

    // нахождение НОК
    public int NOK(int A, int B) {
        int nok = (A * B) / NOD(A, B);
        return nok;
    }

    public static void main(String[] args) {
        Task2 task2 = new Task2();
        task2.runTask();
    }
}
