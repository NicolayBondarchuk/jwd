package by.training.bnm.oop.basics.task1.controller;

import by.training.bnm.oop.basics.task1.service.ActionText;

public class AddTextCommand implements FileCommand {

    ActionText actionText;

    public AddTextCommand(ActionText actionText) {
        this.actionText = actionText;
    }

    @Override
    public void execute(String text) {
        actionText.addText(text);
    }
}
