package by.training.bnm.arrays.arraysimple;

import java.util.Arrays;
import java.util.Scanner;

public class Task7 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n7. Дана последовательность действительных чисел а1 ,а2 ,..., ап .\n" +
                " Заменить все ее члены, большие данного Z, этим числом.\n" +
                "Подсчитать количество замен.");
    }

    public void runTask() {
        int[] a = Task1.createArray();
        int z = 0;
        int counter = 0;
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter z: ");
        z = scanner.nextInt();

        System.out.println("Array: " + Arrays.toString(a));

        for (int i = 0; i < a.length; i++) {
            if (a[i] > z) {
                a[i] = z;
                counter++;
            }
        }
        System.out.println("New array: " + Arrays.toString(a));
        System.out.println("replacement: " + counter);
    }
}
