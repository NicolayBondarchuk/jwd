package by.training.bnm.basics.linear.task5;

import by.training.bnm.basics.linear.task.Task;

import java.util.Scanner;

/*
5. Составить алгоритм нахождения среднего арифметического двух чисел
 */
public class Task5 implements Task {
    public void runTask() {
        int first = 0;
        int second = 0;
        System.out.println("We can calculate average number");
        Scanner in = new Scanner(System.in);
        System.out.print("Enter first number: ");
        first = in.nextInt();
        System.out.print("Enter second number: ");
        second = in.nextInt();
        System.out.println("Average number: " + ((double)(first + second) / 2));
    }

    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n5. Составить алгоритм нахождения среднего арифметического двух чисел");
    }
}
