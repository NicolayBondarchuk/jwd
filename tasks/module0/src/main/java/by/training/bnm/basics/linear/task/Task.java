package by.training.bnm.basics.linear.task;

public interface Task {
    public void showTask();
    public void runTask();
}
