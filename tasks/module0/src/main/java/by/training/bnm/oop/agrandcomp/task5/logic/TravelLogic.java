package by.training.bnm.oop.agrandcomp.task5.logic;

import by.training.bnm.oop.agrandcomp.task5.data.Tour;
import by.training.bnm.oop.agrandcomp.task5.group.Travel;

public class TravelLogic {
    Travel travel;

    public TravelLogic() {
    }

    public Travel getTravel() {
        return travel;
    }

    public void setTravel(Travel travel) {
        this.travel = travel;
    }

    public TravelLogic(Travel travel) {
        this.travel = travel;
    }

    // поиск путевок по типу отдыха
    public Tour[] searchType(String type) {
        int count = 0;
        for (Tour tour : travel.getTours()) {
            if (type.equals(tour.getType()))
                count++;
        }
        if (count == 0)
            return null;
        // создаем и заполняем массив
        Tour[] array = new Tour[count];
        int j = 0;
        for (Tour tour : travel.getTours()) {
            if (type.equals(tour.getType())) {
                array[j] = tour;
                j++;
            }
        }
        return array;
    }

    // поиск путевок по типу транспорта
    public Tour[] searchTransport(String transport) {
        int count = 0;
        for (Tour tour : travel.getTours()) {
            if (transport.equals(tour.getTransport()))
                count++;
        }
        if (count == 0)
            return null;
        // создаем и заполняем массив
        Tour[] array = new Tour[count];
        int j = 0;
        for (Tour tour : travel.getTours()) {
            if (transport.equals(tour.getTransport())) {
                array[j] = tour;
                j++;
            }
        }
        return array;
    }

    // поиск путевок по типу питания
    public Tour[] searchMeals(String meals) {
        int count = 0;
        for (Tour tour : travel.getTours()) {
            if (meals.equals(tour.getMeals()))
                count++;
        }
        if (count == 0)
            return null;
        // создаем и заполняем массив
        Tour[] array = new Tour[count];
        int j = 0;
        for (Tour tour : travel.getTours()) {
            if (meals.equals(tour.getMeals())) {
                array[j] = tour;
                j++;
            }
        }
        return array;
    }

    // поиск по числу дней
    public Tour[] searchDays(int days) {
        int count = 0;
        for (Tour tour : travel.getTours()) {
            if (days + 1 >= tour.getNumberOfDays() ||
                    days - 1 <= tour.getNumberOfDays())
                count++;
        }
        if (count == 0)
            return null;
        // создаем и заполняем массив
        Tour[] array = new Tour[count];
        int j = 0;
        for (Tour tour : travel.getTours()) {
            if (days + 1 >= tour.getNumberOfDays() ||
                    days - 1 <= tour.getNumberOfDays()) {
                array[j] = tour;
                j++;
            }
        }
        return array;
    }
    // сортировка путевок по странам
    public void sortCountry() {
        int j = 0;
        Tour temp;
        int n = travel.getTours().length;
        int c = 0; // переменная для перехода к следующему символу
        // реализация сортировки Шелла
        while (j < n - 1) {
            if (!travel.getTour(j).getCountry().equals(travel.getTour(j + 1).getCountry())) {
                if (travel.getTour(j).getCountry().charAt(c) >
                        travel.getTour(j + 1).getCountry().charAt(c)) {
                    temp = travel.getTour(j);
                    travel.setTour(j, travel.getTour(j + 1));
                    travel.setTour(j + 1, temp);
                    if (j > 0) {
                        j--;
                        c = 0;
                    } else {
                        j++;
                        c = 0;
                    }
                    // если символы совпадают, сравниваем следующие
                } else if (travel.getTour(j).getCountry().charAt(c) ==
                        travel.getTour(j + 1).getCountry().charAt(c)) {
                    c++;
                } else {
                    j++;
                    c = 0;
                }
            } else {
                j++;
                c = 0;
            }
        }
    }

    // сортировка путевок по типу отдыха
    public void sortType() {
        int j = 0;
        Tour temp;
        int n = travel.getTours().length;
        int c = 0; // переменная для перехода к следующему символу
        // реализация сортировки Шелла
        while (j < n - 1) {
            if (!travel.getTour(j).getType().equals(travel.getTour(j + 1).getType())) {
                if (travel.getTour(j).getType().charAt(c) >
                        travel.getTour(j + 1).getType().charAt(c)) {
                    temp = travel.getTour(j);
                    travel.setTour(j, travel.getTour(j + 1));
                    travel.setTour(j + 1, temp);
                    if (j > 0) {
                        j--;
                        c = 0;
                    } else {
                        j++;
                        c = 0;
                    }
                    // если символы совпадают, сравниваем следующие
                } else if (travel.getTour(j).getType().charAt(c) ==
                        travel.getTour(j + 1).getType().charAt(c)) {
                    c++;
                } else {
                    j++;
                    c = 0;
                }
            } else {
                j++;
                c = 0;
            }
        }
    }

    // сортировка путевок по типу транспорта
    public void sortTransport() {
        int j = 0;
        Tour temp;
        int n = travel.getTours().length;
        int c = 0; // переменная для перехода к следующему символу
        // реализация сортировки Шелла
        while (j < n - 1) {
            if (!travel.getTour(j).getTransport().equals(travel.getTour(j + 1).getTransport())) {
                if (travel.getTour(j).getTransport().charAt(c) >
                        travel.getTour(j + 1).getTransport().charAt(c)) {
                    temp = travel.getTour(j);
                    travel.setTour(j, travel.getTour(j + 1));
                    travel.setTour(j + 1, temp);
                    if (j > 0) {
                        j--;
                        c = 0;
                    } else {
                        j++;
                        c = 0;
                    }
                    // если символы совпадают, сравниваем следующие
                } else if (travel.getTour(j).getTransport().charAt(c) ==
                        travel.getTour(j + 1).getTransport().charAt(c)) {
                    c++;
                } else {
                    j++;
                    c = 0;
                }
            } else {
                j++;
                c = 0;
            }
        }
    }

    // сортировка путевок по типу питания
    public void sortMeals() {
        int j = 0;
        Tour temp;
        int n = travel.getTours().length;
        int c = 0; // переменная для перехода к следующему символу
        // реализация сортировки Шелла
        while (j < n - 1) {
            if (!travel.getTour(j).getMeals().equals(travel.getTour(j + 1).getMeals())) {
                if (travel.getTour(j).getMeals().charAt(c) >
                        travel.getTour(j + 1).getMeals().charAt(c)) {
                    temp = travel.getTour(j);
                    travel.setTour(j, travel.getTour(j + 1));
                    travel.setTour(j + 1, temp);
                    if (j > 0) {
                        j--;
                        c = 0;
                    } else {
                        j++;
                        c = 0;
                    }
                    // если символы совпадают, сравниваем следующие
                } else if (travel.getTour(j).getMeals().charAt(c) ==
                        travel.getTour(j + 1).getMeals().charAt(c)) {
                    c++;
                } else {
                    j++;
                    c = 0;
                }
            } else {
                j++;
                c = 0;
            }
        }
    }

    // сортировка по дням отдыха
    public void sortDays() {
        int j = 0;
        Tour temp;
        int n = travel.getTours().length;
        // реализация сортировки Шелла
        while (j < n - 1) {
            if (travel.getTour(j).getNumberOfDays() >
                    travel.getTour(j + 1).getNumberOfDays()) {
                temp = travel.getTour(j);
                travel.setTour(j, travel.getTour(j + 1));
                travel.setTour(j + 1, temp);
                if (j > 0) {
                    j--;
                } else {
                    j++;
                }
            } else {
                j++;
            }
        }
    }

}
