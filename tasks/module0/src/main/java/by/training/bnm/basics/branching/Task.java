package by.training.bnm.basics.branching;

public interface Task {
    public void showTask();
    public void runTask();
}
