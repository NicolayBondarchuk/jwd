package by.training.bnm.decomposition;

import by.training.bnm.decomposition.Task1.Point;

import java.util.Scanner;

public class Task7 implements Task {
    @Override
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n7. На плоскости заданы своими координатами n точек. Написать метод(методы)," +
                " определяющие, между какими из\n пар точек самое большое расстояние. Указание. Координаты" +
                " точек занести в массив.");
    }

    @Override
    public void runTask() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter n: ");
        int n = scanner.nextInt();

        double x = 0.0, y = 0.0, z = 0.0;
        Point[] points = new Point[n];
        for (int i = 0; i < n; i++) {
            System.out.println("Enter point " + (i + 1) + ": ");
            System.out.print("x: ");
            x = scanner.nextDouble();
            System.out.print("y: ");
            y = scanner.nextDouble();
            System.out.print("z: ");
            z = scanner.nextDouble();
            points[i] = new Point(x, y, z);
        }

        // массив результата [максимальное расстояние, точка 1, точка 2]
        double[] result = new double[3];
        result = maxLength(points);

        // результат
        System.out.println("Maximum length is " + result[0] + " between points " +
                points[(int)result[1]] + " and " + points[(int)result[2]]);


    }

    // метод нахождения максимального расстояния между точками
    public double[] maxLength(Point[] points)
    {
        double[] result = new double[3];

        double max = Integer.MIN_VALUE;
        double temp = 0;
        for (int i = 0; i < (points.length - 1); i++) {
            for (int j = i + 1; j < points.length; j++) {
                // для вычисления расстояний используем static-метод из Task1
                temp = Task1.calcLength(points[i], points[j]);
                if (max < temp) {
                    max = temp;
                    result[0] = max;
                    result[1] = i;
                    result[2] = j;
                }
            }
        }
        return result;
    }
}
