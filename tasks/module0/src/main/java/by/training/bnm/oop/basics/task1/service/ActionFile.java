package by.training.bnm.oop.basics.task1.service;

public interface ActionFile {
    boolean createFile(String name);
    boolean rename(String newName, String fileName);
    boolean remove(String name);
}
