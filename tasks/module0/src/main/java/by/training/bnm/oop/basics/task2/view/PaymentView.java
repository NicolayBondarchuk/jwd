package by.training.bnm.oop.basics.task2.view;

import by.training.bnm.oop.basics.task2.data.Payment;

public class PaymentView {

  private Payment payment;

  public PaymentView() {
  }

  public PaymentView(Payment payment) {
    this.payment = payment;
  }

  public Payment getPayment() {
    return payment;
  }

  public void setPayment(Payment payment) {
    this.payment = payment;
  }

  public void showPayment() {
    for (int i = 0; i < payment.getSize(); i++) {
      System.out.println(payment.getString(i));
    }
  }

}
