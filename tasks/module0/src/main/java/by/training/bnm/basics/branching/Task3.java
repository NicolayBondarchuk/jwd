package by.training.bnm.basics.branching;

import java.util.Scanner;

public class Task3 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n3. Составить программу сравнения введенного числа а и цифры 3. " +
                "Вывести на экран слово «yes», если число а меньше 3; если больше, то " +
                "вывести слово «no».");
    }

    public void runTask() {
        int a = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter number a: ");
        a = scanner.nextInt();
        if (a < 3)
            System.out.println("Yes");
        else if (a > 3)
            System.out.println("No");
        else
            System.out.println("Equals");

    }
}
