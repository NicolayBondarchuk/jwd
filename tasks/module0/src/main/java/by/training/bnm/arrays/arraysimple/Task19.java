package by.training.bnm.arrays.arraysimple;

import java.util.Arrays;

public class Task19 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n19. В массиве целых чисел с количеством элементов n найти наиболее \n" +
                "часто встречающееся число. Если таких чисел несколько, \n" +
                "то определить наименьшее из них.");
    }

    public void runTask() {
        int[] a = Task1.createArray();
        int count = 0;
        int maxRepeat = 0;
        int result = 0;

        System.out.println("Array: " + Arrays.toString(a));

        for (int i = 0; i < a.length; i++) {
            for (int j = i + 1; j < a.length; j++) {
                if (a[i] == a[j])
                    count++;
            }
            if (maxRepeat < count) {
                maxRepeat = count;
                result = a[i];
            }
            if (maxRepeat == count) {
                if (result > a[i])
                    result = a[i];
            }
            count = 0;
        }
        System.out.print("Number " + result);
        System.out.println(", max repeat is " + maxRepeat);
    }
}
