package by.training.bnm.arrays.sort;

import java.util.Arrays;

public class Task6 implements Task {
    @Override
    public void showTask() {
        System.out.println("-----------------------------------------------");
        System.out.println("\n6. Сортировка Шелла. Дан массив n действительных чисел. Требуется упорядочить" +
                " его по возрастанию. Делается\n это следующим образом: сравниваются два соседних элемента " +
                "ai и a(i+1). Если ai <= a(i+1), то продвигаются на один\n элемент вперед. Если ai > a(i+1)," +
                " то производится перестановка и сдвигаются на один элемент назад. Составить\n" +
                "алгоритм этой сортировки.");
    }

    @Override
    public void runTask() {
        int[] a = Task.createArray();
        System.out.println("Array: " + Arrays.toString(a));

        SortShell(a);
        System.out.println("Array after sort: " + Arrays.toString(a));

    }

    public static void SortShell (int[] array){
        // реализация сортировки Шелла
        int temp = 0;
        int j = 0;
        while (j < array.length - 1) {
            if (array[j] > array[j + 1]) {
                temp = array[j];
                array[j] = array[j + 1];
                array[j + 1] = temp;
                if (j > 0)
                    j--;
                else
                    j++;
            }
            else
                j++;
        }
    }
}
