package by.training.bnm.decomposition;

public class Task10 implements Task {
    @Override
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n10. Написать метод(методы) для вычисления суммы факториалов всех " +
                "нечетных чисел от 1 до 9.");
    }

    @Override
    public void runTask() {
        int size = 9;
        int[] array = new int[size];
        for (int i = 0; i < array.length; i++) {
            array[i] = i + 1;
        }

        int sum = sumOddNumbers(array);
        System.out.println("Sum = " + sum);

    }

    public int sumOddNumbers(int[] array) {
        int sum = 0;
        int factorial;
        for (int value : array) {
            if (value % 2 != 0) {
                factorial = 1;
                for (int j = 2; j < value; j++) {
                    factorial *= j;
                }
                sum += factorial;
            }
        }
        return sum;
    }
}
