package by.training.bnm.decomposition;

import java.util.Arrays;
import java.util.Scanner;

public class Task16 implements Task {
    @Override
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n16. Два простых числа называются «близнецами», если они " +
                "отличаются друг от друга на 2 (например, 41 и 43). Найти\n и напечатать" +
                " все пары «близнецов» из отрезка [n,2n], где n - заданное натуральное " +
                "число больше 2. Для решения\n задачи использовать декомпозицию.");
    }

    @Override
    public void runTask() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter n (bigger than 2): " );
        int n = scanner.nextInt();

        // создаем массив
        int[] array = createArrayN2N(n);
        System.out.println("Array is " + Arrays.toString(array));

        // нахождение пар близнецов простых чисел
        findPrimePairs(array);


    }

    // создание массива, который заполняется числами от n до 2n
    public int[] createArrayN2N(int size) {
        int[] array = new int[size + 1];
        //заполнение
        for (int i = 0; i <= size; i++) {
            array[i] = i + size;
        }
        return array;
    }

    // проверка на простое число
    public boolean isPrime(int number) {
        for (int i = 2; i <= number / i; i++) {
            if (number % i == 0)
                return false;
            //if (i > number / i)
             //   return true;
        }
        return true;
    }

    // нахождение пар близнецов простых чисел
    public void findPrimePairs(int[] array) {
        int firstNumber = 0;
        for (int number : array) {
            // проверка на простое число
            if (isPrime(number)) {
                if (firstNumber == 0) {
                    firstNumber = number;
                } else if (number - firstNumber == 2){
                    System.out.println("Pair: " + firstNumber + " and " + number);
                } else {
                    firstNumber = number;
                }
            }
        }
    }
}
