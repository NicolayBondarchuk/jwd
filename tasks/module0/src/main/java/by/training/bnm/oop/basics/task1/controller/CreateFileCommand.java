package by.training.bnm.oop.basics.task1.controller;

import by.training.bnm.oop.basics.task1.service.ActionFile;

public class CreateFileCommand implements FileCommand {

    private ActionFile actionFile;

    public CreateFileCommand(ActionFile actionFile) {
        this.actionFile = actionFile;
    }

    @Override
    public void execute(String name) {
        actionFile.createFile(name);
    }
}
