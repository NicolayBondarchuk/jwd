package by.training.bnm.arrays.sort;

import java.util.Scanner;

public interface Task {
    public void showTask();
    public void runTask();

    static int[] createArrayNatural() {
        int max = 0;
        int n = 0;
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter max number for array: ");
        max = scanner.nextInt();
        System.out.print("Enter size of array: ");
        n = scanner.nextInt();

        int[] a = new int[n];
        // заполнение массива
        for (int i = 0; i < n; i++) {
            a[i] = (int)(Math.random() * max + 1);
        }

        return a;
    }

    static int[] createArray() {
        int max = 0;
        int n = 0;
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter max number for array: ");
        max = scanner.nextInt();
        max *= 2;
        System.out.print("Enter size of array: ");
        n = scanner.nextInt();

        int[] a = new int[n];
        // заполнение массива
        for (int i = 0; i < n; i++) {
            a[i] = (int)(Math.random() * max) - max / 2;
        }

        return a;
    }
}
