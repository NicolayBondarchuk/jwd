package by.training.bnm.arrays.arraysimple;

import java.util.Arrays;
import java.util.Scanner;

public class Task10 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n10. Даны целые числа а1 ,а2 ,..., аn . Вывести на печать \n" +
                "только те числа, для которых аi > i.");
    }

    public void runTask() {
        int[] a = Task1.createArray();
        Scanner scanner = new Scanner(System.in);

        System.out.println("Array is: " + Arrays.toString(a));

        System.out.print("New array is: [");
        for (int i = 0; i < a.length; i++) {
            if (a[i] > i) {
                if (i == a.length - 1) {
                    System.out.print(a[i]);
                    break;
                }
                System.out.print(a[i] + ", ");
            }
        }
        System.out.println("]");

    }
}
