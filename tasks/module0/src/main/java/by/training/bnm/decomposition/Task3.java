package by.training.bnm.decomposition;

import java.util.Scanner;

public class Task3 implements Task {
    @Override
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n3. Написать метод(методы) для нахождения наибольшего общего делителя " +
                "четырех натуральных чисел.");
    }

    @Override
    public void runTask() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter number 1: ");
        int number1 = scanner.nextInt();
        System.out.print("Enter number 2: ");
        int number2 = scanner.nextInt();
        System.out.print("Enter number 3: ");
        int number3 = scanner.nextInt();
        System.out.print("Enter number 4: ");
        int number4 = scanner.nextInt();

        int nod = NOD(number1, number2, number3, number4);
        System.out.println("NOD of four number is " + nod);
    }

    public int NOD(int number1, int number2, int number3, int number4) {
        Task2 task2 = new Task2();
        int nod12 = task2.NOD(number1, number2);
        int nod34 = task2.NOD(number3, number4);
        int nod = task2.NOD(nod12, nod34);
        return nod;
    }
}
