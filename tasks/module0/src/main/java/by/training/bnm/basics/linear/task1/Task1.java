package by.training.bnm.basics.linear.task1;

/*
    1. Даны два действительных числа х и у. Вычислить их сумму, разность, произведение и частное.
 */

import by.training.bnm.basics.linear.task.Task;

import java.util.Scanner;

public class Task1 implements Task {

    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n1. Даны два действительных числа х и у." +
                " Вычислить их сумму, разность, произведение и частное.");
    }

    public void runTask() {
        int x = 0;
        int y = 0;
        Scanner in = new Scanner(System.in);
        System.out.print("Enter x: ");
        x = in.nextInt();
        System.out.print("Enter y: ");
        y = in.nextInt();
        System.out.println("x + y = " + (x + y));
        System.out.println("x - y = " + (x - y));
        System.out.println("x * y = " + (x * y));
        System.out.println("x / y = " + (double)x / y);
    }
}
