package by.training.bnm.basics.cycles;

import java.util.Scanner;

public class Task11 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n14. Дано натуральное n. вычислить: 1 + 1/2 + 1/3 + 1/4 + ... + 1/n.");
    }

    public void runTask() {
        double n = 0;
        double i = 1;
        double sum = 0;
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter n: ");
        n = scanner.nextInt();

        while (i <= n) {
            sum += 1.0 / i;
            i++;
        }

        System.out.println("Sum = " + sum);

    }
}
