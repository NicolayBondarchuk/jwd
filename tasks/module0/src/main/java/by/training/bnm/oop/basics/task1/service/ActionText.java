package by.training.bnm.oop.basics.task1.service;

public interface ActionText {
    boolean addText(String text);
    void showText();
}
