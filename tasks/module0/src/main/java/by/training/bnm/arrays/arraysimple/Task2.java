package by.training.bnm.arrays.arraysimple;

import java.util.Arrays;
import java.util.Scanner;

public class Task2 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n2. В целочисленной последовательности есть нулевые элементы." +
                " Создать массив из номеров этих элементов.");
    }

    public void runTask() {
        int[] array = Task1.createArray();
        // счетчик
        int count = 0;

        Scanner scanner = new Scanner(System.in);

        for (int i = 0; i < array.length; i++) {
            if (array[i] == 0)
                count++;
        }
        System.out.println("We have: " + Arrays.toString(array));

        if (count == 0) {
            System.out.println("No null element!");
        } else {
            int[] nullArray = new int[count];
            int j = 0;
            for (int i = 0; i < count; i++) {
                if (array[i] == 0) {
                    nullArray[j] = i;
                    j++;
                }
            }

            System.out.println("NullArray: " + Arrays.toString(nullArray));
        }
    }
}
