package by.training.bnm.oop.basics.task1.controller;

import by.training.bnm.oop.basics.task1.service.ActionFile;

import java.util.Scanner;

public class RenameFileCommand implements FileCommand {

    private ActionFile actionFile;

    public RenameFileCommand(ActionFile actionFile) {
        this.actionFile = actionFile;
    }

    @Override
    public void execute(String name) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter new name for file: ");
        String newName = scanner.nextLine();

        actionFile.rename(newName, name);
    }
}
