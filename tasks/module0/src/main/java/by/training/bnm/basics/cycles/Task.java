package by.training.bnm.basics.cycles;

public interface Task {
    public void showTask();
    public void runTask();
}
