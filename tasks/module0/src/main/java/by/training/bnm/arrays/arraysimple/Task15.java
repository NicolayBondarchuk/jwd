package by.training.bnm.arrays.arraysimple;

import java.util.Arrays;
import java.util.Scanner;

public class Task15 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n15. Дана последовательность действительных чисел a1, a2, ..., an.\n" +
                " Указать те ее элементы, которые принадлежат отрезку [с, d].");
    }

    public void runTask() {
        int[] a = Task1.createArray();
        int c = 0;
        int d = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Array: " + Arrays.toString(a));

        System.out.print("Enter c: ");
        c = scanner.nextInt();
        System.out.print("Enter d: ");
        d = scanner.nextInt();

        System.out.print("Between 'c' and 'd': [");
        for (int i = 0; i < a.length; i++) {
            if (a[i] > c && a[i] < d) {
                if (i == a.length - 1) {
                    System.out.print(a[i]);
                    continue;
                }
                System.out.print(a[i] + ", ");
            }
        }
        System.out.println("]");
    }
}
