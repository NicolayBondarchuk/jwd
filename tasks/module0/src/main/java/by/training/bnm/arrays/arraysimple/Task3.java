package by.training.bnm.arrays.arraysimple;

import java.util.Arrays;
import java.util.Scanner;

public class Task3 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n3. Дана последовательность целых чисел а1, а2 ,..., аn. " +
                "Выяснить, какое число встречается раньше - положительное или\n" +
                "отрицательное.");
    }

    public void runTask() {
        int[] a = Task1.createArray();
        Scanner scanner = new Scanner(System.in);

        System.out.println("Array: " + Arrays.toString(a));

        if (a[0] < 0) {
            System.out.println("Negative number is first");
        } else {
            System.out.println("Positive number is first");
        }
    }
}
