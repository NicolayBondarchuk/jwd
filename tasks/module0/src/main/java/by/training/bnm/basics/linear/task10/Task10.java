package by.training.bnm.basics.linear.task10;

import by.training.bnm.basics.linear.task.Task;

import java.util.Scanner;

import static java.lang.Math.*;

public class Task10 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n10. Вычислить значение выражения по формуле (все " +
                "переменные принимают действительные значения):" +
                "(sin(x) + cos(y)) / (cos(x) - sin(y)) * tan(x * y)");
    }

    public void runTask() {
        double x = 0;
        double y = 0;
        double result = 0;
        Scanner in = new Scanner(System.in);

        System.out.print("Enter x: ");
        x = in.nextDouble();
        System.out.print("Enter y: ");
        y = in.nextDouble();

        result = ((sin(x) - cos(y)) / (cos(x) - sin(y)) * tan(x * y));
        System.out.println("Result: " + result);
    }
}
