package by.training.bnm.oop.simple.task1;

/*
    1. Создайте класс Test1 двумя переменными. Добавьте метод вывода на экран и методы изменения этих переменных.
Добавьте метод, который находит сумму значений этих переменных, и метод, который находит наибольшее значение
из этих двух переменных.

 */
public class Main {

    public static void main(String[] args) {
        Test1 one = new Test1();
        Test1 two = new Test1();

        one.setX(5);
        one.setY(8);

        one.show();

        two.setX(10);
        two.setY(1);

        two.show();

        System.out.println("one.max: " + one.max());
        System.out.println("two.max: " + two.max());
        System.out.println("one.sum: " + one.sum());
        System.out.println("two.sum: " + two.sum());
    }
}
