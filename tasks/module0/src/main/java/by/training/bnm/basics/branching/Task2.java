package by.training.bnm.basics.branching;

import java.util.Scanner;

public class Task2 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("2. Составить программу сравнения двух чисел 1 и 2. " +
                "Если 1 меньше 2 – вывести на экран слово «yes», в противном" +
                "случае – слово «no»");
    }

    public void runTask() {
        int number1 = 0;
        int number2 = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter number 1: ");
        number1 = scanner.nextInt();
        System.out.print("Enter number 2: ");
        number2 = scanner.nextInt();

        if (number1 < number2)
            System.out.println("Yes");
        else
            System.out.println("No");

    }
}
