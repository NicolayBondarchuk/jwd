package by.training.bnm.decomposition;

import java.util.Arrays;
import java.util.Scanner;

public class Task8 implements Task {
    @Override
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n8. Составить программу, которая в массиве A[N] находит второе по " +
                "величине число (вывести на печать число,\n которое меньше максимального " +
                "элемента массива, но больше всех других элементов).");
    }

    @Override
    public void runTask() {
        int[] A;
        // статический метод для создания массива int
        A = by.training.bnm.arrays.sort.Task.createArray();
        System.out.println("Array: " + Arrays.toString(A));

        int result = prevMax(A);
        System.out.println("Number before maximum is " + result);

    }

    public int prevMax(int[] array) {
        int max = Integer.MIN_VALUE;
        int previous = max;

        for (int i = 0; i < array.length; i++) {
            if (max < array[i]) {
                previous = max;
                max = array[i];
            } else if (previous < array[i] && array[i] != max) {
                previous = array[i];
            }
        }
        return previous;
    }
}
