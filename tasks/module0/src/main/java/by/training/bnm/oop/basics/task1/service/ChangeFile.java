package by.training.bnm.oop.basics.task1.service;

import by.training.bnm.oop.basics.task1.model.Directory;
import by.training.bnm.oop.basics.task1.model.File;

public class ChangeFile implements ActionFile {

    Directory directory;

    public ChangeFile(Directory directory) {
        this.directory = directory;
    }

    public ChangeFile() {

    }

    @Override
    public boolean createFile(String name) {
        // проверка на совпадение имен
        for (File filename :
                directory.getFiles()) {
            if (name.equals(filename.getName())) {
                return false;
            }
        }

        return directory.getFiles().add(new File(name));
    }

    @Override
    public boolean rename(String newName, String fileName) {
        // проверка на совпадение имен
        for (File filename :
             directory.getFiles()) {
            if (newName.equals(filename.getName())) {
                return false;
            }
        }
        directory.getFiles(fileName).setName(newName);
        return true;
    }

    @Override
    public boolean remove(String name) {
        return directory.getFiles().removeIf(file -> name.equals(file.getName()));
    }
}
