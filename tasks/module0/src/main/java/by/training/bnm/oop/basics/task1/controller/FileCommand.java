package by.training.bnm.oop.basics.task1.controller;

public interface FileCommand {
    void execute(String name);
}
