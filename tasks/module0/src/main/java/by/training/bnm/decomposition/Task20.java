package by.training.bnm.decomposition;

import java.util.Scanner;

public class Task20 implements Task {
    @Override
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n20. Из заданного числа вычли сумму его цифр. Из результата " +
                "вновь вычли сумму его цифр и т.д. Сколько таких\n действий надо произвести," +
                " чтобы получился нуль? Для решения задачи использовать декомпозицию.");
    }

    @Override
    public void runTask() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter number: ");
        int number = scanner.nextInt();

        // находим разрядность числа number
        int size = Task13.sizeNumber(number);

        // находим сумму элементов числа
        int sum = Task17.sumDigitsOfNumber(number, size);

        // находим количество действий вычитания
        int counter = counterOfDif(number, sum);

        System.out.println("The number of actions is " + counter);

    }

    // функция нахождения количества действий вычитания number2 от number1
    public int counterOfDif(int number1, int number2) {
        int counter = 0;
        while (number1 > 0) {
            number1 -= number2;
            counter++;
        }
        return counter;
    }
}
