package by.training.bnm.oop.simple.task9.group;

import by.training.bnm.oop.simple.task9.data.Book;

public class Library {
    private Book[] libr;

    public Library() {
    }

    public Library(Book[] libr) {
        this.libr = libr;
    }

    public int getLength() {
        return libr.length;
    }

    public Book getBook(int index) {
        return libr[index];
    }
}
