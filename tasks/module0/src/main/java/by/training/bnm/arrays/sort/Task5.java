package by.training.bnm.arrays.sort;

import java.util.Arrays;

public class Task5 implements Task {
    @Override
    public void showTask() {
        System.out.println("-----------------------------------------------");
        System.out.println("\n5. Сортировка вставками. Дана последовательность чисел a1, a2, ... , an. Требуется " +
                "переставить числа в порядке\n возрастания. Делается это следующим образом. Пусть a1, a2, ..." +
                " , ai - упорядоченная последовательность, т. е.\na1 <= a2 <= ... <= an. Берется следующее число " +
                "a(i+1) и вставляется в последовательность так, чтобы новая\n последовательность была тоже " +
                "возрастающей. Процесс производится до тех пор, пока все элементы от i+1 до n не\n будут " +
                "перебраны. Примечание. Место помещения очередного элемента в отсортированную часть производить с\n" +
                "помощью двоичного поиска. Двоичный поиск оформить в виде отдельной функции.");
    }

    @Override
    public void runTask() {
        int[] a = Task.createArray();
        int temp = 0;
        int temp_i = 0;
        System.out.println("Array: " + Arrays.toString(a));

        // реализация сортировки вставками
        for (int i = 1; i < a.length; i++) {
            temp = a[i];
            temp_i = bin_find(a, i - 1, a[i]);
            for (int j = i; j > temp_i; j--) {
                a[j] = a[j - 1];
            }
            a[temp_i] = temp;
        }

        System.out.println("Array after sort: " + Arrays.toString(a));
    }

    // функция бинарного поиска
    public static int bin_find(int[] array, int size, int value) {
        int left = 0;
        int right = size + 1;
        int aver = 0;
        while (left <= right) {
            aver = (left + right) / 2;
            if (aver == 0){
                if (value < array[aver])
                    return 0;
                else
                    return 1;
            }
            else if (value == array[aver]) {
                return aver;
            }
            else if (value <= array[aver] && value > array[aver - 1])
                return aver;
            if (value < array[aver])
                right = aver - 1;
            else
                left = aver + 1;
        }
        return aver;
    }
}
