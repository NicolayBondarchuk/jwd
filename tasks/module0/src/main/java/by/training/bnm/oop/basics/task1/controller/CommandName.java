package by.training.bnm.oop.basics.task1.controller;

public enum CommandName {
    NEW_FILE("-n"),
    RENAME("-r"),
    SHOW_FILE("-s"),
    ADD_TEXT("-t"),
    REMOVE("-R");

    CommandName(String s) {
        shortCommand = s;
    }

    private final String shortCommand;

    public static CommandName fromString(String name) {

        final CommandName[] values = CommandName.values();
        for (CommandName commandName : values) {
            if (commandName.shortCommand.equals(name) || commandName.name().equals(name)) {
                return commandName;
            }
        }
        return null;
    }
}
