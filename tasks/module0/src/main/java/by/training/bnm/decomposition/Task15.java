package by.training.bnm.decomposition;

import java.util.Arrays;
import java.util.Scanner;

public class Task15 implements Task {
    @Override
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n15. Даны натуральные числа К и N. Написать метод(методы) " +
                "формирования массива А, элементами которого\n являются числа, сумма " +
                "цифр которых равна К и которые не большее N.");
    }

    @Override
    public void runTask() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter K: ");
        int K = scanner.nextInt();
        System.out.print("Enter N: ");
        int N = scanner.nextInt();
        if (N <= K)
            N = K + 1;

        int[] array = createArrayKN(K, N);
        System.out.println("Array is " + Arrays.toString(array));
    }

    // создание массива, элементами которого являются [случайные] числа,
    // сумма цифр которых равна К и которые не больше N
    public int[] createArrayKN(int k, int n) {
        int[] array = new int[100];

        int sum = 0;
        int i = 0;
        while (sum < k || i >= 100) {
            array[i] = (int)(Math.random() * 2 * n - n);
            sum += array[i];
            i++;
        }

        i--;
        sum -= array[i];
        array[i] = k - sum;
        int[] newArray = new int[i + 1];

        // копируем массив в новый с усечением размера
        newArray = Arrays.copyOf(array, newArray.length);

        return newArray;
    }
}
