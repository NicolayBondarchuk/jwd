package by.training.bnm.oop.simple.task1;

public class Test1 {
    private int x;
    private int y;

    public Test1() {
    }

    public void show() {
        System.out.print("x = " + this.x);
        System.out.println(", y = " + this.y);
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int sum() {
        return this.x + this.y;
    }

    public int max() {
        return Math.max(x, y);
    }
}