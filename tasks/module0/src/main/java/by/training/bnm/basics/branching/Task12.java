package by.training.bnm.basics.branching;

import java.util.Scanner;

public class Task12 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n12. Даны три действительных числа. Возвести в " +
                "квадрат те из них, значения которых неотрицательны, и в " +
                "четвертую степень — отрицательные.");
    }

    public void runTask() {
        double[] number = new double[3];
        Scanner scanner = new Scanner(System.in);

        for (int i = 0; i < number.length; i++) {
            System.out.print("Enter number " + (i + 1) + ": ");
            number[i] = scanner.nextDouble();
        }

        for (int i = 0; i < number.length; i++) {
            if (number[i] >= 0)
                System.out.println("number" + (i + 1) + " = " +
                        number[i] * number[i]);
            else
                System.out.println("number" + (i + 1) + " = " +
                        number[i] * number[i] * number[i] * number[i]);
        }
    }
}
