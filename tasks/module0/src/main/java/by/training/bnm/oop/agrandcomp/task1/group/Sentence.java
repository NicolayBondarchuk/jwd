package by.training.bnm.oop.agrandcomp.task1.group;

import by.training.bnm.oop.agrandcomp.task1.data.Word;

public class Sentence {
    private Word[] sentence;

    public Sentence() {
    }

    public Sentence(Word[] sentence) {
        this.sentence = sentence;
    }

    public Word[] getSentence() {
        return sentence;
    }

    public void setSentence(Word[] sentence) {
        this.sentence = sentence;
    }
}
