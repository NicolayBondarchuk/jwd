package by.training.bnm.basics.cycles;


import java.util.Scanner;

public class Task8 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n8. Вычислить значения функции на отрезке [а,b] c шагом h:");
        System.out.println("y = (x + c) * 2, if x == 15");
        System.out.println("y = (x - c) + 6, if x != 15");
    }

    public void runTask() {
        int a = 0;
        int b = 0;
        int h = 0;
        int c = 5;
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a: ");
        a = scanner.nextInt();
        System.out.print("Enter b: ");
        b = scanner.nextInt();
        System.out.print("Enter h: ");
        h = scanner.nextInt();

        while (a < b) {
            if (a == 15)
                System.out.println("y = " + ((a + c) * 2));
            else
                System.out.println(" y = " + ((a - c) + 6));
            a += h;
        }

    }
}
