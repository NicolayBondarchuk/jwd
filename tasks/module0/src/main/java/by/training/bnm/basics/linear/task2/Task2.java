package by.training.bnm.basics.linear.task2;

import by.training.bnm.basics.linear.task.Task;

import java.util.Scanner;

/*
2. Найдите значение функции: с = 3 + а.
 */

public class Task2 implements Task {

    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n2. Найдите значение функции: с = 3 + а.");
    }

    public void runTask() {
        int a = 0;
        int c = 0;
        System.out.println("Function is: c = 3 + a");
        System.out.print("Enter a: ");
        Scanner in = new Scanner(System.in);
        a = in.nextInt();
        c = 3 + a;
        System.out.println("c = " + c);

    }
}
