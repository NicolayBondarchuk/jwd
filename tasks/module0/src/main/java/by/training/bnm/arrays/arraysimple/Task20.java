package by.training.bnm.arrays.arraysimple;

import java.util.Arrays;

public class Task20 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n20. Дан целочисленный массив с количеством элементов п. Сжать массив, \n" +
                "выбросив из него каждый второй элемент (освободившиеся элементы заполнить нулями). \n" +
                "Примечание. Дополнительный массив не использовать.");
    }

    public void runTask() {
        int[] array = Task1.createArray();

        System.out.println("Array: " + Arrays.toString(array));

        for (int i = 1; i < array.length; i++) {
            if (i < array.length / 2)
                array[i] = array[2 * i];
            else
                array[i] = 0;
        }
        System.out.println("Array: " + Arrays.toString(array));
    }

    public static void main(String[] args) {
        Task20 task20 = new Task20();
        task20.showTask();
        task20.runTask();
    }
}
