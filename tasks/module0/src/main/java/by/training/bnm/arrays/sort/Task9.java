package by.training.bnm.arrays.sort;

import java.util.Arrays;

public class Task9 implements Task {
    @Override
    public void showTask() {
        System.out.println("-----------------------------------------------");
        System.out.println("\n9. Алгоритм фон Неймана. Упорядочить массив a1, a2, ... , an по неубыванию " +
                "с помощью алгоритма сортировки\n слияниями:\n" +
                "1) каждая пара соседних элементов сливается в одну группу из двух элементов (последняя " +
                "группа может состоять из\n одного элемента);\n" +
                "2) каждая пара соседних двухэлементных групп сливается в одну четырехэлементную группу и т. д.\n" +
                "При каждом слиянии новая укрупненная группа упорядочивается.");
    }

    @Override
    public void runTask() {
        int[] a = Task.createArray();
        System.out.println("Array: " + Arrays.toString(a));
        int size = 1;
        int temp = 0;
    }
}

