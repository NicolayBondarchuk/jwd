package by.training.bnm.decomposition;

import java.util.Scanner;

public class Task17 implements Task {
    @Override
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n17. Натуральное число, в записи которого n цифр, называется " +
                "числом Армстронга, если сумма его цифр, возведенная\n в степень n, равна" +
                " самому числу. Найти все числа Армстронга от 1 до k. Для решения задачи " +
                "использовать\n декомпозицию.");
    }

    @Override
    public void runTask() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter k: ");
        int k = scanner.nextInt();

        System.out.println("From 1 to " + k + " the Armstrong's number is :");
        for (int i = 1; i <= k; i++) {
            if (isNumbArmstrong(i))
                System.out.print(i + " ");
        }
        System.out.println();
    }

    // метод нахождения суммы элементов числа
    public static int sumDigitsOfNumber(int number, int size) {
        int sum = 0;
        while (size > 0) {
            // заполняем массив с конца
            sum += (number % 10);
            size--;
            number /= 10;
        }
        return sum;
    }

    // проверка на число Армстронга
    public boolean isNumbArmstrong(int number) {
        int n = 0;
        int sum = 0;

        // для определения длины воспользуемся методом из Task13
        n = Task13.sizeNumber(number);

        // разбиваем число N на составные числа и заносим их в массив
        int[] numbers = Task13.createArrayFromNumber(number, n);

        // возводим каждое число numbers в степень n и находим сумму
        for (int value : numbers) {
            sum += (int)Math.pow(value, n);
        }

        // сравниваем с начальным числом
        if (sum == number)
            return true;
        else
            return false;
    }
}