package by.training.bnm.oop.basics.task5a.logic;


import by.training.bnm.oop.basics.task5a.data.FlowerComp;

public class FlowerCompLogic {
  public double getPrice(FlowerComp flowerComp) {
    double price = 0;
    for (int i = 0; i < flowerComp.getFlowerKits().size(); i++) {
      price += flowerComp.getFlowerKit(i).getPrice() *
                flowerComp.getFlowerKit(i).getAmount();
    }
    price += flowerComp.getPackages().getPrice();
    return price;
  }
}
