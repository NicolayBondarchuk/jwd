package by.training.bnm.arrays.sort;

import java.util.Arrays;

public class Task4 implements Task {
    @Override
    public void showTask() {
        System.out.println("-----------------------------------------------");
        System.out.println("\n4. Сортировка обменами. Дана последовательность чисел a1, a2, ... , an." +
                " Требуется переставить числа в порядке\n возрастания. Для этого сравниваются два соседних " +
                "числа ai и a(i + 1) . Если ai > a(i + 1) , то делается перестановка. Так\n" +
                "продолжается до тех пор, пока все элементы не станут расположены в порядке возрастания. " +
                "Составить алгоритм\n сортировки, подсчитывая при этом количества перестановок.");
    }

    @Override
    public void runTask() {
        int[] a = Task.createArray();
        int temp = 0;
        int counter = 0;        // подсчет количества перестановок
        System.out.println("Array: " + Arrays.toString(a));

        for (int i = 0; i < a.length; i++){
            for (int j = 0; j < a.length - i - 1; j++){
                if (a[j] > a[j + 1]){
                    temp = a[j];
                    a[j] = a[j + 1];
                    a[j + 1] = temp;
                    counter++;
                }
            }
        }

        System.out.println("New array: " + Arrays.toString(a));
        System.out.println("Number of permutations: " + counter);
    }
}
