package by.training.bnm.arrays.sort;

import java.util.Arrays;

public class Task7 implements Task {
    public void showTask() {
        System.out.println("-----------------------------------------------");
        System.out.println("\n7. Пусть даны две неубывающие последовательности действительных чисел " +
                "a1 <= a2 <= ... <= an и b1 <= b2 <= ... <= bm.\n Требуется указать те места, на " +
                "которые нужно вставлять элементы последовательности b1 <= b2 <= ... <= bm в первую\n" +
                "последовательность так, чтобы новая последовательность оставалась возрастающей.");
    }

    public void runTask() {
        int[] a = Task.createArray();
        int[] b = Task.createArray();
        int n = a.length;
        int m = b.length;

        // sorting
        Task6.SortShell(a);
        Task6.SortShell(b);

        System.out.println("Array a: " + Arrays.toString(a));
        System.out.println("Array b: " + Arrays.toString(b));

        int i = 0;
        int j = 0;
        int k = 0;
        while (i < n + m - 1) {
            if (j >= m)
                j = m - 1;
            if (k >= n)
                k = n - 1;
            if (j >= n && b[j] > a[n - 1]) {
                System.out.println("b[" + j + "] after a[" + (n - 1) + "];");
                k++;
                i++;
                j++;
            } else if (b[j] < a[k]) {
                System.out.print("b[" + j + "] ");
                if (k > 0)
                    System.out.print("between a[" + (k - 1) + "] and ");
                else
                    System.out.print("before ");
                System.out.println("a[" + k + "]");
                j++;
                i++;
            } else {
                k++;
                i++;
            }
        }

    }

    public static void main(String[] args) {
        Task7 task7 = new Task7();
        task7.runTask();
    }
}
