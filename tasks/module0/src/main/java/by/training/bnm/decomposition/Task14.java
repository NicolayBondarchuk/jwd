package by.training.bnm.decomposition;

import java.util.Scanner;

public class Task14 implements Task {
    @Override
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n14. Написать метод(методы), определяющий, в каком из " +
                "данных двух чисел больше цифр.");
    }

    @Override
    public void runTask() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter number 1: ");
        long number1 = scanner.nextLong();
        System.out.print("Enter number 2: ");
        long number2 = scanner.nextLong();

        long maxDigit = maxDigit(number1, number2);

        System.out.println("Max digit is in " + maxDigit);
    }

    public long maxDigit(long number1, long number2) {
        // для определения длины воспользуемся методом из Task13
        int size1 = Task13.sizeNumber(number1);
        int size2 = Task13.sizeNumber(number2);
        if (size1 < size2)
            return number2;
        return number1;
    }
}
