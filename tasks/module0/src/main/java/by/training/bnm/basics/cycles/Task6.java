package by.training.bnm.basics.cycles;


import java.util.Scanner;

public class Task6 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n6. Напишите программу, где пользователь вводит любое целое " +
                "положительное число. А программа\n" +
                "суммирует все числа от 1 до введенного пользователем числа.");
    }

    public void runTask() {
        int number = 0;
        int i = 0;
        int sum = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter number: ");
        number = scanner.nextInt();

        while (i <= number) {
            sum += i;
            i++;
        }
        System.out.println("Sum = " + sum);
    }
}
