package by.training.bnm.arrays.arraysimple;

import java.util.Arrays;
import java.util.Scanner;

public class Task6 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n6. Дана последовательность чисел а1 , а2 ,..., ап . \n" +
                "Указать наименьшую длину числовой оси, содержащую все эти числа.");
    }

    public void runTask() {
        int[] a = Task1.createArray();
        int max = Integer.MIN_VALUE;
        int min = Integer.MAX_VALUE;
        Scanner scanner = new Scanner(System.in);

        for (int i = 0; i < a.length; i++) {
            if (min > a[i])
                min = a[i];
            if (max < a[i])
                max = a[i];
        }

        System.out.println("Array: " + Arrays.toString(a));

        System.out.println("Those numbers are of the numerical axis between "
                + min + " and " + max);

    }
}
