package by.training.bnm.arrays.arraysimple;

import java.util.Arrays;
import java.util.Scanner;

public class Task9 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n9. Даны действительные числа а1, а2, ..., аn. Поменять \n" +
                "местами наибольший и наименьший элементы.");
    }

    public void runTask() {
        int[] a = Task1.createArray();
        int max = Integer.MIN_VALUE;
        int min = Integer.MAX_VALUE;
        int numberMax = 0;
        int numberMin = 0;
        Scanner scanner = new Scanner(System.in);

        // заполнение массива
        for (int i = 0; i < a.length; i++) {
            if (min > a[i]) {
                min = a[i];
                numberMin = i;
            }
            if (max < a[i]) {
                max = a[i];
                numberMax = i;
            }
        }
        System.out.println("Array: " + Arrays.toString(a));

        int temp = a[numberMax];
        a[numberMax] = a[numberMin];
        a[numberMin] = temp;

        System.out.println("max = " + max + "; min = " + min);
        System.out.println("New array: " + Arrays.toString(a));

    }
}
