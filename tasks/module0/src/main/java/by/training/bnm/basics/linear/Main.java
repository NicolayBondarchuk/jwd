package by.training.bnm.basics.linear;

import by.training.bnm.basics.branching.Task;
import by.training.bnm.basics.branching.Task1;
import by.training.bnm.basics.branching.Task10;
import by.training.bnm.basics.branching.Task11;
import by.training.bnm.basics.branching.Task12;
import by.training.bnm.basics.branching.Task13;
import by.training.bnm.basics.branching.Task14;
import by.training.bnm.basics.branching.Task2;
import by.training.bnm.basics.branching.Task3;
import by.training.bnm.basics.branching.Task4;
import by.training.bnm.basics.branching.Task5;
import by.training.bnm.basics.branching.Task6;
import by.training.bnm.basics.branching.Task7;
import by.training.bnm.basics.branching.Task8;
import by.training.bnm.basics.branching.Task9;

public class Main {

    public static void main(String[] args) {
        Main main = new Main();
        for (int i = 0; i < main.getTask().length; i++) {
            main.getTask(i).showTask();
            main.getTask(i).runTask();
        }

    }

    private Task[] task;

    public Main() {
        task = new Task[14];
        task[0] = new Task1();
        task[1] = new Task2();
        task[2] = new Task3();
        task[3] = new Task4();
        task[4] = new Task5();
        task[5] = new Task6();
        task[6] = new Task7();
        task[7] = new Task8();
        task[8] = new Task9();
        task[9] = new Task10();
        task[10] = new Task11();
        task[11] = new Task12();
        task[12] = new Task13();
        task[13] = new Task14();
    }

    public Task[] getTask() {
        return task;
    }

    public Task getTask(int index) {
        return task[index];
    }
}
