package by.training.bnm.basics.cycles;


public class Task3 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n3. Необходимо вывести на экран таблицу умножения на 3:");
    }

    public void runTask() {
        int result = 0;
        for (int i = 0; i < 10; i++) {
            result = 3 * (i + 1);
            System.out.println("3 * " + (i + 1) + " = " + result);
        }
    }
}
