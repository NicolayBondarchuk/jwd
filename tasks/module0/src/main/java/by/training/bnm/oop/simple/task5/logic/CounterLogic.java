package by.training.bnm.oop.simple.task5.logic;

import by.training.bnm.oop.simple.task5.data.Counter;

public class CounterLogic {
    private Counter counter;

    public CounterLogic() {
    }

    public CounterLogic(Counter counter) {
        this.counter = counter;
    }

    public Counter getCounter() {
        return counter;
    }

    public void setCounter(Counter counter) {
        this.counter = counter;
    }

    // увеличение значения
    public void increase() {
        counter.setCount(counter.getCount() + 1);
    }

    // уменьшение значения
    public void decrease() {
        counter.setCount(counter.getCount() - 1);
    }
}
