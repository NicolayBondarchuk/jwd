package by.training.bnm.oop.simple.task3;

public class Student {
    private String Name;
    private int groupNumber;
    private int[] progress;

    public Student() {
    }

    public Student(String name, int group, int[] prg) {
        Name = name;
        groupNumber = group;
        progress = new int[5];
        System.arraycopy(prg, 0, progress, 0, 5);
    }

    public String getName() {
        return Name;
    }

    public int getGroupNumber() {
        return groupNumber;
    }

    public int[] getProgress() {
        return progress;
    }

    public void setName(String name) {
        Name = name;
    }

    public void setGroupNumber(int groupNumber) {
        this.groupNumber = groupNumber;
    }

    public void setProgress(int[] progress) {
        this.progress = progress;
    }
}
