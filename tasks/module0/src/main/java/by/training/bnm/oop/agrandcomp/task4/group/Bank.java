package by.training.bnm.oop.agrandcomp.task4.group;

import by.training.bnm.oop.agrandcomp.task4.data.Account;

import java.util.Arrays;

public class Bank {
    private Account[] accounts;

    public Bank() {
    }

    public Bank(Account[] accounts) {
        this.accounts = accounts;
    }

    public Account[] getAccounts() {
        return accounts;
    }

    public void setAccount(Account account, int index) {
        accounts[index] = account;
    }

    public Account getAccount(int index) {
        return accounts[index];
    }

    public void setAccounts(Account[] accounts) {
        this.accounts = accounts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bank bank = (Bank) o;
        return Arrays.equals(accounts, bank.accounts);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(accounts);
    }
}
