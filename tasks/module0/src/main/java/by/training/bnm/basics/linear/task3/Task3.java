package by.training.bnm.basics.linear.task3;

/*
3. Найдите значение функции: z = 2 * x + ( y – 2 ) * 5.
 */


import by.training.bnm.basics.linear.task.Task;

import java.util.Scanner;

public class Task3 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n3. Найдите значение функции: z = 2 * x + ( y – 2 ) * 5.");
    }

    public void runTask() {
        int x = 0;
        int y = 0;
        System.out.println("Function is: z = 2 * x + ( y – 2 ) * 5");
        Scanner in = new Scanner(System.in);
        System.out.print("Enter x: ");
        x = in.nextInt();
        System.out.print("Enter y: ");
        y = in.nextInt();
        System.out.println("z = " + (2 * x + (y - 2) * 5));
    }
}
