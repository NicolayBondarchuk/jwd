package by.training.bnm.decomposition;

public interface Task {
    public void showTask();
    public void runTask();
}
