package by.training.bnm.decomposition;

import java.util.Arrays;
import java.util.Scanner;

public class Task13 implements Task {
    @Override
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n13. Дано натуральное число N. Написать метод(методы) " +
                "для формирования массива, элементами которого являются\n" +
                "цифры числа N.");
    }

    @Override
    public void runTask() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter N: ");
        long N = scanner.nextLong();

        // находим разрядность числа N
        int size = sizeNumber(N);

        // разбиваем число N на составные числа и заносим их в массив
        int[] arrayN = createArrayFromNumber(N, size);
        System.out.println("Array from number N is " + Arrays.toString(arrayN));
    }

    // находим разрядность числа N
    public static int sizeNumber(long number) {
        int size = 1;
        while (number / 10 != 0) {
            size++;
            number = number / 10;
        }
        return size;
    }

    // разбиваем число N на составные числа и заносим их в массив
    public static int[] createArrayFromNumber(long number, int size) {
        int[] array = new int[size];
        while (size > 0) {
            // заполняем массив с конца
            array[size - 1] = (int) (number % 10);
            size--;
            number /= 10;
        }
        return array;
    }
}
