package by.training.bnm.oop.agrandcomp.task5.group;

import by.training.bnm.oop.agrandcomp.task5.data.Tour;

public class Travel {
    Tour[] tours;

    public Travel() {
    }

    public Travel(Tour[] tours) {
        this.tours = tours;
    }

    public Tour[] getTours() {
        return tours;
    }

    public Tour getTour(int index) {
        return tours[index];
    }

    public void setTours(Tour[] tours) {
        this.tours = tours;
    }

    public void setTour(int index, Tour tour) {
        this.tours[index] = tour;
    }



}
