package by.training.bnm.basics.branching;

import java.util.Scanner;

public class Task10 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n10. Составить программу, которая определит площадь какого круга меньше.");
    }

    public void runTask() {
        double R1 = 0;
        double R2 = 0;
        double S1 = 0;
        double S2 = 0;
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter R1 for circle 1: ");
        R1 = scanner.nextDouble();
        System.out.print("Enter R2 for circle 2: ");
        R2 = scanner.nextDouble();

        S1 = Math.PI * R1 * R1;
        S2 = Math.PI * R2 * R2;

        if (S1 < S2)
            System.out.println("The first circle area is less than second");
        else if (S1 > S2)
            System.out.println("The second circle area is less than first");
        else
            System.out.println("The circles is equals");
    }
}
