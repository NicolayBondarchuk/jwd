package by.training.bnm.oop.basics.task1;

/*
 * Задача 1.
 * Создать объект класса Текстовый файл, используя классы Файл, Директория.
 * Методы: создать, переименовать, вывести на консоль содержимое, дополнить, удалить.
 */

import by.training.bnm.oop.basics.task1.controller.FileCommandFactory;
import by.training.bnm.oop.basics.task1.controller.FileController;
import by.training.bnm.oop.basics.task1.controller.SimpleFileCommandFactory;
import by.training.bnm.oop.basics.task1.model.Directory;
import by.training.bnm.oop.basics.task1.model.File;
import by.training.bnm.oop.basics.task1.service.ActionFile;
import by.training.bnm.oop.basics.task1.service.ChangeFile;
import by.training.bnm.oop.basics.task1.service.ChangeText;

public class Main {
    public static void main(String[] args) {

        // создаем директорию
        Directory myDocuments = new Directory("My Documents");
        ChangeText changeText = new ChangeText();
        ActionFile actionFile = new ChangeFile(myDocuments);
        FileCommandFactory fileCommandFactory = new SimpleFileCommandFactory(changeText, actionFile);
        FileController fileController = new FileController(fileCommandFactory);

        // создаем файл
        fileController.output("-n", "First File");
        fileController.output("-n", "Second File");
        fileController.output("-n", "3th File");
        fileController.output("-n", "4th File");
        fileController.output("-n", "5th File");
        fileController.output("-n", "6th File");
        // печатаем размер
        System.out.println("size of directory: " + myDocuments.getSizeOfDirectory());

        // меняем имя файла с именем "First File"
        fileController.output("-r", "First File");

        // заполняем значениями
        int i = 1;
        for (File file:
             myDocuments.getFiles()) {
            changeText.setFile(myDocuments.getFile((i - 1)));
            fileController.output("-t", "Hello, my name is " + i + "th File");
            i++;
        }

        // выборочно выводим на печать
        changeText.setFile(myDocuments.getFile(2));
        fileController.output("-s", "3th File");
        changeText.setFile(myDocuments.getFile(4));
        fileController.output("-s", "5th File");
    }
}
