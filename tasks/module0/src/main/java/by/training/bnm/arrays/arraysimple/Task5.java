package by.training.bnm.arrays.arraysimple;

import java.util.Arrays;
import java.util.Scanner;

public class Task5 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n5. Дана последовательность натуральных чисел а1 , а2 ,..., ап." +
                " Создать массив из четных чисел этой последовательности.\n" +
                "Если таких чисел нет, то вывести сообщение об этом факте.");
    }

    public void runTask() {
        int[] a = Task1.createArrayNatural();
        int counter = 0;
        Scanner scanner = new Scanner(System.in);

        System.out.println("Array: " + Arrays.toString(a));

        for (int i = 0; i < a.length; i++) {
            if (a[i] % 2 == 0)
                counter++;
        }
        if (counter == 0) {
            System.out.println("The sequence doesn't have even numbers");
        } else {
            int j = 0;
            int[] evenNumbers = new int[counter];
            for (int i = 0; i < a.length; i++) {
                if (a[i] % 2 == 0) {
                    evenNumbers[j] = a[i];
                    j++;
                }
            }
            System.out.println("Even numbers - they are: " + Arrays.toString(evenNumbers));
        }

    }
}
