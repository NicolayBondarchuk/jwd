package by.training.bnm.basics.branching;

import java.util.Scanner;

public class Task5 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n5. Составить программу: определения наименьшего из двух чисел а и b.");
    }

    public void runTask() {
        int a = 0;
        int b = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a: ");
        a = scanner.nextInt();
        System.out.print("Enter b: ");
        b = scanner.nextInt();

        if (a < b)
            System.out.println(a + " is the smallest number");
        else if (a > b)
            System.out.println(b + " is the smallest number");
        else
            System.out.println(a + " and " + b + " are equal");

    }
}
