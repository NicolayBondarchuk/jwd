package by.training.bnm.decomposition;

import java.util.Scanner;

public class Task12 implements Task {
    @Override
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n12. Даны числа X, Y, Z, Т — длины сторон четырехугольника. Написать " +
                "метод(методы) вычисления его площади,\n если угол между сторонами длиной X и Y — прямой.");
    }

    @Override
    public void runTask() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter X: ");
        double X = scanner.nextDouble();
        System.out.print("Enter Y: ");
        double Y = scanner.nextDouble();
        System.out.print("Enter Z: ");
        double Z = scanner.nextDouble();
        System.out.print("Enter T: ");
        double T = scanner.nextDouble();

        // площадь считаем как сумму площадей двух треугольников


        // площадь прямогольного треугольника
        double areaTriangle1 = calcAreaOfRightTriangle(X, Y);

        // площадь треугольника с известными сторонами
        // находим недостающую сторону
        double XY = calcHypotenuse(X, Y);
        // применяем статический метод из Task1
        double areaTriangle2 = Task1.calcTriangleArea(XY, Z, T) + calcAreaOfRightTriangle(X, Y);

        // общая площадь
        double area = areaTriangle1 + areaTriangle2;
        System.out.println("The area of quadrangle is " + area);

    }

    // метод вычисления гипотенузы прямоугольного треугольника
    public double calcHypotenuse(double a, double b) {
        double result = 0.0;
        result = Math.sqrt(a * a + b * b);
        return result;
    }

    // метод вычисления площади прямоугольного треугольника
    public double calcAreaOfRightTriangle(double a, double b) {
        return (a * b / 2);
    }
}
