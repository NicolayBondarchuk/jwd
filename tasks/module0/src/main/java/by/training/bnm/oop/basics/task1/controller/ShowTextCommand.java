package by.training.bnm.oop.basics.task1.controller;

import by.training.bnm.oop.basics.task1.service.ActionText;

public class ShowTextCommand implements FileCommand {

    ActionText actionText;

    public ShowTextCommand(ActionText actionText) {
        this.actionText = actionText;
    }

    @Override
    public void execute(String name) {
        actionText.showText();
    }
}
