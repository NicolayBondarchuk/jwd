package by.training.bnm.decomposition;

import java.util.Scanner;

public class Task4 implements Task {
    @Override
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n4. Написать метод(методы) для нахождения наименьшего общего кратного " +
                "трех натуральных чисел.");
    }

    @Override
    public void runTask() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter number 1: ");
        int number1 = scanner.nextInt();
        System.out.print("Enter number 2: ");
        int number2 = scanner.nextInt();
        System.out.print("Enter number 3: ");
        int number3 = scanner.nextInt();

        int nok = NOK(number1, number2, number3);
        System.out.println("NOK of three numbers is " + nok);
    }

    public int NOK(int number1, int number2, int number3) {
        Task2 task2 = new Task2();
        int nok12 = task2.NOK(number3, number2);
        int nok = task2.NOK(nok12, number1);
        return nok;
    }
}
