package by.training.bnm.basics.cycles;

import java.util.Scanner;

public class Task7 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n7. Вычислить значения функции на отрезке [а,b] c шагом h:");
        System.out.println("y = x, if x > 2;");
        System.out.println("y = -x, if x <= 2;");
    }

    public void runTask() {
        int a = 0;
        int b = 0;
        int h = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a: ");
        a = scanner.nextInt();
        System.out.print("Enter b: ");
        b = scanner.nextInt();
        System.out.print("Enter h: ");
        h = scanner.nextInt();

        while (a < b) {
            if (a > 2)
                System.out.println("y = " + a);
            else
                System.out.println(" y = " + ((-1) * a));
            a += h;
        }

    }
}
