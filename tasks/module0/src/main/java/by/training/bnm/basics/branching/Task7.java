package by.training.bnm.basics.branching;

import java.util.Scanner;

public class Task7 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n7. Составить программу нахождения модуля выражения " +
                "a*x*x + b*x + c при заданных значениях a, b, c и х");
    }

    public void runTask() {
        int a = 0;
        int b = 0;
        int c = 0;
        int x = 0;
        int result = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a: ");
        a = scanner.nextInt();
        System.out.print("Enter b: ");
        b = scanner.nextInt();
        System.out.print("Enter c: ");
        c = scanner.nextInt();
        System.out.print("Enter x: ");
        x = scanner.nextInt();

        result = a * x * x + b * x + c;
        if (result < 0)
            result *= (-1);
        System.out.println("Result = " + result);
    }
}
