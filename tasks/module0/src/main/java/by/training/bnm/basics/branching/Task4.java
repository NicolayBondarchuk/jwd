package by.training.bnm.basics.branching;

import java.util.Scanner;

public class Task4 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n4. Составить программу: равны ли два числа а и b?");
    }

    public void runTask() {
        int a = 0;
        int b = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a: ");
        a = scanner.nextInt();
        System.out.print("Enter b: ");
        b = scanner.nextInt();
        if (a == b)
            System.out.println(a + " == " + b);
        else
            System.out.println(a + " != " + b);
    }
}
