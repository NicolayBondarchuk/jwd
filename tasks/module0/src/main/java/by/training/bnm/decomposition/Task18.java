package by.training.bnm.decomposition;

public class Task18 implements Task {
    @Override
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n18. Найти все натуральные n-значные числа, цифры в которых" +
                " образуют строго возрастающую последовательность\n (например, 1234, 5789)." +
                " Для решения задачи использовать декомпозицию.");
    }

    @Override
    public void runTask() {
        // все числа четырехзначные
        final int size = 4;
        final int begin = 1000;
        final int end = 10000;
        int j = 0;
        System.out.println("The numbers are: ");
        for (int i = begin; i < end; i++) {
            // разбиваем число на массив символов
            int[] array = Task13.createArrayFromNumber(i, size);
            // проверяем массив на возрастание
            if (isIncreaseArray(array)) {
                j++;
                System.out.print(i + ", ");
                if (j % 10 == 0)
                    System.out.println();
            }
        }
        System.out.println();
    }

    // проверка массива на возрастание
    public boolean isIncreaseArray(int[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            if (array[i] >= array[i + 1])
                return false;
        }
        return true;
    }
}
