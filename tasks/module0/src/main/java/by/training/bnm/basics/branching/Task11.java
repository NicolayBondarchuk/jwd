package by.training.bnm.basics.branching;

import java.util.Scanner;

public class Task11 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n11. Составить программу, которая определит площадь какого треугольника больше.");
    }

    public void runTask() {

        System.out.print("Enter triangle 1:");
        double sTriangle1 = calculateArea();
        System.out.print("Enter triangle 2:");
        double sTriangle2 = calculateArea();

        if (sTriangle1 > sTriangle2)
            System.out.println("The area of triangle 1 is bigger than area of triangle 2");
        else if (sTriangle1 < sTriangle2)
            System.out.println("The area of triangle 2 is bigger than area of triangle 1");
        else
            System.out.println("The area of the triangles are equal");

    }

    private double calculateArea() {
        double x1 = 0;
        double y1 = 0;
        double x2 = 0;
        double y2 = 0;
        double x3 = 0;
        double y3 = 0;
        double Square = 0;
        Scanner in = new Scanner(System.in);

        System.out.println("Enter coordinates:");

        System.out.print("Enter x1: ");
        x1 = in.nextDouble();
        System.out.print("Enter y1: ");
        y1 = in.nextDouble();
        System.out.print("Enter x2: ");
        x2 = in.nextDouble();
        System.out.print("Enter y2: ");
        y2 = in.nextDouble();
        System.out.print("Enter x3: ");
        x3 = in.nextDouble();
        System.out.print("Enter y3: ");
        y3 = in.nextDouble();

        Square = 1.0 / 2.0 * ((x1 - x3) * (y2 - y3) - (x2 - x3) * (y1 - y3));
        if (Square < 0)
            Square = -1 * Square;
        return Square;
    }
}
