package by.training.bnm.oop.basics.task5a.view;

import by.training.bnm.oop.basics.task5a.data.FlowerComp;

public class FlowerCompView {

  public void showFlowerComp(FlowerComp flowerComp) {
    System.out.println(flowerComp.toString());
  }
}
