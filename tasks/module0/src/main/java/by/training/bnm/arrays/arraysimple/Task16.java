package by.training.bnm.arrays.arraysimple;

import java.util.Arrays;

public class Task16 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n16. Даны действительные числа a1, a2, ..., an. Найти\n" +
                "max( a1 + a2n , a2 + a(2n − 1) , ..., an + a(n + 1) ) .");
    }

    public void runTask() {
        int[] a = Task1.createArray();
        int max = Integer.MIN_VALUE;
        int temp = 0;
        int size = a.length / 2;

        System.out.println("Array: " + Arrays.toString(a));

        if (a.length % 2 == 0) {
            for (int i = 0; i < size; i++) {
                temp = a[i] + a[2 * size - 1 - i];
                if (max < temp)
                    max = temp;
            }
        } else {
            for (int i = 0; i < size; i++) {
                temp = a[i] + a[2 * size - 1 - i];
                if (max < temp)
                    max = temp;
            }
            temp = a[size + 1];
            if (max < temp)
                max = temp;
        }

        System.out.println("max = " + max);
    }
}
