package by.training.bnm.oop.basics.task1.controller;

public interface FileCommandFactory {
    FileCommand getCommand(String commandName);
}
