package by.training.bnm.decomposition;

import java.util.Scanner;

public class Task19 implements Task {
    @Override
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n19. Написать программу, определяющую сумму n - значных чисел, " +
                "содержащих только нечетные цифры. Определить\n также, сколько четных цифр в" +
                " найденной сумме. Для решения задачи использовать декомпозицию.");
    }

    @Override
    public void runTask() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter n: ");
        int n = scanner.nextInt();

        // определяем границы области выборки данных
        int begin = (int)Math.pow(10, n);
        int end = (int)Math.pow(10, n + 1);

        int sum = 0;
        for (int i = begin; i < end; i++) {
            // разбиваем число на массив символов
            int[] array = Task13.createArrayFromNumber(i, n);

            // выделяем числа из выборки с нечетными числами в составе
            if (isOddNumbers(array))
                sum += i;
        }

        System.out.println("Sum is " + sum);

        // находим разрядность числа sum
        int size = Task13.sizeNumber(sum);
        // разбиваем число на массив символов
        int[] array = Task13.createArrayFromNumber(sum, size);
        // выделяем числа из выборки с четными числами в составе
        // и считаем их
        int evenNumbers = allEvenNumbers(array);

        System.out.println("All even numbers of 'Sum' is " + evenNumbers);

    }

    // проверка массива на все нечетные числа в составе
    public boolean isOddNumbers(int[] array) {
        for (int number : array) {
            if (number % 2 == 0)
                return false;
        }
        return true;
    }

    // подсчет всех четных чисел в массиве
    public int allEvenNumbers(int[] array) {
        int counter = 0;
        for (int number : array) {
            if (number % 2 == 0)
                counter++;
        }
        return counter;
    }
}
