package by.training.bnm.basics.linear.task7;

import by.training.bnm.basics.linear.task.Task;

import java.util.Scanner;

/*
Дан прямоугольник, ширина которого в два раза меньше длины. Найти площадь прямоугольника
 */
public class Task7 implements Task {

    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n7. Дан прямоугольник, ширина которого в два раза меньше длины. " +
                "Найти площадь прямоугольника");
    }

    public void runTask() {
        int width = 0;
        Scanner in = new Scanner(System.in);

        System.out.print("Enter rectangle width: ");
        width = in.nextInt();

        System.out.println("Rectangle square is " +
                width * 2 * width);
    }
}
