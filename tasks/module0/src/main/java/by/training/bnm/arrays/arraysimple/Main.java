package by.training.bnm.arrays.arraysimple;

public class Main {

    private Task[] task;

    public Main() {
        task = new Task[19];
        task[0] = new Task1();
        task[1] = new Task2();
        task[2] = new Task3();
        task[3] = new Task4();
        task[4] = new Task5();
        task[5] = new Task6();
        task[6] = new Task7();
        task[7] = new Task8();
        task[8] = new Task9();
        task[9] = new Task10();
        task[10] = new Task11();
        task[11] = new Task12();
        task[12] = new Task13();
        task[13] = new Task14();
        task[14] = new Task15();
        task[15] = new Task16();
        task[16] = new Task17();
        task[17] = new Task19();
        task[18] = new Task20();
    }

    public Task[] getTask() {
        return task;
    }

    public Task getTask(int index) {
        return task[index];
    }

    public static void main(String[] args) {
        Main main = new Main();
        for (int i = 0; i < main.getTask().length; i++) {
            main.getTask(i).showTask();
            main.getTask(i).runTask();
        }

    }
}
