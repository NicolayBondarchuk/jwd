package by.training.bnm.arrays.sort;

import java.util.Arrays;
import java.util.Scanner;

public class Task1 implements Task {
    public void showTask() {
        System.out.println("-----------------------------------------------");
        System.out.println("\n1. Заданы два одномерных массива с различным количеством элементов \n" +
                "и натуральное число k. Объединить их  в один массив, включив \n" +
                "второй массив между k-м и (k+1) - м элементами первого, при \n" +
                "этом не используя дополнительный массив.");
    }

    public void runTask() {
        System.out.println("Enter parameters for first array:");
        int[] array1 = Task.createArray();
        System.out.println("Array 1: " + Arrays.toString(array1));

        System.out.println("Enter parameters for second array:");
        int[] array2 = Task.createArray();
        System.out.println("Array 2: " + Arrays.toString(array2));

        int k = 0;
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter k: ");
        k = scanner.nextInt();
        int m = array1.length;
        int n = array2.length;


        int j = 0;
        int d = 0;
        int temp = 0;
        for (int i = 0; i < m; i++) {
            if (i >= k && j < n) {
                temp = array1[i];
                array1[i] = array2[j];
                array2[j] = temp;
                j++;
            } else if (k + j <= i) {
                array1[i] = array2[d];
                d++;
            }
        }

        System.out.println("New array 1: " + Arrays.toString(array1));

    }
}

