package by.training.bnm.arrays.arraysimple;

import java.util.Arrays;

public class Task17 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n17. Дана последовательность целых чисел a1, a2, ..., an.\n" +
                " Образовать новую последовательность, выбросив из исходной\n" +
                "те члены, которые равны min(a1, a2, ..., an).");
    }

    public void runTask() {
        int[] a = Task1.createArray();
        int min = Integer.MAX_VALUE;
        // counter of minimal numbers
        int counter = 0;

        System.out.println("Array: " + Arrays.toString(a));

        for (int i = 0; i < a.length; i++) {
            if (min == a[i]) {
                counter++;
            }
            if (min > a[i]) {
                min = a[i];
                counter = 1;
            }
        }
        System.out.println("min = " + min);

        int[] newArray = new int[a.length - counter];
        int j = 0;
        for (int i = 0; i < a.length; i++) {
            if (a[i] != min) {
                newArray[j] = a[i];
                j++;
            }
        }

        System.out.println("New array is " + Arrays.toString(newArray));
    }
}
