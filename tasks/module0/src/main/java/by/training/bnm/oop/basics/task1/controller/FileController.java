package by.training.bnm.oop.basics.task1.controller;

public class FileController {
    private FileCommandFactory fileCommandFactory;

    public FileController(FileCommandFactory fileCommandFactory) {
        this.fileCommandFactory = fileCommandFactory;
    }

    public void output(String command, String text) {
        FileCommand fileCommand = fileCommandFactory.getCommand(command);
        fileCommand.execute(text);

    }
}
