package by.training.bnm.arrays.sort;

import java.util.Arrays;
import java.util.Scanner;

public class Task3 implements Task {
    @Override
    public void showTask() {
        System.out.println("-----------------------------------------------");
        System.out.println("\n3. Сортировка выбором. Дана последовательность чисел a1, a2, ... , an. " +
                "Требуется переставить элементы так,\n чтобы они были расположены по убыванию. Для " +
                "этого в массиве, начиная с первого, выбирается наибольший элемент\n" +
                "и ставится на первое место, а первый - на место наибольшего. Затем, начиная со второго," +
                " эта процедура повторяется.\nНаписать алгоритм сортировки выбором.");
    }

    @Override
    public void runTask() {
        int[] a = Task.createArray();
        System.out.println("Array: " + Arrays.toString(a));
        int temp = Integer.MIN_VALUE;
        int max_index = 0;

        for (int i = 0; i < a.length; i++){
            for (int j = i; j < a.length; j++){
                if (temp < a[j]){
                    temp = a[j];
                    max_index = j;
                }
            }
            temp = a[i];
            a[i] = a[max_index];
            a[max_index] = temp;
            temp = Integer.MIN_VALUE;
        }
        System.out.println("Array after sort: " + Arrays.toString(a));

    }
}
