package by.training.bnm.oop.basics.task1.model;

import java.util.LinkedList;

public class Directory {
  private String name;
  private LinkedList<File> files;

  public Directory(String name) {
    this.name = name;
    files = new LinkedList<File>();
  }

  public Directory() {
    this.name = "new directory";
    files = new LinkedList<File>();
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public File getFiles(String name) {
    for (File file : files) {
      if (name.equals(file.getName()))
        return file;
    }
    return null;
  }

  // доступ по индексу
  public File getFile(int index) {
    return files.get(index);
  }

  public LinkedList<File> getFiles() {
    return files;
  }

  public int getSizeOfDirectory() {
    return files.size();
  }


  public void setFiles(LinkedList<File> files) {
    this.files = files;
  }

}
