package by.training.bnm.oop.basics.task1.controller;

import by.training.bnm.oop.basics.task1.service.ActionFile;
import by.training.bnm.oop.basics.task1.service.ActionText;

public class SimpleFileCommandFactory implements FileCommandFactory {

    ActionText actionText;
    ActionFile actionFile;

    public SimpleFileCommandFactory(ActionText actionText, ActionFile actionFile) {
        this.actionText = actionText;
        this.actionFile = actionFile;
    }

    @Override
    public FileCommand getCommand(String commandName) {

        final CommandName fileCommandName = CommandName.fromString(commandName);

        switch (fileCommandName) {
            case NEW_FILE:
                return new CreateFileCommand(actionFile);
            case RENAME:
                return new RenameFileCommand(actionFile);
            case ADD_TEXT:
                return new AddTextCommand(actionText);
            case SHOW_FILE:
                return new ShowTextCommand(actionText);
            case REMOVE:
                return new RemoveFileCommand(actionFile);
        }
        return null;
    }
}
