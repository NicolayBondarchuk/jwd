package by.training.bnm.oop.basics.task1.model;

public class File {

  private String name;
  private String content;
  private boolean readOnly;


  public File() {
  }

  public File(String name) {
    this.name = name;
  }

  public File(String name, String content) {
    this.name = name;
    this.content = content;
    this.readOnly = false;
  }

  public File(String name, String content, boolean readOnly) {
    this.name = name;
    this.content = content;
    this.readOnly = readOnly;
  }

  public String getName() {
    return name;
  }

  public String getFile() {
    return name + "." + content;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public boolean isReadOnly() {
    return readOnly;
  }

  public void setReadOnly(boolean readOnly) {
    this.readOnly = readOnly;
  }

  @Override
  public String toString() {
    return "File{" +
            "'" + name + '\'' +
            ", extension='" + content + '\'' +
            ", readOnly=" + readOnly +
            '}';
  }
}
