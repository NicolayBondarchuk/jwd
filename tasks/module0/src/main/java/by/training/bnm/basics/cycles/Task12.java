package by.training.bnm.basics.cycles;

import java.util.Scanner;

public class Task12 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n17. Даны действительное (а) и натуральное (n). вычислить: a(a+1)...(a+n-1)");
    }

    public void runTask() {
        int n = 0;
        int i = 0;
        double a = 0;
        double sum = 0;
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter a: ");
        a = scanner.nextInt();
        System.out.print("Enter n: ");
        n = scanner.nextInt();

        while (i < n) {
            sum += a + i;
            i++;
        }

        System.out.println("Sum = " + sum);
    }
}
