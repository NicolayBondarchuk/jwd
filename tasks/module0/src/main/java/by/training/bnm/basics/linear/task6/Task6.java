package by.training.bnm.basics.linear.task6;

/*
Написать код для решения задачи. В n малых бидонах 80 л молока. Сколько литров молока в m больших бидонах,
если в каждом большом бидоне на 12 л. больше, чем в малом?
 */

import by.training.bnm.basics.linear.task.Task;

import java.util.Scanner;

public class Task6 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n6. Написать код для решения задачи. В n малых бидонах 80 л молока. " +
                        "Сколько литров молока в m больших бидонах, если в каждом большом " +
                "бидоне на 12 л. больше, чем в малом?");
    }

    public void runTask() {
        int n = 0;
        int m = 0;
        final int n_liter = 80;
        Scanner in = new Scanner(System.in);

        System.out.print("Enter n: ");
        n = in.nextInt();

        System.out.print("Enter m: ");
        m = in.nextInt();

        System.out.println("Milk in " + m + " containers: " +
                (m *((double)n_liter / n + 12)));
    }
}
