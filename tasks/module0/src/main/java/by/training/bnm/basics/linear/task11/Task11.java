package by.training.bnm.basics.linear.task11;

import by.training.bnm.basics.linear.task.Task;

import java.util.Scanner;

/*
Вычислить периметр и площадь прямоугольного треугольника по длинам а и b двух катетов.
 */
public class Task11 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n11. Вычислить периметр и площадь прямоугольного треугольника" +
                " по длинам а и b двух катетов.");
    }

    public void runTask() {
        double a = 0;
        double b = 0;
        double perimeter = 0;
        double area = 0;
        Scanner in = new Scanner(System.in);

        System.out.print("Enter a: ");
        a = in.nextDouble();

        System.out.print("Enter b: ");
        b = in.nextDouble();

        perimeter = (a + b + (Math.sqrt(a * a + b * b)));
        System.out.println("Perimeter of the right triangle is " +
                perimeter);
        area = ((a * b) / 2);
        System.out.println("Square of the right triangle is " +
                area);
    }
}
