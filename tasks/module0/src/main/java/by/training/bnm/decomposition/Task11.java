package by.training.bnm.decomposition;

import java.util.Arrays;
import java.util.Scanner;

public class Task11 implements Task {
    @Override
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n11. Задан массив D. Определить следующие суммы: D[1] + D[2] + D[3]; " +
                "D[3] + D[4] + D[5]; D[4] +D[5] +D[6].\nПояснение. Составить метод(методы) для " +
                "вычисления суммы трех последовательно расположенных элементов\n" +
                "массива с номерами от k до m.");
    }

    @Override
    public void runTask() {
        // используем статический метод для создания массива
        int[] D = by.training.bnm.arrays.sort.Task.createArray();
        System.out.println("D[" + D.length + "]: " + Arrays.toString(D));

        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter k: ");
        int k = scanner.nextInt();
        System.out.print("Enter m: ");
        int m = scanner.nextInt();

        int[] sum = sumThreeArray(D, k, m);
        System.out.println("Sum = " + Arrays.toString(sum));

    }

    // метод вычисления суммы трех последовательно расположенных элементов
    private int[] sumThreeArray(int[] d, int k, int m) {
        int size = (m - k) / 2 + 1;
        int[] result = new int[size];

        int sum = 0;
        // счетчик для d[]
        int i = k;
        // счетчик для result[]
        int j = 0;
        while(i + 2 < m) {
            sum += d[i] + d[i + 1] + d[i + 2];
            i += 2;
            result[j] = sum;
            j++;
            sum = 0;
        }
        if (i - 2 <= m) {
            i = m - 2;
            sum += d[i] + d[i + 1] + d[i + 2];
            result[j] = sum;
        }
        return result;
    }
}
