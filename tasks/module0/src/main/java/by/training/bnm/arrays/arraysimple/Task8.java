package by.training.bnm.arrays.arraysimple;

import java.util.Arrays;
import java.util.Scanner;

public class Task8 implements Task {
    public void showTask() {
        System.out.println("------------------------------------------------------");
        System.out.println("\n8. Дан массив действительных чисел, размерность которого N.\n" +
                "Подсчитать, сколько в нем отрицательных, положительных и нулевых элементов.");
    }

    public void runTask() {
        int[] a = Task1.createArray();
        int counterNegative = 0;
        int counterPositive = 0;
        int counterNull= 0;
        Scanner scanner = new Scanner(System.in);

        System.out.println("Array: " + Arrays.toString(a));

        for (int i = 0; i < a.length; i++) {
            if (a[i] < 0)
                counterNegative++;
            else if (a[i] > 0)
                counterPositive++;
            else
                counterNull++;
        }

        System.out.println("Negative elements: " + counterNegative);
        System.out.println("Positive elements: " + counterPositive);
        System.out.println("Null elements: " + counterNull);

    }
}
