package by.training.bnm.entity.passengers;

public class SeatCar extends PassengerTrainCar {

    {
        setTrainWeight(23000);
        setNumberOfPlaces(96);
    }

    public SeatCar() {
        setPassengers(((int) (Math.random() * getNumberOfPlaces())));
        setBaggage((int)(Math.random() * 10 * getPassengers()));
    }

    @Override
    public String toString() {
        return "Seat Car{" +
                " Id=" + getId() +
                ", numberOfPlaces=" + getNumberOfPlaces() +
                ", passengers=" + getPassengers() +
                ", baggage=" + getBaggage() +
                ", trainWeight=" + getTrainWeight() +
                '}';
    }
}
