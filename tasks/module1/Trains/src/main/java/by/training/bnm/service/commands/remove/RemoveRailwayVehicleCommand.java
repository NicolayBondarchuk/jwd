package by.training.bnm.service.commands.remove;

import by.training.bnm.entity.RailwayVehicle;
import by.training.bnm.service.commands.Command;
import by.training.bnm.dao.repository.TrainCarService;
import org.apache.log4j.Logger;

import java.util.Map;

public class RemoveRailwayVehicleCommand implements Command {
    private final static Logger LOGGER = Logger.getLogger(RemoveRailwayVehicleCommand.class);

    private TrainCarService trainCarService;
    private Map<String, String> commands;

    public RemoveRailwayVehicleCommand(TrainCarService trainCarService, Map<String, String> commands) {
        this.trainCarService = trainCarService;
        this.commands = commands;
    }

    private RailwayVehicle parseExchange() {
        String idString = commands.get("trainCarIdToDelete");
        int id = Integer.parseInt(idString);
        for (RailwayVehicle rv : trainCarService.findAllTrainCars()) {
            if (id == rv.getId()) {
                return rv;
            }
        }
        return new RailwayVehicle() {
            @Override
            public int getId() {
                return 0;
            }

            @Override
            public void setId(int id) {
            }
        };
    }

    @Override
    public void execute() {
        trainCarService.removeRailwayVehicle(parseExchange());
    }
}
