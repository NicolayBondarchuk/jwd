package by.training.bnm.service.commands;

public interface WebCommand {
    void execute();
    String writeResponse();
    String message();
}
