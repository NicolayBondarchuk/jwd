package by.training.bnm.entity.baggage;

import by.training.bnm.entity.TrainCar;

public class BaggageCar extends TrainCar {
    int volume;
    {
        volume = 5000;
        setTrainWeight(25000);
    }

    @Override
    public String toString() {
        return "Baggage Car{" +
                "id=" + getId() +
                ", volume=" + volume +
                ", trainWeight=" + getTrainWeight() +
                '}';
    }
}
