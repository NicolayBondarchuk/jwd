package by.training.bnm.service.commands.parsing;

import com.sun.net.httpserver.HttpExchange;
import org.apache.log4j.Logger;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

public class GetParse implements Parsing {
    private final static Logger LOGGER = Logger.getLogger(GetParse.class);

    private HttpExchange exchange;

    public GetParse(HttpExchange exchange) {
        this.exchange = exchange;
    }

    @Override
    public Map<String, String> parseRequest() {
        Map<String, String> get = new HashMap<>();

        URI requestURI = exchange.getRequestURI();
        String query = requestURI.getQuery();

        if (query == null) {
            return get;
        }

        LOGGER.info("Request URI: " + requestURI.toString());
        LOGGER.info("query: " + query);

        String[] temp = query.split("&");

        for (String t : temp) {
            String[] array = t.split("=");
            get.put(array[0], array[1]);
        }

        return get;
    }

}
