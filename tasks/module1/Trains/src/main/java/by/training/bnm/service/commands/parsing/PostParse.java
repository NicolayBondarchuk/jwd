package by.training.bnm.service.commands.parsing;

import com.sun.net.httpserver.HttpExchange;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class PostParse implements Parsing {
    private final static Logger LOGGER = Logger.getLogger(PostParse.class);

    private HttpExchange exchange;

    public PostParse(HttpExchange exchange) {
        this.exchange = exchange;
    }

    @Override
    public Map<String, String> parseRequest() {
        URI requestURI = exchange.getRequestURI();
        String requestMethod = exchange.getRequestMethod();

        String postCommand = "";
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(exchange.getRequestBody()))) {
            postCommand = URLDecoder.decode(new BufferedReader(new InputStreamReader(exchange.getRequestBody(),
                    StandardCharsets.UTF_8)).lines().collect(Collectors.joining()), StandardCharsets.UTF_8.name());
        } catch (UnsupportedEncodingException e) {
            LOGGER.error("unsupportedEncodingException " + e.getMessage());
        } catch (IOException e) {
            LOGGER.error("ioexception " + e.getMessage());
        } catch (Exception e) {
            LOGGER.error("exception " + e.getMessage());
        }
        LOGGER.info("Received incoming request: " + requestMethod);
        LOGGER.info("Request URI: " + requestURI.toString());
        LOGGER.info("Received: " + postCommand);

        String[] temp = postCommand.split("&");

        Map<String, String> post = new HashMap<>();
        for (String t : temp) {
            String[] array = t.split("=");
            // null checking
            if (array.length > 1) {
                post.put(array[0], array[1]);
            } else {
                post.put(array[0], "");
            }
        }


        return post;
    }
}
