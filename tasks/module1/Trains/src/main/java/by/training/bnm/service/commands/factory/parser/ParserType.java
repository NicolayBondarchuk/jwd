package by.training.bnm.service.commands.factory.parser;

import com.sun.net.httpserver.HttpExchange;

import java.net.URI;

public enum ParserType {
    GET,
    POST,
    NULL;

    public static ParserType getParserType(HttpExchange exchange) {
        String command = exchange.getRequestMethod();
        switch (command) {
            case "POST":
                return POST;
            case "GET":
                return GET;
            default:
                return NULL;
        }
    }
}
