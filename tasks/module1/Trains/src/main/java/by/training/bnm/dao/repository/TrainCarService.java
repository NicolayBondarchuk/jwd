package by.training.bnm.dao.repository;


import by.training.bnm.entity.RailwayVehicle;

import java.util.List;

public interface TrainCarService {
    List<RailwayVehicle> findAllTrainCars();
    boolean addRailwayVehicle(RailwayVehicle vehicle);
    public boolean removeRailwayVehicle(RailwayVehicle vehicle);
}
