package by.training.bnm.service.commands.factory.handle;

import by.training.bnm.dao.repository.TrainCarService;
import by.training.bnm.entity.RailwayVehicle;
import by.training.bnm.service.commands.calculate.AllBaggage;
import by.training.bnm.service.commands.get.*;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class GetFactory {
    private TrainCarService trainCarService;
    private final static Logger LOGGER = Logger.getLogger(AllBaggage.class);

    public GetFactory(TrainCarService trainCarService) {
        this.trainCarService = trainCarService;
    }

    public Display getShowClass(String command) {
        if (command == null) {
            command = "";
        }
        switch (command) {
            case "DISPLAY_ALL":
                LOGGER.info("new command DISPLAY_ALL");
                return new DisplayAll(trainCarService);
            case "DISPLAY_LOCOMOTIVES":
                LOGGER.info("new command DISPLAY_LOCOMOTIVES");
                return new DisplayLocomotives(trainCarService);
            case "DISPLAY_TRAIN_CARS":
                LOGGER.info("new command DISPLAY_TRAIN_CARS");
                return new DisplayTrainCars(trainCarService);
            case "SORT_BY_ID":
                LOGGER.info("new command SORT_BY_ID");
                return new SortById(trainCarService);
            case "SORT_BY_TRAIN_WEIGHT":
                LOGGER.info("new command SORT_BY_TRAIN_WEIGHT");
                return new SortByTrainWeight(trainCarService);
            case "SORT_BY_PASSENGERS":
                LOGGER.info("new command SORT_BY_TRAIN_PASSENGERS");
                return new SortByPassengers(trainCarService);
            case "SORT_BY_BAGGAGE":
                LOGGER.info("new command SORT_BY_BAGGAGE");
                return new SortByBaggage(trainCarService);
            default:
                LOGGER.info("new command default");
                return new Display() {
                    @Override
                    public List<RailwayVehicle> showVehicles() {
                        return new ArrayList<>();
                    }
                };
        }
    }
}
