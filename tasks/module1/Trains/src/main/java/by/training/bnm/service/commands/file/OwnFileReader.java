package by.training.bnm.service.commands.file;

import org.apache.log4j.Logger;

import java.io.*;

public class OwnFileReader {
    private static final Logger LOGGER = Logger.getLogger(OwnFileReader.class);
    String fileName;
    String problemWithFile;


    public OwnFileReader(String fileName) {
        this.fileName = fileName;
    }

    public String readFromFile() {
        StringBuilder stringBuilder = new StringBuilder();
        byte[] buffer = new byte[100];
        InputStream in = null;
        File file = new File(fileName);
        try {
            in = new FileInputStream(file);
            while (in.read(buffer) != -1) {
                String str = new String(buffer);
                stringBuilder.append(str);
                buffer = new byte[100];
            }
            problemWithFile = "file was written successful";
            LOGGER.info("file was written successful");
        } catch (FileNotFoundException e) {
            LOGGER.error(e.getMessage());
            problemWithFile = "file wasn't found";
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
            problemWithFile = e.getMessage();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    LOGGER.error("cannot close stream: " + e.getMessage());
                    throw new IllegalStateException("Cannot close stream", e);
                }
            }
        }
        String out = new String(stringBuilder);

        return out;
    }

    public String[] commandsFromFile() {
        String commands = readFromFile();
        String[] arrayCommands = commands.split("\\p{Space}");
        return arrayCommands;
    }

    public String getMessage() {
        return problemWithFile;
    }
}
