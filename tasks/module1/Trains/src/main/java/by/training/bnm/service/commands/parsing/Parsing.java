package by.training.bnm.service.commands.parsing;

import java.io.UnsupportedEncodingException;
import java.util.Map;

public interface Parsing {
    Map<String,String> parseRequest() throws UnsupportedEncodingException;
}
