package by.training.bnm.service.commands.get;

import by.training.bnm.dao.repository.TrainCarService;
import by.training.bnm.entity.RailwayVehicle;
import by.training.bnm.entity.TrainCar;
import by.training.bnm.entity.locomotive.Locomotive;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SortByTrainWeight implements Display {
    private TrainCarService trainCarService;

    public SortByTrainWeight(TrainCarService trainCarService) {
        this.trainCarService = trainCarService;
    }

    @Override
    public List<RailwayVehicle> showVehicles() {
        List<RailwayVehicle> listLocomotives = new ArrayList<>();
        for (RailwayVehicle railwayVehicle : trainCarService.findAllTrainCars()) {
            if (railwayVehicle instanceof Locomotive) {
                listLocomotives.add(railwayVehicle);
            }
        }
        Collections.sort(listLocomotives, new Comparator<RailwayVehicle>() {
            public int compare(RailwayVehicle rv1, RailwayVehicle rv2) {
                if (rv1.getId() > rv2.getId()) {
                    return 1;
                } else if (rv1.getId() < rv2.getId()) {
                    return -1;
                } else {
                    return 0;
                }
            }
        });

        List<RailwayVehicle> listTrainCars = new ArrayList<>();
        for (RailwayVehicle railwayVehicle : trainCarService.findAllTrainCars()) {
            if (railwayVehicle instanceof TrainCar) {
                listTrainCars.add(railwayVehicle);
            }
        }
        Collections.sort(listTrainCars, new Comparator<RailwayVehicle>() {
            public int compare(RailwayVehicle rv1, RailwayVehicle rv2) {
                if (((TrainCar) rv1).getTrainWeight() > ((TrainCar) rv2).getTrainWeight()) {
                    return 1;
                } else if (((TrainCar) rv1).getTrainWeight() < ((TrainCar) rv2).getTrainWeight()) {
                    return -1;
                } else {
                    return 0;
                }
            }
        });
        listLocomotives.addAll(listTrainCars);
        return listLocomotives;
    }
}
