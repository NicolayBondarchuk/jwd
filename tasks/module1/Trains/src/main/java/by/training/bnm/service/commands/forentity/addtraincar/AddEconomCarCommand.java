package by.training.bnm.service.commands.forentity.addtraincar;

import by.training.bnm.entity.TrainCar;
import by.training.bnm.entity.passengers.EconomicCar;
import by.training.bnm.service.commands.Command;
import by.training.bnm.dao.repository.TrainCarService;

public class AddEconomCarCommand implements Command {
    private TrainCarService service;
    private TrainCar trainCar;

    public AddEconomCarCommand(TrainCarService service) {
        this.service = service;
        this.trainCar = new EconomicCar();
    }

    @Override
    public void execute() {
        service.addRailwayVehicle(trainCar);
    }
}
