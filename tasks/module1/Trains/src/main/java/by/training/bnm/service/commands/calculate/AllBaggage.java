package by.training.bnm.service.commands.calculate;

import by.training.bnm.dao.repository.TrainCarService;
import by.training.bnm.entity.RailwayVehicle;
import by.training.bnm.entity.passengers.PassengerTrainCar;
import org.apache.log4j.Logger;

public class AllBaggage implements Calculate {
    private final static Logger LOGGER = Logger.getLogger(AllBaggage.class);

    private TrainCarService trainCarService;
    private int allBaggage;

    public AllBaggage(TrainCarService trainCarService) {
        this.trainCarService = trainCarService;
    }

    private void calculateAllBaggage() {
        for (RailwayVehicle rv: trainCarService.findAllTrainCars()) {
            if (rv instanceof PassengerTrainCar) {
                allBaggage += ((PassengerTrainCar) rv).getBaggage();
            }
        }
        LOGGER.info("calculate baggage: result = " + allBaggage);
    }

    @Override
    public String getCalcResult() {
        calculateAllBaggage();
        return String.valueOf(allBaggage);
    }
}
