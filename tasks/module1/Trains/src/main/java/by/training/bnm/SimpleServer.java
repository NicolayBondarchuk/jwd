package by.training.bnm;

import by.training.bnm.controller.SimpleHandler;
import by.training.bnm.dao.repository.InMemoryTrainCarService;
import by.training.bnm.dao.repository.TrainCarService;
import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SimpleServer {

    public static void main(String[] args) throws IOException {


        InetSocketAddress address = new InetSocketAddress("localhost", 59090);
        HttpServer server = HttpServer.create(address, 0);



        TrainCarService trainCarService = new InMemoryTrainCarService();
        SimpleHandler simpleHandler = new SimpleHandler(trainCarService);

        HttpContext serverContext = server.createContext("/trains", simpleHandler);

        ExecutorService executorService = Executors.newFixedThreadPool(4);
        server.setExecutor(executorService);
        server.start();

        System.out.println("Server started on " + server.getAddress().toString());
    }
}
