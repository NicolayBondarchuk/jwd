package by.training.bnm.service.commands.forentity.addtraincar;

import by.training.bnm.entity.TrainCar;
import by.training.bnm.entity.passengers.SeatCar;
import by.training.bnm.service.commands.Command;
import by.training.bnm.dao.repository.TrainCarService;

public class AddSeatCarCommand implements Command {
    private TrainCarService service;
    private TrainCar trainCar;

    public AddSeatCarCommand(TrainCarService service) {
        this.service = service;
        this.trainCar = new SeatCar();
    }

    @Override
    public void execute() {
        service.addRailwayVehicle(trainCar);
    }
}
