package by.training.bnm.service.commands;

public interface Command {
    void execute();
}
