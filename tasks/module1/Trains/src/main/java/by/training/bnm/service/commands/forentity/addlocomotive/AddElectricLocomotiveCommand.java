package by.training.bnm.service.commands.forentity.addlocomotive;

import by.training.bnm.entity.locomotive.ElectricLocomotive;
import by.training.bnm.entity.locomotive.Locomotive;
import by.training.bnm.service.commands.Command;
import by.training.bnm.dao.repository.TrainCarService;

public class AddElectricLocomotiveCommand implements Command {
    private TrainCarService service;
    private Locomotive locomotive;

    public AddElectricLocomotiveCommand(TrainCarService service) {
        this.service = service;
        this.locomotive = new ElectricLocomotive();
    }

    @Override
    public void execute() {
        service.addRailwayVehicle(locomotive);
    }
}
