package by.training.bnm.entity;

public interface RailwayVehicle {
    int getId();
    void setId(int id);
    String toString();

}
