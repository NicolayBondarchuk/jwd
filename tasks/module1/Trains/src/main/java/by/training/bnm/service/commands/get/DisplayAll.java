package by.training.bnm.service.commands.get;

import by.training.bnm.dao.repository.TrainCarService;
import by.training.bnm.entity.RailwayVehicle;

import java.util.List;

public class DisplayAll implements Display {
    private TrainCarService trainCarService;

    public DisplayAll(TrainCarService trainCarService) {
        this.trainCarService = trainCarService;
    }


    @Override
    public List<RailwayVehicle> showVehicles() {
        List<RailwayVehicle> vehicles = trainCarService.findAllTrainCars();
        return vehicles;
    }
}
