package by.training.bnm.service.commands.factory.handle;

import by.training.bnm.dao.repository.TrainCarService;
import by.training.bnm.service.commands.Command;
import by.training.bnm.service.commands.NoCommand;
import by.training.bnm.service.commands.factory.train.CommandFactory;
import by.training.bnm.service.commands.factory.train.TrainsCommand;
import by.training.bnm.service.commands.file.ReadFileCommand;
import by.training.bnm.service.commands.remove.RemoveRailwayVehicleCommand;
import org.apache.log4j.Logger;

import java.util.Map;


public class PostFactory {
    private static Logger LOGGER = Logger.getLogger(PostFactory.class);

    private TrainCarService trainCarService;
    private Map<String, String> commands;
    private CommandFactory commandFactory;

    public PostFactory(TrainCarService trainCarService,
                       Map<String, String> commands, CommandFactory commandFactory) {
        this.trainCarService = trainCarService;
        this.commands = commands;
        this.commandFactory = commandFactory;
    }

    public Command getCommand(String command) {
        switch (command) {
            case "delete":
                LOGGER.info("create remove command");
                return new RemoveRailwayVehicleCommand(trainCarService, commands);
            case "addFile":
                LOGGER.info("create add file command");
                return new ReadFileCommand(commands, commandFactory);
            case "AddElectricLocomotive":
            case "AddDieselLocomotive":
            case "AddBaggageCar":
            case "AddCompartmentCar":
            case "AddDiningCar":
            case "AddEconomCar":
            case "AddPassengerCar":
            case "AddSeatCar":
                return new CommandFactory(trainCarService).getTrainCar(TrainsCommand.getTrainCommand(command));
            default:
                LOGGER.info("create no command");
                return new NoCommand();
        }
    }
}
