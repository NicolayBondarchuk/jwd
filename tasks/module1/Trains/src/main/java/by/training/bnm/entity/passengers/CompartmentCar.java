package by.training.bnm.entity.passengers;

public class CompartmentCar extends PassengerTrainCar {
    {
        setTrainWeight(22000);
        setNumberOfPlaces(36);
    }

    public CompartmentCar() {
        setPassengers(((int) (Math.random() * getNumberOfPlaces())));
        setBaggage((int)(Math.random() * 10 * getPassengers()));
    }

    @Override
    public String toString() {
        return "Compartment Car{" +
                " Id=" + getId() +
                ", numberOfPlaces=" + getNumberOfPlaces() +
                ", passengers=" + getPassengers() +
                ", baggage=" + getBaggage() +
                ", trainWeight=" + getTrainWeight() +
                '}';
    }
}
