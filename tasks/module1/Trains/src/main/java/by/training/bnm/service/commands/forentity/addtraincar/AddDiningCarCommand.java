package by.training.bnm.service.commands.forentity.addtraincar;

import by.training.bnm.entity.TrainCar;
import by.training.bnm.entity.dining.DiningCar;
import by.training.bnm.service.commands.Command;
import by.training.bnm.dao.repository.TrainCarService;

public class AddDiningCarCommand implements Command {
    private TrainCarService service;
    private TrainCar trainCar;

    public AddDiningCarCommand(TrainCarService service) {
        this.service = service;
        this.trainCar = new DiningCar();
    }

    @Override
    public void execute() {
        service.addRailwayVehicle(trainCar);
    }
}
