package by.training.bnm.service.commands.file;

import by.training.bnm.service.commands.Command;
import by.training.bnm.service.commands.factory.train.CommandFactory;
import by.training.bnm.service.commands.factory.train.TrainsCommand;
import org.apache.log4j.Logger;

import java.util.Arrays;
import java.util.Map;


public class ReadFileCommand implements Command {
    private final static Logger LOGGER = Logger.getLogger(ReadFileCommand.class);

    private CommandFactory commandFactory;
    private Map<String, String> commands;
    private OwnFileReader fileReader;

    public ReadFileCommand(Map<String, String> commands, CommandFactory commandFactory) {
        this.commands = commands;
        this.commandFactory = commandFactory;

        LOGGER.info("fileName = " + commands.get("path"));
        fileReader = new OwnFileReader(commands.get("path"));
    }

    private void createTrain() {

        String[] fileData = fileReader.commandsFromFile();
        LOGGER.info(Arrays.toString(fileData));
        LOGGER.info("length array " + fileData.length);
        TrainsCommand trainsCommand;
        Command command;
        for (String fd : fileData) {
            trainsCommand = TrainsCommand.getTrainCommand(fd);
            command = commandFactory.getTrainCar(trainsCommand);
            command.execute();
        }
    }

    public String getMessage() {
        return fileReader.getMessage();
    }

    @Override
    public void execute() {
        createTrain();
    }


}
