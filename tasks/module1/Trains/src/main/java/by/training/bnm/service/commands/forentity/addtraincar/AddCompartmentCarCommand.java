package by.training.bnm.service.commands.forentity.addtraincar;

import by.training.bnm.entity.TrainCar;
import by.training.bnm.entity.passengers.CompartmentCar;
import by.training.bnm.service.commands.Command;
import by.training.bnm.dao.repository.TrainCarService;

public class AddCompartmentCarCommand implements Command {
    private TrainCarService service;
    private TrainCar trainCar;

    public AddCompartmentCarCommand(TrainCarService service) {
        this.service = service;
        this.trainCar = new CompartmentCar();
    }

    @Override
    public void execute() {
        service.addRailwayVehicle(trainCar);
    }
}
