package by.training.bnm.entity.passengers;

public class EconomicCar extends PassengerTrainCar {

    {
        setTrainWeight(25000);
        setNumberOfPlaces(50);
    }

    public EconomicCar() {
        setPassengers(((int) (Math.random() * getNumberOfPlaces())));
        setBaggage((int)(Math.random() * 10 * getPassengers()));
    }

    @Override
    public String toString() {
        return "Economic Car{" +
                " Id=" + getId() +
                ", numberOfPlaces=" + getNumberOfPlaces() +
                ", passengers=" + getPassengers() +
                ", baggage=" + getBaggage() +
                ", trainWeight=" + getTrainWeight() +
                '}';
    }
}
