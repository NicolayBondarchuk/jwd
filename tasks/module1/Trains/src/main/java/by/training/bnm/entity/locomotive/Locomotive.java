package by.training.bnm.entity.locomotive;

import by.training.bnm.entity.RailwayVehicle;

public abstract class Locomotive implements RailwayVehicle {
    private int Id;
    private int power;

    public Locomotive(int power) {
        this.power = power;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getPower() {
        return power;
    }

}

