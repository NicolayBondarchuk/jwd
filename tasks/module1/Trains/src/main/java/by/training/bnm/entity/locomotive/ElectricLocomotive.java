package by.training.bnm.entity.locomotive;


public class ElectricLocomotive extends Locomotive {

    public ElectricLocomotive() {
        super(600000);
    }

    @Override
    public String toString() {
        return "Electric Locomotive{" +
                "id=" + getId() +
                ", power=" + getPower() +
                '}';
    }
}
