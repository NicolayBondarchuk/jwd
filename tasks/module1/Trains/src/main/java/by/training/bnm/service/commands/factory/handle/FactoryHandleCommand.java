package by.training.bnm.service.commands.factory.handle;

import by.training.bnm.dao.repository.TrainCarService;
import by.training.bnm.service.commands.WebCommand;
import by.training.bnm.service.commands.factory.train.CommandFactory;
import by.training.bnm.service.commands.forhandle.GetCommand;
import by.training.bnm.service.commands.forhandle.NoWebCommand;
import by.training.bnm.service.commands.forhandle.PostCommand;
import com.sun.net.httpserver.HttpExchange;
import org.apache.log4j.Logger;

import java.io.UnsupportedEncodingException;


public class FactoryHandleCommand {
    private final static Logger LOGGER = Logger.getLogger(FactoryHandleCommand.class);

    private FactoryHandleCommand() {}

    public static WebCommand getHandleCommand(HttpExchange exchange,
                                              CommandFactory factory,
                                              TrainCarService trainCarService) throws UnsupportedEncodingException {
        String requestMethod = exchange.getRequestMethod();
        switch (requestMethod) {
            case "GET":
                LOGGER.info("call GET-web-command");
                return new GetCommand(exchange, trainCarService);
            case "POST":
                LOGGER.info("call POST-web-command");
                return new PostCommand(exchange, factory, trainCarService);
            default:
                LOGGER.info("call NO-web-command");
                return new NoWebCommand();
        }
    }
}
