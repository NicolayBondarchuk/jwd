package by.training.bnm.service.commands.forentity.addlocomotive;

import by.training.bnm.entity.locomotive.DieselLocomotive;
import by.training.bnm.entity.locomotive.Locomotive;
import by.training.bnm.service.commands.Command;
import by.training.bnm.dao.repository.TrainCarService;

public class AddDieselLocomotiveCommand implements Command {

    private TrainCarService service;
    private Locomotive locomotive;

    public AddDieselLocomotiveCommand(TrainCarService service) {
        this.service = service;
        this.locomotive = new DieselLocomotive();
    }

    @Override
    public void execute() {
        service.addRailwayVehicle(locomotive);
    }
}
