package by.training.bnm.service.commands.forhandle;

import by.training.bnm.dao.repository.TrainCarService;
import by.training.bnm.entity.RailwayVehicle;
import by.training.bnm.service.commands.WebCommand;
import by.training.bnm.service.commands.factory.parser.ParserFactory;
import by.training.bnm.service.commands.get.Display;
import by.training.bnm.service.commands.factory.handle.GetFactory;
import by.training.bnm.service.commands.parsing.Parsing;
import com.sun.net.httpserver.HttpExchange;
import org.apache.log4j.Logger;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class GetCommand implements WebCommand {
    private final static Logger LOGGER = Logger.getLogger(GetCommand.class);

    private List<RailwayVehicle> railwayVehicles;
    private Map<String, String> commands;
    private GetFactory displayFactory;

    public GetCommand(HttpExchange exchange, TrainCarService trainCarService) throws UnsupportedEncodingException {
        this.railwayVehicles = new ArrayList<>();
        this.displayFactory = new GetFactory(trainCarService);

        Parsing parser = ParserFactory.getParser(exchange);
        commands = parser.parseRequest();
    }

    @Override
    public void execute() {
        Display display = displayFactory.getShowClass(commands.get("commandName"));
        railwayVehicles.addAll(display.showVehicles());
    }

    @Override
    public String writeResponse() {
        StringBuilder template = new StringBuilder();
        template.append("<lu>");
        for (RailwayVehicle rv : railwayVehicles) {
            template.append("<li>");
            template.append("<form action=\"/trains\" method=\"POST\">\n" +
                    "\t<input type=\"hidden\" name=\"commandName\" value=\"delete\"/>\n" +
                    "\t<input type=\"hidden\" name=\"trainCarIdToDelete\" value=\"" + rv.getId() + "\"/>\n" +
                    "\t<input type=\"submit\" value=\"Delete\" />\n" +
                    "</form>");
            template.append(rv.toString());
            template.append("</li>");
        }
        template.append("</lu>");
        return new String(template);
    }

    @Override
    public String message() {
        return "";
    }

}
