package by.training.bnm.entity;

import java.util.Comparator;

public abstract class TrainCar implements RailwayVehicle {
    private int Id;
    private int trainWeight;

    public TrainCar() {

    }

    public void setTrainWeight(int trainWeight) {
        this.trainWeight = trainWeight;
    }

    public int getTrainWeight() {
        return trainWeight;
    }

    @Override
    public int getId() {
        return Id;
    }

    @Override
    public void setId(int id) {
        Id = id;
    }

}
