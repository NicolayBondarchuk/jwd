package by.training.bnm.entity.locomotive;

public class DieselLocomotive extends Locomotive {

    public DieselLocomotive() {
        super(500000);
    }

    @Override
    public String toString() {
        return "Diesel Locomotive{" +
                "id=" + getId() +
                ", power=" + getPower() +
                '}';
    }
}
