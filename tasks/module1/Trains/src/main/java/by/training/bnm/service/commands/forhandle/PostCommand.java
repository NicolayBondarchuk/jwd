package by.training.bnm.service.commands.forhandle;

import by.training.bnm.dao.repository.TrainCarService;
import by.training.bnm.entity.RailwayVehicle;
import by.training.bnm.service.commands.Command;
import by.training.bnm.service.commands.NoCommand;
import by.training.bnm.service.commands.WebCommand;
import by.training.bnm.service.commands.factory.handle.PostFactory;
import by.training.bnm.service.commands.factory.parser.ParserFactory;
import by.training.bnm.service.commands.factory.train.CommandFactory;

import by.training.bnm.service.commands.file.ReadFileCommand;
import by.training.bnm.service.commands.parsing.Parsing;
import com.sun.net.httpserver.HttpExchange;
import org.apache.log4j.Logger;

import java.io.UnsupportedEncodingException;
import java.util.Map;


public class PostCommand implements WebCommand {
    private final static Logger LOGGER = Logger.getLogger(PostCommand.class);

    private TrainCarService trainCarService;
    private Command command;
    private PostFactory postFactory;
    private Map<String, String> commands;

    public PostCommand(HttpExchange exchange,
                       CommandFactory commandFactory,
                       TrainCarService trainCarService) throws UnsupportedEncodingException {
        this.trainCarService = trainCarService;
        this.command = new NoCommand();

        Parsing parsing = ParserFactory.getParser(exchange);
        commands = parsing.parseRequest();
        this.postFactory = new PostFactory(trainCarService, commands, commandFactory);
    }

    @Override
    public void execute() {

        command = postFactory.getCommand(commands.get("commandName"));
        command.execute();
    }

    @Override
    public String writeResponse() {
        StringBuilder template = new StringBuilder();
        template.append("<lu>");
        for (RailwayVehicle rv : trainCarService.findAllTrainCars()) {
            template.append("<li>");
            template.append("<form action=\"/trains\" method=\"POST\">\n" +
                    "\t<input type=\"hidden\" name=\"commandName\" value=\"delete\"/>\n" +
                    "\t<input type=\"hidden\" name=\"trainCarIdToDelete\" value=\"" + rv.getId() + "\"/>\n" +
                    "\t<input type=\"submit\" value=\"Delete\" />\n" +
                    "</form>");
            template.append(rv.toString());
            template.append("</li>");
        }
        template.append("</lu>");
        return new String(template);
    }

    @Override
    public String message() {
        if (command instanceof ReadFileCommand) {
            return ((ReadFileCommand) command).getMessage();
        }
        return "";
    }

}
