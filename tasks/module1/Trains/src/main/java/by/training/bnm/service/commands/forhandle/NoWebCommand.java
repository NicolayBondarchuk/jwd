package by.training.bnm.service.commands.forhandle;

import by.training.bnm.service.commands.WebCommand;

public class NoWebCommand implements WebCommand {
    @Override
    public void execute() {

    }

    @Override
    public String writeResponse() {
        return "";
    }

    @Override
    public String message() {
        return "";
    }
}
