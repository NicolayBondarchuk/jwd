package by.training.bnm.controller;

import by.training.bnm.service.commands.calculate.AllBaggage;
import by.training.bnm.service.commands.calculate.AllPassengers;
import by.training.bnm.service.commands.calculate.Calculate;
import by.training.bnm.service.commands.factory.handle.FactoryHandleCommand;
import by.training.bnm.service.commands.WebCommand;
import by.training.bnm.service.commands.factory.train.CommandFactory;
import by.training.bnm.dao.repository.TrainCarService;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import org.apache.log4j.Logger;

import java.io.*;
import java.text.MessageFormat;
import java.util.stream.Collectors;


public class SimpleHandler implements HttpHandler {
    private TrainCarService trainCarService;
    private CommandFactory commandFactory;

    private final static Logger LOGGER = Logger.getLogger(SimpleHandler.class);

    public SimpleHandler(TrainCarService trainCarService) {
        this.trainCarService = trainCarService;
        this.commandFactory = new CommandFactory(trainCarService);
    }

    @Override
    public void handle(HttpExchange exchange) throws IOException {

        WebCommand webCommand = FactoryHandleCommand.getHandleCommand(exchange, commandFactory, trainCarService);
        webCommand.execute();
        Calculate passengers = new AllPassengers(trainCarService);
        Calculate baggage = new AllBaggage(trainCarService);


        InputStream resourceAsStream = this.getClass().getResourceAsStream("/defaultTemplate.html");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(resourceAsStream));
        String template = bufferedReader.lines().collect(Collectors.joining());

        String write = webCommand.writeResponse();
        String message = webCommand.message();

        String view = MessageFormat.format(template, write, message, "",
                passengers.getCalcResult(), baggage.getCalcResult());
        OutputStream responseBody = exchange.getResponseBody();
        exchange.sendResponseHeaders(200, view.length());
        responseBody.write(view.getBytes());
        responseBody.flush();
        responseBody.close();

    }
}
