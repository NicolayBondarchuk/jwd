package by.training.bnm.service.commands.calculate;

public interface Calculate {
    String getCalcResult();
}
