package by.training.bnm.service.commands.forentity.addtraincar;

import by.training.bnm.entity.TrainCar;
import by.training.bnm.entity.baggage.BaggageCar;
import by.training.bnm.service.commands.Command;
import by.training.bnm.dao.repository.TrainCarService;

public class AddBaggageCarCommand implements Command {
    private TrainCarService service;
    private TrainCar trainCar;

    public AddBaggageCarCommand(TrainCarService service) {
        this.service = service;
        this.trainCar = new BaggageCar();
    }

    @Override
    public void execute() {
        service.addRailwayVehicle(trainCar);
    }
}
