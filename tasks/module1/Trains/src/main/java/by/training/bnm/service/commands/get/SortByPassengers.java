package by.training.bnm.service.commands.get;

import by.training.bnm.dao.repository.TrainCarService;
import by.training.bnm.entity.RailwayVehicle;
import by.training.bnm.entity.TrainCar;
import by.training.bnm.entity.locomotive.Locomotive;
import by.training.bnm.entity.passengers.PassengerTrainCar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SortByPassengers implements Display {
    private TrainCarService trainCarService;

    public SortByPassengers(TrainCarService trainCarService) {
        this.trainCarService = trainCarService;
    }

    @Override
    public List<RailwayVehicle> showVehicles() {
        List<RailwayVehicle> list = new ArrayList<>();
        for (RailwayVehicle railwayVehicle : trainCarService.findAllTrainCars()) {
            if (railwayVehicle instanceof PassengerTrainCar) {
                list.add(railwayVehicle);
            }
        }
        Collections.sort(list, new Comparator<RailwayVehicle>() {
            public int compare(RailwayVehicle rv1, RailwayVehicle rv2) {
                if (((PassengerTrainCar) rv1).getPassengers() > ((PassengerTrainCar) rv2).getPassengers()) {
                    return 1;
                } else if (((PassengerTrainCar) rv1).getPassengers() < ((PassengerTrainCar) rv2).getPassengers()) {
                    return -1;
                } else {
                    return 0;
                }
            }
        });

        List<RailwayVehicle> listTemp = new ArrayList<>();
        for (RailwayVehicle railwayVehicle : trainCarService.findAllTrainCars()) {
            if (!(railwayVehicle instanceof PassengerTrainCar)) {
                listTemp.add(railwayVehicle);
            }
        }

        Collections.sort(listTemp, new Comparator<RailwayVehicle>() {
            public int compare(RailwayVehicle rv1, RailwayVehicle rv2) {
                if (rv1.getId() > rv2.getId()) {
                    return 1;
                } else if (rv1.getId() < rv2.getId()) {
                    return -1;
                } else {
                    return 0;
                }
            }
        });
        list.addAll(listTemp);
        return list;
    }
}
