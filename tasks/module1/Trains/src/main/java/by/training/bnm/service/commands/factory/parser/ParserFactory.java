package by.training.bnm.service.commands.factory.parser;

import by.training.bnm.service.commands.parsing.GetParse;
import by.training.bnm.service.commands.parsing.Parsing;
import by.training.bnm.service.commands.parsing.PostParse;
import com.sun.net.httpserver.HttpExchange;

import java.util.HashMap;
import java.util.Map;

public class ParserFactory {

    public static Parsing getParser(HttpExchange exchange) {
        ParserType parserType = ParserType.getParserType(exchange);
        switch (parserType) {
            case GET:
                return new GetParse(exchange);
            case POST:
                return new PostParse(exchange);
            case NULL:
            default:
                return new Parsing() {
                    @Override
                    public Map<String, String> parseRequest() {
                        return new HashMap<>();
                    }
                };
        }
    }
}
