package by.training.bnm.entity.dining;

import by.training.bnm.entity.TrainCar;

public class DiningCar extends TrainCar {
    {
        setTrainWeight(32000);
    }

    public DiningCar() {
    }

    @Override
    public String toString() {
        return "Dining Car{" +
                " Id=" + getId() +
                ", trainWeight=" + getTrainWeight() +
                '}';
    }
}
