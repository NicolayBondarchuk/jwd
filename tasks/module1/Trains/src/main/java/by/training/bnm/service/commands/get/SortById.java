package by.training.bnm.service.commands.get;

import by.training.bnm.dao.repository.TrainCarService;
import by.training.bnm.entity.RailwayVehicle;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SortById implements Display {
    private TrainCarService trainCarService;

    public SortById(TrainCarService trainCarService) {
        this.trainCarService = trainCarService;
    }

    @Override
    public List<RailwayVehicle> showVehicles() {
        List<RailwayVehicle> list = new ArrayList<>();
        list.addAll(trainCarService.findAllTrainCars());
        Collections.sort(list, new Comparator<RailwayVehicle>() {
            public int compare(RailwayVehicle rv1, RailwayVehicle rv2) {
                if (rv1.getId() > rv2.getId()) {
                    return 1;
                } else if (rv1.getId() < rv2.getId()) {
                    return -1;
                } else {
                    return 0;
                }
            }
        });
        return list;
    }
}
