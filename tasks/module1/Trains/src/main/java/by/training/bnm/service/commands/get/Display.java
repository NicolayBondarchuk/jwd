package by.training.bnm.service.commands.get;

import by.training.bnm.entity.RailwayVehicle;

import java.util.List;

public interface Display {
    List<RailwayVehicle> showVehicles();
}
