package by.training.bnm.service.commands.get;

import by.training.bnm.dao.repository.TrainCarService;
import by.training.bnm.entity.RailwayVehicle;
import by.training.bnm.entity.TrainCar;

import java.util.ArrayList;
import java.util.List;

public class DisplayTrainCars implements Display {
    private TrainCarService trainCarService;

    public DisplayTrainCars(TrainCarService trainCarService) {
        this.trainCarService = trainCarService;
    }


    @Override
    public List<RailwayVehicle> showVehicles() {
        List<RailwayVehicle> vehicles = new ArrayList<>();
        for (RailwayVehicle r : trainCarService.findAllTrainCars()) {
            if (r instanceof TrainCar) {
                vehicles.add(r);
            }
        }
        return vehicles;
    }
}
