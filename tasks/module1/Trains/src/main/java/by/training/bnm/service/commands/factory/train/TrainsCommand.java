package by.training.bnm.service.commands.factory.train;

public enum TrainsCommand {
    ADD_ELECTRIC_LOCOMOTIVE,
    ADD_DIESEL_LOCOMOTIVE,
    ADD_BAGGAGE_CAR,
    ADD_COMPARTMENT_CAR,
    ADD_DINING_CAR,
    ADD_ECONOM_CAR,
    ADD_PASSENGER_CAR,
    ADD_SEAT_CAR,
    NULL_CAR;


    public static TrainsCommand getTrainCommand(String command) {
        switch (command) {
            case "AddElectricLocomotive":
                return ADD_ELECTRIC_LOCOMOTIVE;
            case "AddDieselLocomotive":
                return ADD_DIESEL_LOCOMOTIVE;
            case "AddBaggageCar":
                return ADD_BAGGAGE_CAR;
            case "AddCompartmentCar":
                return ADD_COMPARTMENT_CAR;
            case "AddDiningCar":
                return ADD_DINING_CAR;
            case "AddEconomCar":
                return ADD_ECONOM_CAR;
            case "AddPassengerCar":
                return ADD_PASSENGER_CAR;
            case "AddSeatCar":
                return ADD_SEAT_CAR;
        }
        return NULL_CAR;
    }

}
