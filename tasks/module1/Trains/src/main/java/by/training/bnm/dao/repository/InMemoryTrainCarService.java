package by.training.bnm.dao.repository;

import by.training.bnm.entity.RailwayVehicle;

import java.util.ArrayList;
import java.util.List;

public class InMemoryTrainCarService implements TrainCarService {

    private int id;
    private List<RailwayVehicle> train;

    public InMemoryTrainCarService() {
        id = 0;
        train = new ArrayList<>();
    }

    @Override
    public boolean addRailwayVehicle(RailwayVehicle vehicle) {
        id++;
        vehicle.setId(id);
        return train.add(vehicle);
    }

    @Override
    public boolean removeRailwayVehicle(RailwayVehicle vehicle) {
        return train.remove(vehicle);
    }

    @Override
    public List<RailwayVehicle> findAllTrainCars() {
        return train;
    }

}
