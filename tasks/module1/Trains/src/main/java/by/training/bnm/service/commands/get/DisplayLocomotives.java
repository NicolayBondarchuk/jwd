package by.training.bnm.service.commands.get;

import by.training.bnm.dao.repository.TrainCarService;
import by.training.bnm.entity.RailwayVehicle;
import by.training.bnm.entity.locomotive.Locomotive;

import java.util.ArrayList;
import java.util.List;

public class DisplayLocomotives implements Display {
    private TrainCarService trainCarService;

    public DisplayLocomotives(TrainCarService trainCarService) {
        this.trainCarService = trainCarService;
    }


    @Override
    public List<RailwayVehicle> showVehicles() {
        List<RailwayVehicle> vehicles = new ArrayList<>();
        for (RailwayVehicle r : trainCarService.findAllTrainCars()) {
            if (r instanceof Locomotive) {
                vehicles.add(r);
            }
        }
        return vehicles;
    }
}
