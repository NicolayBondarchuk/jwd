package by.training.bnm.service.commands.factory.train;

import by.training.bnm.service.commands.Command;
import by.training.bnm.service.commands.NoCommand;
import by.training.bnm.service.commands.forentity.addlocomotive.AddDieselLocomotiveCommand;
import by.training.bnm.service.commands.forentity.addlocomotive.AddElectricLocomotiveCommand;
import by.training.bnm.service.commands.forentity.addtraincar.*;
import by.training.bnm.dao.repository.TrainCarService;
import org.apache.log4j.Logger;

public class CommandFactory {
    private final static Logger LOGGER = Logger.getLogger(CommandFactory.class);

    private TrainCarService trainCarService;

    public CommandFactory(TrainCarService trainCarService) {
        this.trainCarService = trainCarService;
    }

    public Command getTrainCar(TrainsCommand trainsCommand) {
        switch (trainsCommand) {
            case ADD_DIESEL_LOCOMOTIVE:
                LOGGER.info("create Diesel Locomotive");
                return new AddDieselLocomotiveCommand(trainCarService);
            case ADD_ELECTRIC_LOCOMOTIVE:
                LOGGER.info("create Electric Locomotive");
                return new AddElectricLocomotiveCommand(trainCarService);
            case ADD_BAGGAGE_CAR:
                LOGGER.info("create Baggage Train Car");
                return new AddBaggageCarCommand(trainCarService);
            case ADD_COMPARTMENT_CAR:
                LOGGER.info("create Compartment Train Car");
                return new AddCompartmentCarCommand(trainCarService);
            case ADD_DINING_CAR:
                LOGGER.info("create Dining Train Car");
                return new AddDiningCarCommand(trainCarService);
            case ADD_ECONOM_CAR:
                LOGGER.info("create Econom Train Car");
                return new AddEconomCarCommand(trainCarService);
            case ADD_SEAT_CAR:
                LOGGER.info("create Seat Train Car");
                return new AddSeatCarCommand(trainCarService);
            default:
                LOGGER.info("no command");
                return new NoCommand();
        }
    }
}
