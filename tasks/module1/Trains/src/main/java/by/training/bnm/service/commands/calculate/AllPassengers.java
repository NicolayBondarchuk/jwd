package by.training.bnm.service.commands.calculate;

import by.training.bnm.dao.repository.TrainCarService;
import by.training.bnm.entity.RailwayVehicle;
import by.training.bnm.entity.passengers.PassengerTrainCar;
import org.apache.log4j.Logger;

public class AllPassengers implements Calculate {
    private final static Logger LOGGER = Logger.getLogger(AllBaggage.class);

    private TrainCarService trainCarService;
    private int allPassengers;

    public AllPassengers(TrainCarService trainCarService) {
        this.trainCarService = trainCarService;
    }

    private void calculateAllPassengers() {
        for (RailwayVehicle rv: trainCarService.findAllTrainCars()) {
            if (rv instanceof PassengerTrainCar) {
                allPassengers += ((PassengerTrainCar) rv).getPassengers();
            }
        }
        LOGGER.info("calculate passengers: result = " + allPassengers);
    }

    @Override
    public String getCalcResult() {
        calculateAllPassengers();
        return String.valueOf(allPassengers);
    }
}
