package by.training.bnm.entity.passengers;

import by.training.bnm.entity.TrainCar;

public class PassengerTrainCar extends TrainCar {

    private int numberOfPlaces;
    private int passengers;
    private int baggage;

    public int getNumberOfPlaces() {
        return numberOfPlaces;
    }

    public void setNumberOfPlaces(int numberOfPlaces) {
        this.numberOfPlaces = numberOfPlaces;
    }

    public int getPassengers() {
        return passengers;
    }

    public void setPassengers(int passengers) {
        this.passengers = passengers;
    }

    public int getBaggage() {
        return baggage;
    }

    public void setBaggage(int baggage) {
        this.baggage = baggage;
    }
}
