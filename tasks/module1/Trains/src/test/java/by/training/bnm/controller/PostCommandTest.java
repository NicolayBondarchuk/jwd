package by.training.bnm.controller;

import by.training.bnm.dao.repository.InMemoryTrainCarService;
import by.training.bnm.dao.repository.TrainCarService;
import by.training.bnm.entity.RailwayVehicle;
import by.training.bnm.entity.dining.DiningCar;
import by.training.bnm.service.commands.Command;
import by.training.bnm.service.commands.factory.handle.PostFactory;
import by.training.bnm.service.commands.factory.train.CommandFactory;
import by.training.bnm.service.commands.forentity.addlocomotive.AddElectricLocomotiveCommand;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;


public class PostCommandTest {
    CommandFactory commandFactory;
    TrainCarService trainCarService;
    Map<String, String> commands;
    Command command;
    PostFactory postFactory;

    @Before
    public void initTest() {
        commands = new HashMap<>();
        trainCarService = new InMemoryTrainCarService();
        commandFactory = new CommandFactory(trainCarService);
        postFactory = new PostFactory(trainCarService, commands, commandFactory);
    }

    @Test
    public void testAddCommand() {

        commands.put("commandName", "AddElectricLocomotive");
        command = postFactory.getCommand(commands.get("commandName"));

        // check to return necessary command
        assertEquals(true, (command instanceof AddElectricLocomotiveCommand));
    }

    @Test
    public void testAddFileGood() {
        commands.put("commandName", "addFile");
        commands.put("path", "./src/main/resources/createCommands.csv");
        command = postFactory.getCommand(commands.get("commandName"));
        command.execute();

        // check to be not empty dao after successful reading file
        assertEquals(true, trainCarService.findAllTrainCars().size() > 0);
    }

    @Test
    public void testAddFileWrong() {
        commands.put("commandName", "addFile");
        commands.put("path", "/main/resources/createCommands.csv");
        command = postFactory.getCommand(commands.get("commandName"));
        command.execute();

        // check to be not empty dao after not successful reading file
        assertEquals(true, trainCarService.findAllTrainCars().size() > 0);
    }

    @Test
    public void testExecuteCommand() {
        commands.put("commandName", "AddDiningCar");
        postFactory = new PostFactory(trainCarService, commands, commandFactory);
        command = postFactory.getCommand(commands.get("commandName"));
        command.execute();
        RailwayVehicle railwayVehicle = trainCarService.findAllTrainCars().get(0);

        // check to be not empty dao after executing add command
        assertEquals(true, (railwayVehicle instanceof DiningCar));

    }

    @After
    public void afterTest() {
        commands = null;
        trainCarService = null;
        commandFactory = null;
        postFactory = null;
    }
}
