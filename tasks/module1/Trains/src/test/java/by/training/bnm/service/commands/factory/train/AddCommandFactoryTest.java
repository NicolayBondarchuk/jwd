package by.training.bnm.service.commands.factory.train;

import by.training.bnm.service.commands.Command;
import by.training.bnm.service.commands.NoCommand;
import by.training.bnm.service.commands.forentity.addlocomotive.AddDieselLocomotiveCommand;
import by.training.bnm.service.commands.forentity.addlocomotive.AddElectricLocomotiveCommand;
import by.training.bnm.service.commands.forentity.addtraincar.*;
import by.training.bnm.dao.repository.InMemoryTrainCarService;
import by.training.bnm.dao.repository.TrainCarService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AddCommandFactoryTest {
    private Command equalCommand;
    private CommandFactory factory;
    private TrainCarService trainCarService;

    @Before
    public void initTest() {
        trainCarService = new InMemoryTrainCarService();
        factory = new CommandFactory(trainCarService);
    }

    @Test
    public void testAddDieselLocomotiveCommand() throws Exception {
        equalCommand = factory.getTrainCar(TrainsCommand.getTrainCommand("commandName=AddDieselLocomotive"));
        assertEquals(true, (equalCommand instanceof AddDieselLocomotiveCommand));
    }

    @Test
    public void testAddElectricLocomotiveCommand() throws Exception {
        equalCommand = factory.getTrainCar(TrainsCommand.getTrainCommand("commandName=AddElectricLocomotive"));
        assertEquals(true, (equalCommand instanceof AddElectricLocomotiveCommand));
    }

    @Test
    public void testAddBaggageCarCommand() throws Exception {
        equalCommand = factory.getTrainCar(TrainsCommand.getTrainCommand("commandName=AddBaggageCar"));
        assertEquals(true, (equalCommand instanceof AddBaggageCarCommand));
    }

    @Test
    public void testAddCompartmentCarCommand() throws Exception {
        equalCommand = factory.getTrainCar(TrainsCommand.getTrainCommand("commandName=AddCompartmentCar"));
        assertEquals(true, (equalCommand instanceof AddCompartmentCarCommand));
    }

    @Test
    public void testAddDiningCarCommand() throws Exception {
        equalCommand = factory.getTrainCar(TrainsCommand.getTrainCommand("commandName=AddDiningCar"));
        assertEquals(true, (equalCommand instanceof AddDiningCarCommand));
    }

    @Test
    public void testAddEconomCarCommand() throws Exception {
        equalCommand = factory.getTrainCar(TrainsCommand.getTrainCommand("commandName=AddEconomCar"));
        assertEquals(true, (equalCommand instanceof AddEconomCarCommand));
    }

    @Test
    public void testAddSeatCarCommand() throws Exception {
        equalCommand = factory.getTrainCar(TrainsCommand.getTrainCommand("commandName=AddSeatCar"));
        assertEquals(true, (equalCommand instanceof AddSeatCarCommand));
    }

    @Test
    public void testNoCommand() throws Exception {
        equalCommand = factory.getTrainCar(TrainsCommand.getTrainCommand("commandName="));
        assertEquals(true, (equalCommand instanceof NoCommand));
    }

    @After
    public void afterTest() {
        trainCarService = null;
        factory = null;
    }
}
