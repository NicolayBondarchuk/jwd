package by.training.bnm.controller;

import by.training.bnm.dao.repository.InMemoryTrainCarService;
import by.training.bnm.dao.repository.TrainCarService;
import by.training.bnm.service.commands.get.Display;
import by.training.bnm.service.commands.get.DisplayAll;
import by.training.bnm.service.commands.factory.handle.GetFactory;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class GetCommandTest {
    GetFactory displayFactory;
    Map<String, String> commands;
    TrainCarService trainCarService;

    @Before
    public void initTest() {
        commands = new HashMap<>();
        trainCarService = new InMemoryTrainCarService();
        displayFactory = new GetFactory(trainCarService);
    }

    @Test
    public void testDisplay() {
        commands.put("commandName", "DISPLAY_ALL");
        Display display = displayFactory.getShowClass(commands.get("commandName"));
        assertEquals(true, (display instanceof DisplayAll));
    }
}
