package by.training.bnm.dao.repository;

import by.training.bnm.entity.RailwayVehicle;
import by.training.bnm.entity.baggage.BaggageCar;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class InMemoryTrainCarServiceTest {
    private InMemoryTrainCarService service;
    RailwayVehicle vehicle;

    @Before
    public void initTest() {
        service = new InMemoryTrainCarService();
        vehicle = new BaggageCar();
        service.addRailwayVehicle(vehicle);
    }

    @Test
    public void testAddRailwayVehicle() {
        assertEquals(true, service.addRailwayVehicle(vehicle));
    }

    @Test
    public void testRemoveRailwayVehicle() {
        assertEquals(true, service.removeRailwayVehicle(vehicle));
    }

    @After
    public void afterTest() {
        service = null;
        vehicle = null;
    }
}
