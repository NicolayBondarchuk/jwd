package by.training.bnm.check;

import by.training.bnm.entity.Postcard;
import by.training.bnm.parser.parse.*;
import by.training.bnm.parser.callback.ErrorCallback;
import by.training.bnm.parser.callback.ErrorCallbackImpl;
import by.training.bnm.parser.validator.SAXValidator;
import by.training.bnm.parser.validator.ValidationResult;
import by.training.bnm.parser.validator.XMLValidator;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.ArrayList;
import java.util.List;

@RunWith(JUnit4.class)
public class ParsersTest {
    private static final Logger LOGGER = Logger.getLogger(ParsersTest.class);
    EntityBuilder<Postcard> builder;
    XMLValidator validator;
    ErrorCallback errorCallback;
    ValidationResult validationResult;

    @Before
    public void init() {
        builder = new PostcardBuilder();
        validator = new SAXValidator("xml/postcards.xsd");
        errorCallback = new ErrorCallbackImpl();
        validationResult = validator
                .validate(ClassLoader.getSystemResourceAsStream("xml/postcards.xml"));
    }

    //@Ignore
    @Test
    public void initDOMParserTest() {
        List<Postcard> results = new ArrayList<>();

        if (validationResult.isValid()) {

            FileParser<Postcard> fileParser = new DOMParser<>(builder);
            try {
                results.addAll(fileParser.parse(ClassLoader.getSystemResourceAsStream("xml/postcards.xml")));
            } catch (ParseException e) {
                //LOGGER.error("DOMParser error " + e.getLocalizedMessage());
                e.printStackTrace();
            }
            Assert.assertEquals(16, results.size());
            for (int i = 0; i < results.size(); i++) {
                System.out.println(results.get(i).toString());
            }
        } else {
            LOGGER.error("from DOM Parser -> xml is not valid");
        }
    }

    @Test
    public void initSAXParserTest() {
        List<Postcard> results = new ArrayList<>();

        if (validationResult.isValid()) {
            FileParser<Postcard> fileParser = new SAXParser<>(builder);
            try {
                results.addAll(fileParser.parse(ClassLoader.getSystemResourceAsStream("xml/postcards.xml")));
            } catch (ParseException e) {
                //LOGGER.error("DOMParser error " + e.getLocalizedMessage());
                e.printStackTrace();
            }
            Assert.assertEquals(16, results.size());
            for (int i = 0; i < results.size(); i++) {
                System.out.println(results.get(i).toString());
            }

        } else {
            LOGGER.error("from SAX Parser -> xml is not valid");
        }
    }

    @Test
    public void initStAXParserTest() {
        List<Postcard> results = new ArrayList<>();

        if (validationResult.isValid()) {
            FileParser<Postcard> fileParser = new StAXParser<>(builder);
            try {
                results.addAll(fileParser.parse(ClassLoader.getSystemResourceAsStream("xml/postcards.xml")));
            } catch (ParseException e) {
                //LOGGER.error("DOMParser error " + e.getLocalizedMessage());
                e.printStackTrace();
            }
            Assert.assertEquals(16, results.size());
            for (int i = 0; i < results.size(); i++) {
                System.out.println(results.get(i).toString());
            }

        } else {
            LOGGER.error("from StAX Parser -> xml is not valid");
        }
    }
}
