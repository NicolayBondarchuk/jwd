package by.training.bnm.parser.validator;

import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;

public class SAXValidator implements XMLValidator {

    private final static Logger LOGGER = Logger.getLogger(SAXValidator.class);
    String schemaName;
    String language = XMLConstants.W3C_XML_SCHEMA_NS_URI;
    SchemaFactory factory = SchemaFactory.newInstance(language);

    public SAXValidator(String schemaName) {
        this.schemaName = schemaName;
    }

    @Override
    public ValidationResult validate(InputStream is) {
        ValidationResult validationResult = new ValidationResult();
        try {
            ClassLoader classLoader = this.getClass().getClassLoader();
            File schemaLocation = new File(Objects.requireNonNull(classLoader.getResource(schemaName)).getFile());
            // create schema
            Schema schema = factory.newSchema(schemaLocation);
            // create validator
            Validator validator = schema.newValidator();
            // check the document
            Source source = new StreamSource(is);
            validator.validate(source);
        } catch (SAXException e) {
            validationResult.addMessage("xml-file isn't valid", e.getMessage());
            LOGGER.error("validation xml-file is not valid because "
                    + e.getMessage());
        } catch (IOException e) {
            validationResult.addMessage("xml-file isn't valid", e.getMessage());
            LOGGER.error("xml-file is not valid because "
                    + e.getMessage());
        } catch (Exception e) {
            validationResult.addMessage("xml-file isn't valid", e.getMessage());
            LOGGER.error("xml-file is not valid because " + e.getMessage());
        }
        return validationResult;
    }

}
