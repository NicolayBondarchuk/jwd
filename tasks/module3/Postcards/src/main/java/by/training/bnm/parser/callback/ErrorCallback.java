package by.training.bnm.parser.callback;

import by.training.bnm.parser.validator.ValidationResult;

public interface ErrorCallback {
    void accept(ValidationResult result);
}
