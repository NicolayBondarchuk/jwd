package by.training.bnm.entity;

import java.util.stream.Stream;

public enum Theme {
    CITY_LANDSCAPE("city_landscape"),
    NATURE("nature"),
    PEOPLE("people"),
    RELIGION("religion"),
    SPORT("sport"),
    ARCHITECTURE("architecture"),
    NO_THEME("no_theme");

    private String theme;

    Theme(String theme) {
        this.theme = theme;
    }

    public static Theme getTheme(String search) {
        return Stream.of(Theme.values())
                .filter(g -> g.theme.equalsIgnoreCase(search))
                .findFirst()
                .orElse(NO_THEME);
    }
}
