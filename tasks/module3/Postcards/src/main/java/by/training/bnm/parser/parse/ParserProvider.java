package by.training.bnm.parser.parse;

public interface ParserProvider<T> {
    void register(String parserName, FileParser<T> parser);
    void remove(String parserName);
    FileParser<T> get(String parserName);
}
