package by.training.bnm.parser.validator;

import java.util.List;

public class ValidationMessage {

    private String fieldName;
    private String fieldValue;
    private List<String> errors;

    public ValidationMessage(String fieldName, List<String> errors) {
        this.fieldName = fieldName;
        this.errors = errors;
    }

    public String getFieldName() {
        return fieldName;
    }

    public String getFieldValue() {
        return fieldValue;
    }

    public List<String> getErrors() {
        return errors;
    }
}
