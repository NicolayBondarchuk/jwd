package by.training.bnm.parser.parse;

import by.training.bnm.entity.*;
import org.apache.log4j.Logger;

import java.util.Map;

public class PostcardBuilder implements EntityBuilder<Postcard> {
    private static final Logger LOGGER = Logger.getLogger(PostcardBuilder.class);

    @Override
    public Postcard build(Map<String, String> data) {
        Postcard postcard = new Postcard();

        try {
            postcard.setId(Long.valueOf(data.get("id")));
            postcard.setCountryOfOrigin(data.get("country"));
            postcard.setTheme(Theme.getTheme(data.get("theme")));
            postcard.setSending(Boolean.parseBoolean(data.get("sending")));
            postcard.setYearOfPublishing(Integer.parseInt(data.get("publishing-year")));
            postcard.setAuthor(new Author());
            postcard.getAuthor().setName(data.get("author-name"));
            postcard.getAuthor().setCountry(data.get("living-country"));
            postcard.getAuthor().setBirthYear(data.get("birth-year"));
            postcard.setValuable(Valuable.getValuable(data.get("valuable")));
            postcard.setType(PostcardType.getType(data.get("type")));
        } catch (Exception e) {
            LOGGER.error("Error to build entity: " + e.getMessage());
        }

        return postcard;
    }
}
