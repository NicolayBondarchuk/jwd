package by.training.bnm.parser.parse;

import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

public class ParserProviderImpl<T> implements ParserProvider<T> {
    private static final Logger LOGGER = Logger.getLogger(ParserProviderImpl.class);

    private final Map<String, FileParser<T>> parserMap = new HashMap<>();

    @Override
    public void register(String parserName, FileParser<T> parser) {
        this.parserMap.put(parserName, parser);
        LOGGER.info("new parser was registered");
    }

    @Override
    public void remove(String parserName) {
        this.parserMap.remove(parserName);
        LOGGER.info("one parser was removed");
    }

    @Override
    public FileParser<T> get(String parserName) {
        return this.parserMap.getOrDefault(parserName, new NOParser<>());
    }
}
