package by.training.bnm.application;

import by.training.bnm.controller.AppCommand;
import by.training.bnm.controller.AppCommandProvider;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@MultipartConfig
@WebServlet("/postcards")
public class PostcardServlet extends HttpServlet {

    private final static Logger LOGGER = Logger.getLogger(PostcardServlet.class);

    private AppCommandProvider commandProvider;
    private Map<String, String> request = new HashMap<>();

    @Override
    public void init(){
        LOGGER.info("servlet init()");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String commandName = "DisplayCommand";
        commandProvider = ApplicationContext.getInstance().getBean(AppCommandProvider.class);
        LOGGER.info("commandProvider: " + commandProvider);
        String query = req.getQueryString();
        if (query != null) {
            request = getParse(query);
            commandName = request.get("commandName");
        }
        AppCommand command = commandProvider.get(commandName);
        command.execute(req, resp);
        LOGGER.info("servlet doGet()");
        request.clear();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.info("doPost is called");
        commandProvider = ApplicationContext.getInstance().getBean(AppCommandProvider.class);
        String commandName = req.getParameter("commandName");
        AppCommand command = commandProvider.get(commandName);
        command.execute(req, resp);
        LOGGER.info("servlet doPost()");
        doGet(req, resp);
    }

    @Override
    public void destroy() {
        ApplicationContext.getInstance().destroy();
        LOGGER.info("servlet destroy()");
    }

    private Map<String, String> getParse(String query) {
        Map<String, String> result = new HashMap<>();
        String[] commands = query.split("&");
        for (String str : commands) {
            String[] temp = str.split("=");
            result.put(temp[0], temp[1]);
        }
        return result;
    }
}
