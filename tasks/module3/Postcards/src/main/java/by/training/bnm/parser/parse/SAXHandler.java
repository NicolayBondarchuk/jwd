package by.training.bnm.parser.parse;

import org.apache.log4j.Logger;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SAXHandler<T> extends DefaultHandler {
    private static final Logger LOGGER = Logger.getLogger(SAXHandler.class);

    private boolean startFlag = true;
    private String startElement = "";
    private Map<String, String> result = new HashMap<>();
    private List<T> results = new ArrayList<>();
    private String content;
    private final EntityBuilder<T> builder;

    public SAXHandler(EntityBuilder<T> builder) {
        this.builder = builder;
    }

    public List<T> getResults() {
        return results;
    }

    private void checkToRepeat(String key) throws ParseException {
        if (startFlag && result.containsKey(key)) {
            startElement = key;
            startFlag = false;
        }
        if (key.equals(startElement)) {
            try {
                results.add(builder.build(result));
            } catch (Exception e) {
                throw new ParseException(e);
            }
            result.clear();
        }
    }

    @Override
    public void startElement(String uri, String localName, String qName,
                             org.xml.sax.Attributes attributes) {
        for (int i = 0; i < attributes.getLength(); i++) {
            String key = attributes.getLocalName(i);
            String value = attributes.getValue(i);
            try {
                checkToRepeat(key);
            } catch (ParseException e) {
                LOGGER.error(e.getMessage());
            }
            result.put(key, value);
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        try {
            checkToRepeat(qName);
        } catch (ParseException e) {
            LOGGER.error(e.getMessage());
        }
        result.put(qName, content);
    }

    @Override
    public void endDocument() throws SAXException {
        try {
            results.add(builder.build(result));
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        content = String.copyValueOf(ch, start, length).trim();
    }
}
