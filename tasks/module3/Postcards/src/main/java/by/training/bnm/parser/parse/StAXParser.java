package by.training.bnm.parser.parse;

import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StAXParser<T> implements FileParser<T> {
    private static final Logger LOGGER = Logger.getLogger(StAXParser.class);

    private final EntityBuilder<T> builder;

    private List<T> results = new ArrayList<>();
    private Map<String, String> result = new HashMap<>();
    private boolean startFlag = true;
    private String startElement = "";

    public StAXParser(EntityBuilder<T> builder) {
        this.builder = builder;
    }

    private void checkToRepeat(String key) throws IOException, SAXException, ParserConfigurationException {
        if (startFlag && result.containsKey(key)) {
            startElement = key;
            startFlag = false;
        }
        if (key.equals(startElement)) {
            results.add(builder.build(result));
            result.clear();
        }
    }

    @Override
    public List<T> parse(InputStream is) throws ParseException {

        try {
            XMLInputFactory factory = XMLInputFactory.newInstance();
            XMLStreamReader reader = factory.createXMLStreamReader(is);
            String value = "";
            while (reader.hasNext()) {
                int event = reader.next();
                switch (event) {
                    case XMLStreamConstants.START_ELEMENT:

                        for (int i = 0; i < reader.getAttributeCount(); i++) {
                            String atrKey = reader.getAttributeLocalName(i);
                            String atrValue = reader.getAttributeValue(i);
                            checkToRepeat(atrKey);
                            result.put(atrKey, atrValue);
                        }
                        break;
                    case XMLStreamConstants.CHARACTERS:
                        value = reader.getText().trim();
                        break;
                    case XMLStreamConstants.END_ELEMENT:
                        String key = reader.getLocalName();
                        checkToRepeat(key);
                        result.put(key, value);
                        break;
                    default:
                        break;
                        //throw new IllegalStateException("Unexpected value: " + event);
                }
            }
            results.add(builder.build(result));
        } catch (Exception e) {
            LOGGER.error("file wasn't parsed by StAXParser because: " + e.getMessage());
            throw new ParseException(e);
        }
        return results;
    }
}
