package by.training.bnm.controller;

public enum AppCommandName {
    DISPLAY_POSTCARDS,
    UPLOAD_POSTCARDS
}
