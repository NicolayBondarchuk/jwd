package by.training.bnm.entity;

import java.util.stream.Stream;

public enum Valuable {
    HISTORICAL("historical"),
    COLLECTIBLE("collectible"),
    THEMATIC("thematic"),
    NO_VALUABLE("no valuable");

    private String valuable;

    Valuable(String valuable) {
        this.valuable = valuable;
    }

    public static Valuable getValuable(String search) {
        return Stream.of(Valuable.values())
                .filter(g -> g.valuable.equalsIgnoreCase(search))
                .findFirst()
                .orElse(NO_VALUABLE);
    }
}
