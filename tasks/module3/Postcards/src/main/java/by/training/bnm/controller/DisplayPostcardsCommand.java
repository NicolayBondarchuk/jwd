package by.training.bnm.controller;

import by.training.bnm.service.PostcardService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DisplayPostcardsCommand extends AbstractAppCommand {
    private final PostcardService postcardService;

    public DisplayPostcardsCommand(PostcardService postcardService) {
        this.postcardService = postcardService;
    }

    @Override
    protected void executeWrapped(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        req.setAttribute("postcards", postcardService.findAll());
        forward(req, resp, "/jsp/postcards.jsp");
    }
}
