package by.training.bnm.controller;

import by.training.bnm.application.ApplicationContext;
import by.training.bnm.entity.Postcard;
import by.training.bnm.parser.callback.ErrorCallback;
import by.training.bnm.parser.parse.FileParser;
import by.training.bnm.parser.parse.ParserProvider;
import by.training.bnm.parser.validator.ValidationResult;
import by.training.bnm.parser.validator.XMLValidator;
import by.training.bnm.service.PostcardService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class UploadPostcardsCommand extends AbstractAppCommand {

    private static final Logger LOGGER = Logger.getLogger(UploadPostcardsCommand.class);

    private final PostcardService postcardService;

    public UploadPostcardsCommand(PostcardService postcardService) {
        this.postcardService = postcardService;
    }


    @Override
    protected void executeWrapped(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Part xmlFile = req.getPart("upload");
        String parserType = req.getParameter("parser");

        if (xmlFile == null || xmlFile.getSize() <= 0L) {
            req.setAttribute("errors", "File doesn't exist or empty");
            return;
        }

        // check for "*.csv"
        String name = xmlFile.getSubmittedFileName();
        String[] splitName = name.split("\\.");
        if (!("xml".equals(splitName[splitName.length - 1]))) {
            req.setAttribute("errors", "File isn't valid (not *.xml)");
            return;
        }

        // validation of the xml-file
        try (InputStream inputStream = xmlFile.getInputStream()) {
            XMLValidator validator = ApplicationContext.getInstance().getBean(XMLValidator.class);
            ValidationResult validationResult = validator.validate(inputStream);

            ErrorCallback errorCallback = ApplicationContext.getInstance().getBean(ErrorCallback.class);
            if (!validationResult.isValid()) {
                LOGGER.error("file is not valid");
                errorCallback.accept(validationResult);
                req.setAttribute("errors", "File isn't valid");
            }
        } catch (Exception e) {
            LOGGER.error("from upload: " + e.getMessage());
        }

        // parse of the xml-file
        try (InputStream inputStream = xmlFile.getInputStream()) {
            FileParser parser = ApplicationContext.getInstance().getBean(ParserProvider.class).get(parserType);
            List<Postcard> parsed = parser.parse(inputStream);

            parsed.forEach(postcardService::save);
            req.setAttribute("postcardFromServer", parsed);

        } catch (Exception e) {
            LOGGER.error("from upload: " + e.getMessage());
        }

    }
}
