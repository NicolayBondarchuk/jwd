package by.training.bnm.parser.validator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ValidationResult {

    List<ValidationMessage> messages = new ArrayList<>();

    public void addMessage(String field, String ...errors) {
        messages.add(new ValidationMessage(field, Arrays.asList(errors)));
    }

    public boolean isValid() {
        return this.messages.isEmpty();
    }

}
