package by.training.bnm.dao;


import by.training.bnm.entity.Author;
import by.training.bnm.entity.Postcard;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public class PostcardDaoImpl implements PostcardDao {

    private final Map<Long, Postcard> storage = new ConcurrentHashMap<>();
    private final AtomicLong sequence = new AtomicLong(0);

    @Override
    public List<Postcard> findAll() {
        return new ArrayList<>(storage.values());
    }

    @Override
    public void save(Postcard postcard) {
        Postcard value = cloneEntity(postcard);
        long key = sequence.incrementAndGet();
        value.setId(key);
        this.storage.put(key, value);
    }

    @Override
    public void update(Long key, Postcard postcard) {

        if (this.storage.containsKey(key)) {
            this.storage.put(key, cloneEntity(postcard));
        } else {
            throw new EntityNotFoundException("not found key=" + key);
        }
    }

    @Override
    public Postcard get(Long key) {

        if (this.storage.containsKey(key)) {
            return this.storage.get(key);
        } else {
            throw new EntityNotFoundException("not found key=" + key);
        }
    }

    @Override
    public void delete(Long key) {

        this.storage.remove(key);
    }

    private Postcard cloneEntity(Postcard postcard) {
        Postcard newPostcard = new Postcard();
        newPostcard.setId(postcard.getId());
        newPostcard.setCountryOfOrigin(postcard.getCountryOfOrigin());
        newPostcard.setTheme(postcard.getTheme());
        newPostcard.setSending(postcard.isSending());
        newPostcard.setYearOfPublishing(postcard.getYearOfPublishing());
        newPostcard.setAuthor(new Author(postcard.getAuthor().getName(),
                postcard.getAuthor().getCountry(), postcard.getAuthor().getBirthYear()));
        newPostcard.setValuable(postcard.getValuable());
        newPostcard.setType(postcard.getType());
       return newPostcard;
    }
}
