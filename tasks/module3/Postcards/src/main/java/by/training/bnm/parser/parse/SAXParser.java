package by.training.bnm.parser.parse;

import by.training.bnm.parser.callback.ErrorCallback;
import by.training.bnm.parser.validator.ValidationResult;
import by.training.bnm.parser.validator.XMLValidator;
import org.apache.log4j.Logger;

import javax.xml.parsers.SAXParserFactory;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class SAXParser<T> implements FileParser<T> {
    private static final Logger LOGGER = Logger.getLogger(SAXParser.class);

    private final EntityBuilder<T> builder;

    public SAXParser(EntityBuilder<T> builder) {
        this.builder = builder;
    }

    @Override
    public List<T> parse(InputStream is) throws ParseException {
        List<T> results;

        try {
            SAXParserFactory parserFactory = SAXParserFactory.newInstance();
            javax.xml.parsers.SAXParser parser = null;
            parser = parserFactory.newSAXParser();
            SAXHandler handler = new SAXHandler(builder);
            parser.parse(is, handler);
            results = new ArrayList<>(handler.getResults());
        } catch (Exception e) {
            LOGGER.error("file wasn't parsed by SAXParser because: " + e.getMessage());
            throw new ParseException(e);
        }

        return results;
    }

}
