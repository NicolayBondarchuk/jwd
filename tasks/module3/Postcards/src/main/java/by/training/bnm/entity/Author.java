package by.training.bnm.entity;

import java.util.Objects;
import java.util.Optional;

public class Author {
    private String name;
    private String country;
    private int birthYear;

    public Author() {
    }

    public Author(String name, String country, int birthYear) {
        this.name = name;
        this.country = country;
        this.birthYear = birthYear;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = Optional.ofNullable(name).orElseGet(() -> "no author name");
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = Optional.ofNullable(country).orElse("no author country");
    }

    public int getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(String birthYear) {
        this.birthYear = Integer.parseInt(Optional.ofNullable(birthYear)
                .map(year -> year.matches("\\d{4}"))
                .map(b -> b ? birthYear : "0").orElse("0"));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Author author = (Author) o;
        return birthYear == author.birthYear &&
                Objects.equals(name, author.name) &&
                Objects.equals(country, author.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, country, birthYear);
    }

    @Override
    public String toString() {
        return "name--" + name +
                ", country--" + country +
                ", birthYear--" + birthYear;
    }
}
