package by.training.bnm.application;

import by.training.bnm.controller.*;
import by.training.bnm.dao.PostcardDao;
import by.training.bnm.dao.PostcardDaoImpl;
import by.training.bnm.entity.Postcard;
import by.training.bnm.parser.callback.ErrorCallback;
import by.training.bnm.parser.parse.EntityBuilder;
import by.training.bnm.parser.parse.PostcardBuilder;
import by.training.bnm.parser.callback.ErrorCallbackImpl;
import by.training.bnm.parser.parse.*;
import by.training.bnm.parser.validator.SAXValidator;
import by.training.bnm.parser.validator.XMLValidator;
import by.training.bnm.service.PostcardService;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

public class ApplicationContext {
    private static final Logger LOGGER = Logger.getLogger(ApplicationContext.class);

    private static ApplicationContext INSTANCE;

    private ApplicationContext() {}

    private final Map<Class<?>, Object> beans = new HashMap<>();

    public static ApplicationContext getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ApplicationContext();

            LOGGER.info("context was create");

            INSTANCE.init();
        }
        return INSTANCE;
    }

    public void init() {
        // create all our objects
        PostcardDao postcardDao = new PostcardDaoImpl();
        PostcardService postcardService = new PostcardService(postcardDao);

        AppCommandProvider postcardProvider = new AppCommandProviderImpl();
        postcardProvider.register("noCommand", new NoCommand());
        postcardProvider.register("UploadCommand", new UploadPostcardsCommand(postcardService));
        postcardProvider.register("DisplayCommand", new DisplayPostcardsCommand(postcardService));

        EntityBuilder<Postcard> builder = new PostcardBuilder();
        XMLValidator validator = new SAXValidator("xml/postcards.xsd");
        ErrorCallback errorCallback = new ErrorCallbackImpl();

        ParserProvider<Postcard> parserProvider = new ParserProviderImpl<>();
        parserProvider.register("DOM_Parsing", new DOMParser<>(builder));
        parserProvider.register("SAX_Parsing", new SAXParser<>(builder));
        parserProvider.register("StAX_Parsing", new StAXParser<>(builder));

        beans.put(XMLValidator.class, validator);
        beans.put(ErrorCallback.class, errorCallback);
        beans.put(PostcardDao.class, postcardDao);
        beans.put(PostcardService.class, postcardService);
        beans.put(AppCommandProvider.class, postcardProvider);
        beans.put(ParserProvider.class, parserProvider);

        LOGGER.info("context was initialized");
    }

    public void destroy() {
        beans.clear();
        LOGGER.info("context was destroyed");
    }

    public <T> T getBean(Class<T> clazz) {
        return (T) this.beans.get(clazz);
    }
}
