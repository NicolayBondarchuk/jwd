package by.training.bnm.parser.parse;

import by.training.bnm.parser.validator.ValidationResult;
import org.apache.log4j.Logger;
import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.InputStream;
import java.util.*;

public class DOMParser<T> implements FileParser<T> {
    private static final Logger LOGGER = Logger.getLogger(DOMParser.class);

    private final EntityBuilder<T> builder;

    public DOMParser(EntityBuilder<T> builder) {
        this.builder = builder;
    }

    private Map<String, String> parseNode(Node node) {
        Map<String, String> data = new HashMap<>();
        if (node.hasAttributes()) {
            NamedNodeMap attributes = node.getAttributes();
            for (int j = 0; j < attributes.getLength(); j++) {
                String key = attributes.item(j).getNodeName();
                String value = attributes.item(j).getNodeValue();
                data.put(key, value);
            }
        }
        if (node.hasChildNodes()) {
            NodeList nodeList = node.getChildNodes();
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node cNode = nodeList.item(i);

                // recursion !!!
                Map<String, String> map = parseNode(cNode);
                if (!map.isEmpty()) {
                    data.putAll(map);
                }
            }
            String key = node.getNodeName();
            String value = node.getLastChild().getTextContent().trim();
            data.put(key, value);
        }


        //LOGGER.info("from recursion method parseNode()");
        return data;
    }

    @Override
    public List<T> parse(InputStream is) throws ParseException {

        List<T> results = new ArrayList<>();

        try {

            Map<String, String> result = new HashMap<>();

            //Get the DOM Builder Factory
            DocumentBuilderFactory factory =
                    DocumentBuilderFactory.newInstance();
            //Get the DOM Builder
            DocumentBuilder documentBuilder = factory.newDocumentBuilder();
            //Load and Parse the XML document
            //document contains the complete XML as a Tree.
            Document document =
                    documentBuilder.parse(is);
            //Iterating through the nodes and extracting the data.
            NodeList nodeList =
                    document.getDocumentElement().getChildNodes();

            for (int i = 0; i < nodeList.getLength(); i++) {
                //We have encountered an <postcard> tag.
                Node node = nodeList.item(i);
                if (node instanceof Element) {
                    result.putAll(parseNode(node));
                    results.add(builder.build(result));
                }
            }

        } catch (Exception e) {
            LOGGER.error("file wasn't parsed by DOMParser");
            throw new ParseException(e);
        }

        return results;
    }
}

