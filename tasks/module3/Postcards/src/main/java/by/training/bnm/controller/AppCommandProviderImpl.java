package by.training.bnm.controller;

import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

public class AppCommandProviderImpl implements AppCommandProvider {
    private static final Logger LOGGER = Logger.getLogger(AppCommandProviderImpl.class);

    private final Map<String, AppCommand> commandMap = new HashMap<>();

    @Override
    public void register(String commandName, AppCommand command) {
        this.commandMap.put(commandName, command);
        LOGGER.info("new command was registered");
    }

    @Override
    public void remove(String commandName) {
        this.commandMap.remove(commandName);
        LOGGER.info("new command was removed");
    }

    @Override
    public AppCommand get(String commandName) {
        LOGGER.info("get command");
        return this.commandMap.getOrDefault(commandName, new NoCommand());
    }

}
