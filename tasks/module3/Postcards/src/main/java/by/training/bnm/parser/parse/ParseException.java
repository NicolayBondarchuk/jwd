package by.training.bnm.parser.parse;

public class ParseException extends Exception {

    public ParseException(Throwable cause) {
        super(cause);
    }

}
