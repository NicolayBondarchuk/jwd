package by.training.bnm.entity;

import java.util.stream.Stream;

public enum PostcardType {
    ADVERTISING_CARD("advertising-card"),
    GREETING_CARD("greeting-card"),
    ORDINARY_CARD("ordinary-card"),
    EMPTY_CARD("empty-card");

    private String type;

    PostcardType(String type) {
        this.type = type;
    }

    public static PostcardType getType(String type) {
        return Stream.of(PostcardType.values())
                .filter(g -> g.type.equalsIgnoreCase(type))
                .findFirst()
                .orElse(EMPTY_CARD);
    }

    public String getType() {
        return type;
    }
}
