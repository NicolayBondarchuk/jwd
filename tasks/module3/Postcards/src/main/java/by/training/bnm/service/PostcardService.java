package by.training.bnm.service;


import by.training.bnm.dao.PostcardDao;
import by.training.bnm.entity.Postcard;

import java.util.List;

public class PostcardService {
    private final PostcardDao postcardDao;

    public PostcardService(PostcardDao postcardDao) {
        this.postcardDao = postcardDao;
    }

    public void save(Postcard postcard) {
        this.postcardDao.save(postcard);
    }

    public List<Postcard> findAll() {
        return this.postcardDao.findAll();
    }
}
