package by.training.bnm.entity;

import java.util.Objects;

public class Postcard {
    private Long id;
    private String countryOfOrigin;
    private Theme theme;
    private boolean sending;
    private int yearOfPublishing;
    private Author author;
    private Valuable valuable;
    private PostcardType type;

    public Postcard() {
        author = new Author();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id =id;
    }

    public String getCountryOfOrigin() {
        return countryOfOrigin;
    }

    public void setCountryOfOrigin(String countryOfOrigin) {
        this.countryOfOrigin = countryOfOrigin;
    }

    public Theme getTheme() {
        return theme;
    }

    public void setTheme(Theme theme) {
        this.theme = theme;
    }

    public boolean isSending() {
        return sending;
    }

    public void setSending(boolean sending) {
        this.sending = sending;
    }

    public int getYearOfPublishing() {
        return yearOfPublishing;
    }

    public void setYearOfPublishing(int yearOfPublishing) {
        this.yearOfPublishing = yearOfPublishing;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public Valuable getValuable() {
        return valuable;
    }

    public void setValuable(Valuable valuable) {
        this.valuable = valuable;
    }

    public PostcardType getType() {
        return type;
    }

    public void setType(PostcardType type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Postcard postcard = (Postcard) o;
        return sending == postcard.sending &&
                yearOfPublishing == postcard.yearOfPublishing &&
                Objects.equals(id, postcard.id) &&
                Objects.equals(countryOfOrigin, postcard.countryOfOrigin) &&
                theme == postcard.theme &&
                Objects.equals(author, postcard.author) &&
                valuable == postcard.valuable &&
                type == postcard.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, countryOfOrigin, theme, sending,
                yearOfPublishing, author, valuable, type);
    }

    @Override
    public String toString() {
        return  "\tid: " + id +
                "\n\tcountryOfOrigin: " + countryOfOrigin +
                "\n\ttheme: " + theme +
                "\n\tsending: " + sending +
                "\n\tyearOfPublishing: " + yearOfPublishing +
                "\n\tauthor: " + author +
                "\n\tvaluable: " + valuable +
                "\n\ttype: " + type;
    }
}
