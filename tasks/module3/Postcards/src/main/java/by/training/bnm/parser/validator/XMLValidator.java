package by.training.bnm.parser.validator;


import java.io.InputStream;

public interface XMLValidator {
    ValidationResult validate(InputStream is);
}
