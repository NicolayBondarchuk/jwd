package by.training.bnm.controller;

import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public abstract class AbstractAppCommand implements AppCommand {
    private static final Logger LOGGER = Logger.getLogger(AbstractAppCommand.class);

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            executeWrapped(req, resp);
        } catch (Exception e) {
            LOGGER.error("execute error" + e.getMessage());
        }
    }

    protected abstract void executeWrapped(HttpServletRequest req, HttpServletResponse resp) throws Exception;

    protected void forward(HttpServletRequest req, HttpServletResponse resp, String url) throws ServletException, IOException {
        RequestDispatcher dispatcher = req.getServletContext().getRequestDispatcher(url);
        dispatcher.forward(req, resp);

    }

    protected void redirect(HttpServletResponse resp, String url) throws IOException {

        resp.sendRedirect(url);

    }
}
