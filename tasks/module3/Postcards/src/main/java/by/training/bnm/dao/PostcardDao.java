package by.training.bnm.dao;

import by.training.bnm.entity.Postcard;

public interface PostcardDao extends CrudEntityDao<Postcard, Long> {

}
