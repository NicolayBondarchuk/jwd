package by.training.bnm.controller;


public interface AppCommandProvider {
    void register(String commandName, AppCommand command);
    void remove(String commandName);
    AppCommand get(String commandName);
}
