package by.training.bnm.parser.parse;

import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.Map;

public interface EntityBuilder<T> {
    // делает StringBuilder для вывода его на html страницу (с тегами)
    T build(Map<String, String> data) throws ParserConfigurationException, IOException, SAXException;
}
