package by.training.bnm.parser.parse;

import java.io.InputStream;
import java.util.List;

public interface FileParser<T> {
    List<T> parse(InputStream is) throws ParseException;
}
