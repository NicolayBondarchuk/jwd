<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>

    <head>
        <title>Postcards list​</title>
        <link rel="stylesheet" href="static/main.css">

    </head>
    <ul>
        <li>
            <a href="?commandName=DisplayCommand">View All</a>
        </li>
    </ul>

    <body>
    <h1>Hello from Tomcat9 intellij idea ultimate</h1>
    <div class="form-style-2">
        <div class="form-style-2-heading">
            Please add file
        </div>
        <form action="/postcards/postcards" method="POST" enctype="multipart/form-data">
            <label>Upload file
                <input type="file" name="upload"/><br>
            </label>
            <label for="Parser">
                <select name="parser" id="parser">
                    <option value="DOM_Parsing">DOM parser</option>
                    <option value="SAX_Parsing">SAX parser</option>
                    <option value="StAX_Parsing">StAX parser</option>
                </select>
           </label>
           <input type="hidden" name="commandName" value="UploadCommand"/>
           <input type="submit" value="Enter"/>
        </form>
        <br>
        <p style="color:#af2222;font-size:20px">
            <i><c:out value="${errors}"/></i><br>
        </p>
    </div>
        <div class="form-style-2-heading">
            Table of Postcards
        </div>
        <table>
            <div class="table100-head">
            <tr>
                <th id="1">-=ID=-</th>
                <th id="2">-=COUNTRY=-</th>
                <th id="3">-=THEME=-</th>
                <th id="4">-=SENDING=- </th>
                <th id="5">-=PUBLISHING YEAR=- </th>
                <th id="6">-=AUTHOR=- </th>
                <th id="7">-=VALUABLE=- </th>
                <th id="8">-=TYPE=- </th>
            </tr>
            </div>
        <c:forEach items="${postcardFromServer}" var="postcard">
            <tr>
                <td>${postcard.id}</td>
                <td>${postcard.countryOfOrigin}</td>
                <td>${postcard.theme}</td>
                <td>${postcard.sending}</td>
                <td>${postcard.yearOfPublishing}</td>
                <td>${postcard.getAuthor().name}</td>
                <td>${postcard.valuable}</td>
                <td>${postcard.type}</td>
            </tr>
            </c:forEach>
        </table>
    </div>
    </body>
</html>
