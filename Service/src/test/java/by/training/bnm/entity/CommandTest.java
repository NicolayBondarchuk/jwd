package by.training.bnm.entity;

import by.training.bnm.command.AppCommand;
import by.training.bnm.command.CommandProvider;
import by.training.bnm.command.CommandType;
import by.training.bnm.context.ApplicationContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class CommandTest {
    private static final Logger LOGGER = LogManager.getLogger(CommandTest.class);


    @Test
    public void OutputCommandTest() {
        ApplicationContext.getInstance().initialize();
        CommandProvider commandProvider = ApplicationContext.getInstance().getBean(CommandProvider.class);

        String register = CommandType.REGISTER_USER.name();
        String show = CommandType.SHOW_USER.name();
        String login = CommandType.LOGIN.name();

        AppCommand command2 = commandProvider.getItem(register);
        AppCommand command3 = commandProvider.getItem(show);
        AppCommand command4 = commandProvider.getItem(login);
        //LOGGER.info("command.execute() = " + command.execute(null, null));
        LOGGER.info("command2.execute() = " + command2.execute(null, null));
        LOGGER.info("command3.execute() = " + command3.execute(null, null));
        LOGGER.info("command4.execute() = " + command4.execute(null, null));

        Assert.assertEquals("register_user", command2.execute(null, null));
        Assert.assertEquals("show_user", command3.execute(null, null));
        Assert.assertEquals("login", command4.execute(null, null));
    }
}
