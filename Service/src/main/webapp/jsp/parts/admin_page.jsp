<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="by.training.bnm.command.CommandType" %>

<div class="container">
    <div class="block">
        <table class="table">
            <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Password</th>
                <th>Email</th>
                <th>Role</th>
                <th>Wallet</th>
                <th>Rating</th>
                <th>Description</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Password</th>
                <th>Email</th>
                <th>Role</th>
                <tn>Wallet</tn>
                <th>Rating</th>
                <th>Description</th>
            </tr>
            </tfoot>
            <tbody>
            <c:forEach items="${userList}" var="user">
                <tr>
                    <th><c:out value="${user.id}"/></th>
                    <td>
                        <a href="?commandName=${CommandType.SHOW_USER}&id=${user.id}">
                            <c:out value="${user.userName}"/>
                        </a>
                    </td>
                    <td><c:out value="${user.userPassword}"/></td>
                    <td><c:out value="${user.userEmail}"/></td>
                    <td><c:out value="${user.userRole}}"/></td>
                    <td><c:out value="${user.userWallet}}"/></td>
                    <td><c:out value="${user.userRating}}"/></td>
                    <td><c:out value="${user.userDescription}}"/></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>
