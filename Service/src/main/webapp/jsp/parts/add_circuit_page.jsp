
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="by.training.bnm.command.CommandType" %>

<div class="container">
    <div class="block">
        <h1 class="title is-bold">Add circuit page:</h1>
        <div class="columns">
            <div class="column is-one-quarter">
                <label class="label">Circuit name:
                </label>
                <label class="label" for="circuitName">
                    <input id="circuitName" name="circuitName" class="input" placeholder="enter mac-address"
                           type="text">
                </label>
            </div>
            <div class="column">
            </div>
        </div>
        <div class="columns">
            <div class="column is-one-third">
                <label class="label">Setpoint:
                </label>
                <label class="label" for="setPoint">
                    <input id="setPoint" name="setPoint" class="input" placeholder="enter setpoint value" type="text">
                </label>
            </div>
            <div class="column">
            </div>
        </div>
        <div class="columns">
            <div class="column is-one-third">
                <label class="label">Alarm:
                </label>
                <label class="label" for="alarm">
                    <input id="alarm" name="alarm" class="input" placeholder="enter alarm value" type="text">
                </label>
            </div>
        </div>
        <div class="columns">
            <div class="column is-one-third">
                <div class="field">
                    <label class="label">Description:</label>
                    <p class="control">
                        <label for="description">
                            <textarea id="description" name="description" class="textarea" placeholder="Description"></textarea>
                        </label>
                    </p>
                </div>
            </div>
        </div>
        <div class="columns">
            <div class="column is-one-quarter">
                <input type="hidden" name="commandName" value="${CommandType.ADD_CIRCUIT}">
                <input type="hidden" name="controller.id" value="${controller.id}">
                <input class="button is-primary" type="submit" value="DONE"><br>
            </div>
            <div class="column">
            </div>
        </div>

    </div>
</div>

