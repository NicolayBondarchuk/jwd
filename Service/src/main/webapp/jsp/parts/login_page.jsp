<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="by.training.bnm.command.CommandType" %>

<div class="container">
    <div class="block">
        <form action="${pageContext.request.contextPath}/" method="get">
            <div class="columns">
                <div class="column is-one-quarter">
                    <label class="label">Name:
                    </label>
                    <label class="label">
                        <input id="user.userName" name="user.userName" class="input" placeholder="enter name" type="text">
                    </label>
                </div>
                <div class="column">
                </div>
            </div>
            <div class="columns">
                <div class="column is-one-quarter">
                    <label class="label">Password:
                    </label>
                    <label class="label">
                        <input id="user.userPassword" name="user.userPassword" class="input" placeholder="enter password" type="password">
                    </label>
                </div>
                <div class="column">
                </div>
            </div>
            <div class="columns">
                <div class="column is-one-quarter">
                    <input type="hidden" name="commandName" value="${CommandType.LOGIN}">
                    <input class="button is-primary" type="submit" value="ENTER"><br>
                </div>
                <div class="column">
                </div>
            </div>
        </form>
    </div>
</div>
