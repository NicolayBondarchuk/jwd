<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="by.training.bnm.command.CommandType" %>

<div class="container">
    <div class="block">
        <h1 class="title is-bold">User page:</h1>
        <h3 class="title">Name: ${user.userName}</h3>
        <form>
            <form method="post">
                <input type="hidden" name="commandName" value="${CommandType.ADD_OBJECT_PAGE}">
                <input type="hidden" name="user.id" value="${user.id}">
                <input class="button is-primary" type="submit" value="+Add New Object">
            </form>
            <c:forEach items="${items}" var="item">
                <section class="hero is-info">
                    <div class="hero-body">
                        <div class="container">
                            <div class="control nav-right">
                                <form method="post">
                                    <input type="hidden" name="commandName" value="${CommandType.ADD_CONTROLLER_PAGE}">
                                    <input type="hidden" name="item.id" value="${item.id}">
                                    <input type="hidden" name="user.id" value="${user.id}">
                                    <input class="button is-white" type="submit" value="+Add New Controller">
                                </form>
                                <form method="post">
                                    <input type="hidden" name="commandName" value="${CommandType.EDIT_OBJECT_PAGE}">
                                    <input type="hidden" name="item.id" value="${item.id}">
                                    <input type="hidden" name="user.id" value="${user.id}">
                                    <input type="hidden" name="redirect" value="edit_object_page">
                                    <input class="button is-success" type="submit" value="Edit">
                                </form>
                                <form method="post">
                                    <input type="hidden" name="commandName" value="${CommandType.DELETE_OBJECT}">
                                    <input type="hidden" name="item.id" value="${item.id}">
                                    <input type="hidden" name="user.id" value="${user.id}">
                                    <input class="button is-warning" type="submit" value="Delete">
                                </form>
                            </div>
                            <h1 class="title">
                                Object name: ${item.itemName}
                            </h1>
                            <h2 class="subtitle">Description:<br>
                                monthly payment: ${item.itemCost}<br>
                                responsibility category: ${item.responsibilityCategory}<br>
                                distance: ${item.distance}<br>
                                    ${item.description}
                            </h2>
                        </div>
                    </div>
                </section>
                <c:forEach items="${controllers}" var="controller">
                    <div class="block">
                        <div class="columns">
                            <div class="column is-two-thirds">
                                <h1 class="title is-4">Controller name: ${controller.controllerName}</h1>
                                <h2 class="subtitle is-6">Description:<br>
                                    mac address: ${controller.macAddress}<br>
                                        ${controller.description}
                                </h2>
                            </div>
                            <div class="column">
                                <p class="notification is-light">
                                <form method="post">
                                    <input type="hidden" name="commandName" value="${CommandType.ADD_CIRCUIT_PAGE}">
                                    <input type="hidden" name="controller.id" value="${controller.id}">
                                    <input type="hidden" name="user.id" value="${user.id}">
                                    <input class="button is-danger" type="submit" value="+Add New Circuit">
                                </form>
                                <form method="post">
                                    <input type="hidden" name="commandName" value="${CommandType.EDIT_CONTROLLER_PAGE}">
                                    <input type="hidden" name="item.id" value="${item.id}">
                                    <input type="hidden" name="controller.id" value="${controller.id}">
                                    <input type="hidden" name="user.id" value="${user.id}">
                                    <input class="button is-success" type="submit" value="Edit">
                                </form>
                                <form method="post">
                                    <input type="hidden" name="commandName" value="${CommandType.DELETE_CONTROLLER}">
                                    <input type="hidden" name="controller.id" value="${controller.id}">
                                    <input type="hidden" name="user.id" value="${user.id}">
                                    <input class="button is-warning" type="submit" value="Delete">
                                </form>
                            </div>
                        </div>
                    </div>
                    <c:forEach items="${circuits}" var="circuit">
                        <div class="block">
                            <aside class="menu">
                                <ul class="menu-list">
                                    <li>
                                        <div class="columns">
                                            <div class="column is-half">
                                                <p class="notification is-info">CIRCUIT: ${circuit.circuitName}</p>
                                                <p>set point: ${circuit.setPoint}; alarm: ${circuit.alarm}.</p>
                                                <p>${circuit.description}</p>
                                            </div>
                                            <div class="column">
                                                <p class="notification is-info">
                                                <form method="post">
                                                    <input type="hidden" name="commandName"
                                                           value="${CommandType.ADD_ELEMENT_PAGE}">
                                                    <input type="hidden" name="circuit.id" value="${circuit.id}">
                                                    <input type="hidden" name="user.id" value="${user.id}">
                                                    <input class="button is-white is-small" type="submit"
                                                           value="+Add New Element">
                                                </form>
                                                <form method="post">
                                                    <input type="hidden" name="commandName"
                                                           value="${CommandType.EDIT_CIRCUIT_PAGE}">
                                                    <input type="hidden" name="circuit.id" value="${circuit.id}">
                                                    <input type="hidden" name="user.id" value="${user.id}">
                                                    <input class="button is-success is-small" type="submit"
                                                           value="Edit">
                                                </form>
                                                <form method="post">
                                                    <input type="hidden" name="commandName"
                                                           value="${CommandType.DELETE_CIRCUIT}">
                                                    <input type="hidden" name="circuit.id" value="${circuit.id}">
                                                    <input type="hidden" name="user.id" value="${user.id}">
                                                    <input class="button is-warning is-small" type="submit"
                                                           value="Edit">
                                                </form>
                                            </div>
                                        </div>

                                        <ul>
                                            <li>
                                                <div class="block">
                                                    <div class="columns">
                                                        <div class="column is-half">
                                                            <p class="notification is-success">ELEMENT</p>
                                                        </div>
                                                        <div class="column">
                                                            <p class="notification is-success">VALUE</p>
                                                        </div>
                                                    </div>
                                                    <c:forEach items="${elements}" var="element">
                                                        <div class="columns">
                                                            <div class="column is-half">
                                                                <p class="notification is-light">
                                                                        ${element.symbol}, ${element.name}:
                                                                </p>
                                                            </div>
                                                            <div class="column">
                                                                <p class="notification is-light">${element.value}</p>
                                                            </div>
                                                            <div class="column">
                                                                <p class="notification is-light">
                                                                <form method="post">
                                                                    <input type="hidden" name="commandName"
                                                                           value="${CommandType.EDIT_ELEMENT_PAGE}">
                                                                    <input type="hidden" name="element.id"
                                                                           value="${element.id}">
                                                                    <input type="hidden" name="user.id"
                                                                           value="${user.id}">
                                                                    <input class="button is-success is-small"
                                                                           type="submit"
                                                                           value="Edit">
                                                                </form>
                                                                <form method="post">
                                                                    <input type="hidden" name="commandName"
                                                                           value="${CommandType.DELETE_ELEMENT}">
                                                                    <input type="hidden" name="element.id"
                                                                           value="${element.id}">
                                                                    <input type="hidden" name="user.id"
                                                                           value="${user.id}">
                                                                    <input class="button is-warning is-small"
                                                                           type="submit"
                                                                           value="Delete">
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </c:forEach>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </aside>
                        </div>
                    </c:forEach>
                </c:forEach>
            </c:forEach>
        </form>
    </div>
</div>
