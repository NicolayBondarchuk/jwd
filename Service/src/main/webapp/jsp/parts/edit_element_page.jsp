
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="container">
    <div class="block">
        <h1 class="title is-bold">Add element page:</h1>
        <div class="columns">
            <div class="column is-one-third">
                <label class="label">Symbol:
                </label>
                <label class="label">
                    <input class="input" placeholder="enter symbol" type="text">
                </label>
            </div>
            <div class="column">
            </div>
        </div>
        <div class="columns">
            <div class="column is-one-third">
                <label class="label">Name:
                </label>
                <label class="label">
                    <input class="input" placeholder="enter name" type="text">
                </label>
            </div>
        </div>
        <div class="columns">
            <div class="column is-one-third">
                <label class="label">Address:
                </label>
                <label class="label">
                    <input class="input" placeholder="enter address 0x:" type="text">
                </label>
            </div>
        </div>
        <div class="columns">
            <div class="column is-one-third">
                <label class="label">Type:
                </label>
                <div class="select">
                    <label>
                        <select>
                            <option>Analog sensor</option>
                            <option>Discrete sensor</option>
                            <option>Analog actuator</option>
                            <option>Discrete actuator</option>
                        </select>
                    </label>
                </div>
            </div>
        </div>
        <div class="columns">
            <div class="column is-one-third">
                <div class="field">
                    <label class="label">Description:</label>
                    <p class="control">
                        <label>
                            <textarea class="textarea" placeholder="Description"></textarea>
                        </label>
                    </p>
                </div>
            </div>
        </div>
        <div class="columns">
            <div class="column is-one-quarter">
                <a class="button is-primary" href="">DONE</a><br>
            </div>
            <div class="column">
            </div>
        </div>

    </div>
</div>

