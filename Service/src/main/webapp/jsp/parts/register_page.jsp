<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="by.training.bnm.command.CommandType" %>

<div class="container">
    <div class="block">
        <form action="${pageContext.request.contextPath}/" method="post">
            <h1 class="title is-bold">Registration user menu:</h1>

            <c:if test="${not empty errors}">
                <p class="error">
                        ${errors}
                </p>
            </c:if>

            <div class="columns">
                <div class="column is-one-third">
                    <label class="label" for="user.userName">Name:</label>
                    <div class="control">
                        <input id="user.userName" name="user.userName" class="input" placeholder="enter userName"
                               type="text">
                    </div>
                </div>
            </div>
            <div class="columns">
                <div class="column is-one-third">
                    <label class="label" for="user.userPassword">Password:</label>
                    <input id="user.userPassword" name="user.userPassword" class="input"
                           placeholder="enter password" type="password">
                </div>
            </div>
            <div class="columns">
                <div class="column is-one-third">
                    <label class="label" for="user.userEmail">Email:</label>
                    <label>
                        <input id="user.userEmail" name="user.userEmail" class="input"
                               placeholder="enter email" type="text">
                    </label>
                </div>
            </div>
            <div class="columns">
                <div class="column is-one-third">
                    <label class="label" for="user.userDescription">Description:</label>
                    <p class="control">
                        <label>
                            <textarea id="user.userDescription" name="user.userDescription" class="textarea"
                                      placeholder="Description"></textarea>
                        </label>
                    </p>
                </div>
            </div>
            <div class="column">
                <input type="hidden" name="commandName" value="${CommandType.REGISTER_USER}">
                <input class="button is-primary" type="submit" value="DONE"><br>
            </div>
        </form>
    </div>
</div>
