<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="by.training.bnm.command.CommandType" %>

<div class="container">
    <div class="block">
        <h1 class="title is-bold">Add object page:</h1>
        <form method="post">
            <div class="columns">
                <div class="column is-one-quarter">
                    <label class="label">Object name:
                    </label>
                    <label class="label" for="contractItem.objectName">
                        <input id="contractItem.objectName" name="contractItem.objectName"
                               class="input" placeholder="enter object name"
                               type="text">
                    </label>
                </div>
                <div class="column">
                </div>
            </div>
            <div class="columns">
                <div class="column is-one-third">
                    <label class="label">Distance:
                    </label>
                    <div class="select">
                        <label for="contractItem.distance">
                            <select id="contractItem.distance" name="contractItem.distance">
                                <option>Same city</option>
                                <option>Close (10 - 50 km)</option>
                                <option>Middle (50 - 150 km)</option>
                                <option>Far (150 - 300 km)</option>
                            </select>
                        </label>
                    </div>
                </div>
                <div class="column">
                </div>
            </div>
            <div class="columns">
                <div class="column is-one-third">
                    <label class="label">Responsibility:
                    </label>
                    <div class="select">
                    <label for="contractItem.responsibilityCategory">
                        <select id="contractItem.responsibilityCategory" name="contractItem.responsibilityCategory">
                            <option>NO CATEGORY</option>
                            <option>LOW CATEGORY</option>
                            <option>MIDDLE CATEGORY</option>
                            <option>HIGH CATEGORY</option>
                        </select>
                    </label>
                    </div>
                </div>
            </div>
            <div class="columns">
                <div class="column is-one-third">
                    <div class="field">
                        <label class="label">Description:</label>
                        <p class="control">
                            <label for="contractItem.description">
                                <textarea id="contractItem.description" name="contractItem.description" class="textarea" placeholder="Description"></textarea>
                            </label>
                        </p>
                    </div>
                </div>
            </div>
            <div class="columns">
                <div class="column is-one-quarter">
                    <input type="hidden" name="commandName" value="${CommandType.ADD_OBJECT}">
                    <input type="hidden" name="user.id" value="${user.id}">
                    <input class="button is-primary" type="submit" value="DONE"/><br>
                </div>
                <div class="column">
                </div>
            </div>
        </form>

    </div>
</div>
