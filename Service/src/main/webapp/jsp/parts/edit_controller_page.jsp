<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="by.training.bnm.command.CommandType" %>

<div class="container">
    <div class="block">
        <h1 class="title is-bold">Edit controller page:</h1>
        <c:if test="${not empty errors}">
            <p class="error">
                    ${errors}
            </p>
        </c:if>
        <form method="post">
            <div class="columns">
                <div class="column is-one-quarter">
                    <label class="label">Controller name:
                    </label>
                    <label class="label" for="controllerName">
                        <input id="controllerName" name="controllerName" class="input" placeholder="enter mac-address"
                               type="text" value="${controller.controllerName}">
                    </label>
                </div>
                <div class="column">
                </div>
            </div>
            <div class="columns">
                <div class="column is-one-quarter">
                    <label class="label">MAC-address:
                    </label>
                    <label class="label" for="macAddress">
                        <input id="macAddress" name="macAddress" class="input" placeholder="Enter mac-address"
                               type="text" value="${controller.macAddress}">
                    </label>
                </div>
                <div class="column">
                </div>
            </div>
            <div class="columns">
                <div class="column is-one-quarter">
                    <label class="label">Description:</label>
                    <p class="control">
                        <label for="description">
                            <textarea id="description" name="description" class="textarea" placeholder="Description">
                                ${controller.description}
                            </textarea>
                        </label>
                    </p>
                </div>
                <div class="column">
                </div>
            </div>
            <div class="columns">
                <div class="column is-one-quarter">
                    <input type="hidden" name="commandName" value="${CommandType.EDIT_CONTROLLER}">
                    <input type="hidden" name="user.id" value="${user.id}">
                    <input type="hidden" name="item.id" value="${item.id}">
                    <input class="button is-primary" type="submit" value="ENTER" >
                </div>
                <div class="column">
                </div>
            </div>
        </form>

    </div>
</div>
