<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="by.training.bnm.command.CommandType" %>


<div class="columns">
    <div class="column">
        <form>
            <a class="button is-primary" type="submit" href="?commandName=${CommandType.LOGIN_PAGE}">LOGIN</a>
            <br>
        </form>
        <form>
            <a class="button is-info" type="submit" href="?commandName=${CommandType.REGISTER_PAGE}">REGISTRATION</a>
        </form>
    </div>
    <div class="column is-three-quarters">
        <figure class="image is-300x300">
            <img alt="builders picture" src="jsp/parts/img/banner2.png">
        </figure>
    </div>
</div>


