<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="by.training.bnm.command.CommandType" %>

<div class="container">
    <div class="block">
        <h1 class="title is-bold">Add controller page:</h1>
        <c:if test="${not empty errors}">
            <p class="error">
                    ${errors}
            </p>
        </c:if>
        <form method="post">
            <div class="columns">
                <div class="column is-one-quarter">
                    <label class="label">Controller name:
                    </label>
                    <label class="label" for="controllerName">
                        <input id="controllerName" name="controllerName" class="input" placeholder="enter mac-address"
                               type="text">
                    </label>
                </div>
                <div class="column">
                </div>
            </div>
            <div class="columns">
                <div class="column is-one-quarter">
                    <label class="label">MAC-address:
                    </label>
                    <label class="label" for="macAddress">
                        <input id="macAddress" name="macAddress" class="input" placeholder="enter mac-address"
                               type="text">
                    </label>
                </div>
                <div class="column">
                </div>
            </div>
            <div class="columns">
                <div class="column is-one-quarter">
                    <label class="label">Description:</label>
                    <p class="control">
                        <label for="description">
                            <textarea id="description" name="description" class="textarea"
                                      placeholder="description"></textarea>
                        </label>
                    </p>
                </div>
                <div class="column">
                </div>
            </div>
            <div class="columns">
                <div class="column is-one-quarter">
                    <input type="hidden" name="commandName" value="${CommandType.ADD_CONTROLLER}">
                    <input class="button is-primary" type="submit" value="ENTER">
                </div>
                <div class="column">
                </div>
            </div>
        </form>

    </div>
</div>
