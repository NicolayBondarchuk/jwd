<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="by.training.bnm.command.CommandType" %>

<html>
<head>
    <title>Index Page</title>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="ie=edge" http-equiv="X-UA-Compatible">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.4.2/css/bulma.css" rel="stylesheet">
</head>

<body><!-- NAVBAR -->

<fmt:setBundle basename="/i18n/ApplicationMessages" scope="application"/>
<div class="block">
    <section class="hero is-success">
        <nav class="nav">
            <div class="nav-left">
                <span class="icon is-large">
                    <form>
                        <a class="fa fa-home" href="?commandName=${CommandType.INDEX_PAGE}"></a>
                    </form>
                </span>
            </div>

            <div class="nav-right">
                <a class="nav-item" href="?command=${CommandType.VIEW_WALLET}">
                    <fmt:message key="user.wallet"/>
                </a>
            </div>
            <div class="nav-right nav-menu">
                <a class="nav-item" href="#">En</a>
                <a class="nav-item" href="#">Ru</a>
            </div>
        </nav>
        <div class="hero-body">
            <div class="container has-text-danger">
                <h1 class="title is-1">
                    <fmt:message key="app.title"/>
                </h1>
                <h2 class="subtitle">

                </h2>
            </div>
        </div>
    </section>

    <jsp:include page="parts/${partName}.jsp"/>


</div>

</body>

</html>