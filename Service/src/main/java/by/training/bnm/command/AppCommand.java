package by.training.bnm.command;

import by.training.bnm.dao.DAOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.function.BiFunction;

public interface AppCommand extends BiFunction<HttpServletRequest, HttpServletResponse, String> {

    String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException, DAOException;

    @Override
    default String apply(HttpServletRequest request, HttpServletResponse response) {

        try {
            return execute(request, response);
        } catch (Exception e) {
            throw new CommandException("Failed to execute command" + this.getClass().getName(), e);
        }
    }

}
