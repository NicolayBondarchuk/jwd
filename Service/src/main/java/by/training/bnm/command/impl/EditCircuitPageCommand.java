package by.training.bnm.command.impl;

import by.training.bnm.command.AppCommand;
import by.training.bnm.command.CommandException;
import by.training.bnm.entity.hardware.Circuit;
import by.training.bnm.service.CircuitService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class EditCircuitPageCommand implements AppCommand {

    private final CircuitService circuitService;

    public EditCircuitPageCommand(CircuitService circuitService) {
        this.circuitService = circuitService;
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {

        Long circuitId = Long.valueOf(request.getParameter("circuit.id"));
        String userId = request.getParameter("user.id");

        Circuit circuit;
        try {
            circuit = circuitService.getById(circuitId);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }

        request.setAttribute("controller.id", circuit.getControllerId());
        request.setAttribute("circuit", circuit);
        request.setAttribute("user.id", userId);

        return "edit_circuit_page";
    }
}
