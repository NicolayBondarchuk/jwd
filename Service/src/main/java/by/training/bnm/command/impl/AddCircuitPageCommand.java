package by.training.bnm.command.impl;

import by.training.bnm.command.AppCommand;
import by.training.bnm.command.CommandException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AddCircuitPageCommand implements AppCommand {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {

        String controllerId = request.getParameter("controller.id");
        String userId = request.getParameter("user.id");
        request.setAttribute("controller.id", controllerId);
        request.setAttribute("user.id", userId);

        return "add_circuit_page";
    }
}
