package by.training.bnm.command.impl;

import by.training.bnm.command.AppCommand;
import by.training.bnm.command.CommandException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AddControllerPageCommand implements AppCommand {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {

        String itemId = request.getParameter("item.id");
        String userId = request.getParameter("user.id");
        request.setAttribute("item.id", itemId);
        request.setAttribute("user.id", userId);

        return "add_controller_page";
    }
}
