package by.training.bnm.command.impl;

import by.training.bnm.command.AppCommand;
import by.training.bnm.command.CommandException;
import by.training.bnm.command.CommandType;
import by.training.bnm.constant.ApplicationConstants;
import by.training.bnm.service.ElementService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AddElementCommand implements AppCommand {

    private final ElementService elementService;

    public AddElementCommand(ElementService elementService) {
        this.elementService = elementService;
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {

        String userId = request.getParameter("user.id");
        Long circuitId = Long.valueOf(request.getParameter("circuit.id"));

        String symbol = request.getParameter("symbol");
        String name = request.getParameter("name");
        String registerAddress = request.getParameter("registerAddress");
        String type = request.getParameter("type");
        String description = request.getParameter("description");

        elementService.create(circuitId, symbol, name, description, registerAddress, type);

        return "redirect:?" + ApplicationConstants.COMMAND_NAME_PARAM + "=" + CommandType.SHOW_USER.name() +
                "&userId=" + userId;
    }
}
