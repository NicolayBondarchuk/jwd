package by.training.bnm.command;

import java.util.stream.Stream;

public enum CommandType {
    INDEX_PAGE,         // *

    LOGIN_PAGE,         // *
    LOGIN,              // ??

    REGISTER_PAGE,      // *
    REGISTER_USER,      // ***
    EDIT_USER_PAGE,     // ***
    EDIT_USER,          // **
    DELETE_USER,        // ??

    SHOW_USER,          //*
    SHOW_ALL_USERS,
    SHOW_ACTIVE_USERS,

    VIEW_WALLET,
    FULL_UP_A_WALLET,
    PAY_FOR_SERVICE,

    ADD_OBJECT_PAGE,    // *
    ADD_OBJECT,         // ***
    EDIT_OBJECT_PAGE,   // *
    EDIT_OBJECT,        // **
    DELETE_OBJECT,      // **

    ADD_CONTROLLER,         // **
    ADD_CONTROLLER_PAGE,         // **
    EDIT_CONTROLLER,        // **
    EDIT_CONTROLLER_PAGE,        // **
    DELETE_CONTROLLER,      // **

    ADD_CIRCUIT_PAGE,
    ADD_CIRCUIT,
    EDIT_CIRCUIT_PAGE,
    EDIT_CIRCUIT,
    DELETE_CIRCUIT,

    ADD_ELEMENT_PAGE,
    ADD_ELEMENT,
    EDIT_ELEMENT_PAGE,
    EDIT_ELEMENT,
    DELETE_ELEMENT,



    NO_COMMAND;

    public static CommandType of (String commandName) {
        return Stream.of(CommandType.values())
                .filter(c -> c.name().equalsIgnoreCase(commandName))
                .findFirst()
                .orElse(NO_COMMAND);
    }

}
