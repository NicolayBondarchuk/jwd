package by.training.bnm.command.impl;

import by.training.bnm.command.AppCommand;
import by.training.bnm.command.CommandException;
import by.training.bnm.entity.hardware.elements.SimpleElement;
import by.training.bnm.service.ElementService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class EditElementPageCommand implements AppCommand {

    private final ElementService elementService;

    public EditElementPageCommand(ElementService elementService) {
        this.elementService = elementService;
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {

        Long elementId = Long.valueOf(request.getParameter("element.id"));
        String userId = request.getParameter("user.id");

        SimpleElement element = elementService.getById(elementId);

        request.setAttribute("element", element);
        request.setAttribute("user.id", userId);

        return "edit_element_page";
    }
}
