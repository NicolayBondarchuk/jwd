package by.training.bnm.command.impl;

import by.training.bnm.command.AppCommand;
import by.training.bnm.command.CommandException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AddElementPageCommand implements AppCommand {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {

        String circuitId = request.getParameter("circuit.id");
        String userId = request.getParameter("user.id");

        request.setAttribute("circuit.id", circuitId);
        request.setAttribute("user.id", userId);

        return "add_element_page";
    }
}
