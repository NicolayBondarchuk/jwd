package by.training.bnm.command.impl;

import by.training.bnm.command.AppCommand;
import by.training.bnm.command.CommandException;
import by.training.bnm.entity.user.UserAccount;
import by.training.bnm.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

public class LoginCommand implements AppCommand {
    private static final Logger LOGGER = LogManager.getLogger(LoginCommand.class);

    private final UserService userService;

    public LoginCommand(UserService userService) {
        this.userService = userService;
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {

        Optional<String> userNameOptional = Optional.ofNullable(request.getParameter("user.userName"));
        Optional<String> userPasswordOptional = Optional.ofNullable(request.getParameter("user.userPassword"));

        String userName = userNameOptional.orElse("no user");
        String userPassword = userPasswordOptional.orElse("no password");

        UserAccount userAccount = userService.getById(userName);

        // ??????

        return "user_page";
    }
}
