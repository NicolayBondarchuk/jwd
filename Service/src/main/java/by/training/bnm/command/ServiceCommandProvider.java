package by.training.bnm.command;

import java.util.EnumMap;
import java.util.Map;

public class ServiceCommandProvider implements CommandProvider {
    private final Map<CommandType, AppCommand> commands;

    public ServiceCommandProvider() {
        commands = new EnumMap<>(CommandType.class);
    }

    @Override
    public AppCommand getItem(String commandName) {
        CommandType commandType = CommandType.of(commandName);
        return commands.get(commandType);
    }

    @Override
    public void registerItem(CommandType commandType, AppCommand command) {
        commands.put(commandType, command);
    }

    @Override
    public void removeItem(CommandType commandType) {
        commands.remove(commandType);
    }
}
