package by.training.bnm.command.impl;

import by.training.bnm.command.AppCommand;
import by.training.bnm.command.CommandType;
import by.training.bnm.constant.ApplicationConstants;
import by.training.bnm.dao.DAOException;
import by.training.bnm.service.ControllerService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class EditControllerCommand implements AppCommand {

    private static final Logger LOGGER = LogManager.getLogger(EditControllerCommand.class);

    private final ControllerService controllerService;

    public EditControllerCommand(ControllerService controllerService) {
        this.controllerService = controllerService;
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws DAOException {

        String userId = request.getParameter("user.id");
        Long controllerId = Long.valueOf(request.getParameter("controller.id"));
        Long contractItemId = Long.valueOf(request.getParameter("item.id"));
        String controllerName = request.getParameter("controllerName");
        String macAddress = request.getParameter("macAddress");
        String description = request.getParameter("description");

        String redirect;
        try {
            controllerService.update(controllerId, contractItemId, controllerName, macAddress, description);
            redirect = "redirect:?" + ApplicationConstants.COMMAND_NAME_PARAM + "=" + CommandType.SHOW_USER.name() +
                    "&userId=" + userId;
        } catch (DAOException e){
            LOGGER.info("error: ", e);
            request.setAttribute("errors", "controller with that mac-address is already exist");
            redirect = CommandType.ADD_CONTROLLER.name();
        }

        return redirect;
        
    }
}
