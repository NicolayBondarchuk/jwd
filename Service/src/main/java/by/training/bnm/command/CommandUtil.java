package by.training.bnm.command;

import by.training.bnm.constant.ApplicationConstants;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

public class CommandUtil {

    public static String getCommandFromRequest(HttpServletRequest request) {

        String commandName = request.getParameter(ApplicationConstants.COMMAND_NAME_PARAM);
        Optional<String> attribute = Optional.ofNullable(commandName);

        return attribute.orElse("index_page");

    }
}
