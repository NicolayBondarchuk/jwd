package by.training.bnm.command.impl;

import by.training.bnm.command.AppCommand;
import by.training.bnm.command.CommandException;
import by.training.bnm.command.CommandType;
import by.training.bnm.constant.ApplicationConstants;
import by.training.bnm.service.CircuitService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class EditCircuitCommand implements AppCommand {

    private final CircuitService circuitService;

    public EditCircuitCommand(CircuitService circuitService) {
        this.circuitService = circuitService;
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {

        String userId = request.getParameter("user.id");
        Long circuitId = Long.valueOf(request.getParameter("circuit.id"));
        Long controllerId = Long.valueOf(request.getParameter("controller.id"));
        String circuitName = request.getParameter("circuitName");
        Double setPoint = Double.valueOf(request.getParameter("setPoint"));
        Double alarm = Double.valueOf(request.getParameter("alarm"));
        String description = request.getParameter("description");

        try {
            circuitService.update(circuitId, controllerId, circuitName, setPoint, alarm, description);
        } catch (ServiceException e) {

        }

        return "redirect:?" + ApplicationConstants.COMMAND_NAME_PARAM + "=" + CommandType.SHOW_USER +
                "&userId=" + userId;
    }
}
