package by.training.bnm.command.impl;

import by.training.bnm.command.AppCommand;
import by.training.bnm.command.CommandException;
import by.training.bnm.entity.user.UserAccount;
import by.training.bnm.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class EditUserPageCommand implements AppCommand {

    private final UserService userService;

    public EditUserPageCommand(UserService userService) {
        this.userService = userService;
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {

        Long userId = Long.valueOf(request.getParameter("user.id"));
        UserAccount user = userService.getById(userId);

        request.setAttribute("user", user);

        return "edit_user_page";
    }
}
