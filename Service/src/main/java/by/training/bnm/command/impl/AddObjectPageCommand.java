package by.training.bnm.command.impl;

import by.training.bnm.command.AppCommand;
import by.training.bnm.command.CommandException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AddObjectPageCommand implements AppCommand {


    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {

        String userId = request.getParameter("user.id");

        request.setAttribute("user.id", userId);

        return "add_object_page";
    }
}
