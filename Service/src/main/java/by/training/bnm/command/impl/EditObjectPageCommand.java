package by.training.bnm.command.impl;

import by.training.bnm.command.AppCommand;
import by.training.bnm.command.CommandException;
import by.training.bnm.entity.contract.ContractItem;
import by.training.bnm.service.ContractItemService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class EditObjectPageCommand implements AppCommand {

    private final ContractItemService contractItemService;

    public EditObjectPageCommand(ContractItemService contractItemService) {
        this.contractItemService = contractItemService;
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {

        Long itemId = Long.valueOf(request.getParameter("item.id"));
        ContractItem item = contractItemService.getById(itemId);

        String userId = request.getParameter("user.id");
        request.setAttribute("item", item);
        request.setAttribute("user.id", userId);

        return "edit_object_page";
    }
}
