package by.training.bnm.command.impl;

import by.training.bnm.command.AppCommand;
import by.training.bnm.command.CommandException;
import by.training.bnm.command.CommandType;
import by.training.bnm.constant.ApplicationConstants;
import by.training.bnm.service.CircuitService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AddCircuitCommand implements AppCommand {

    private final CircuitService circuitService;

    public AddCircuitCommand(CircuitService circuitService) {
        this.circuitService = circuitService;
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {

        String userId = request.getParameter("user.id");
        Long controllerId = Long.valueOf(request.getParameter("controller.id"));
        String circuitName = request.getParameter("circuitName");
        Double setPoint = Double.valueOf(request.getParameter("setPoint"));
        Double alarm = Double.valueOf(request.getParameter("alarm"));
        String description = request.getParameter("description");

        circuitService.create(controllerId, circuitName, setPoint, alarm, description);

        return  "redirect:?" + ApplicationConstants.COMMAND_NAME_PARAM + "=" + CommandType.SHOW_USER.name() +
                "&userId" + userId;
    }
}
