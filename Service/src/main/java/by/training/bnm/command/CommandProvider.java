package by.training.bnm.command;

import by.training.bnm.provider.AppProvider;

public interface CommandProvider extends AppProvider<CommandType, AppCommand> {

}
