package by.training.bnm.command.impl;

import by.training.bnm.command.AppCommand;
import by.training.bnm.command.CommandException;
import by.training.bnm.command.CommandType;
import by.training.bnm.constant.ApplicationConstants;
import by.training.bnm.service.CircuitService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DeleteCircuitCommand implements AppCommand {

    private final CircuitService circuitService;

    public DeleteCircuitCommand(CircuitService circuitService) {
        this.circuitService = circuitService;
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {

        Long circuitId = Long.valueOf(request.getParameter("circuit.id"));
        String userId = request.getParameter("user.id");

        circuitService.remove(circuitId);

        return "redirect:?" + ApplicationConstants.COMMAND_NAME_PARAM + "=" + CommandType.SHOW_USER +
                "&userId=" + userId;
    }
}
