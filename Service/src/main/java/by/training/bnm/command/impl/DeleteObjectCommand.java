package by.training.bnm.command.impl;

import by.training.bnm.command.AppCommand;
import by.training.bnm.command.CommandException;
import by.training.bnm.command.CommandType;
import by.training.bnm.constant.ApplicationConstants;
import by.training.bnm.service.ContractItemService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DeleteObjectCommand implements AppCommand {

    private final ContractItemService service;

    public DeleteObjectCommand(ContractItemService service) {
        this.service = service;
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {

        Long id = Long.parseLong(request.getParameter("item.id"));
        String userId = request.getParameter("user.id");

        service.remove(id);

        return "redirect:?" + ApplicationConstants.COMMAND_NAME_PARAM + "=" + CommandType.SHOW_USER.name() +
                "&userId=" + userId;
    }
}
