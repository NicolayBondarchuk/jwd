package by.training.bnm.command.impl;

import by.training.bnm.command.AppCommand;
import by.training.bnm.command.CommandException;
import by.training.bnm.command.CommandType;
import by.training.bnm.constant.ApplicationConstants;
import by.training.bnm.service.ControllerService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AddControllerCommand implements AppCommand {

    private final ControllerService controllerService;

    public AddControllerCommand(ControllerService controllerService) {
        this.controllerService = controllerService;
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {

        String userId = request.getParameter("user.id");
        String itemId = request.getParameter("item.id");
        String controllerName = request.getParameter("controllerName");
        String macAddress = request.getParameter("macAddress");
        String description = request.getParameter("description");

        String redirect;
        if (controllerService.create(itemId, controllerName, macAddress, description)) {
            redirect = "redirect:?" + ApplicationConstants.COMMAND_NAME_PARAM + "=" + CommandType.SHOW_USER.name() +
                    "&userId=" + userId;
        } else {
            request.setAttribute("errors", "controller with that mac-address is already exist");
            redirect = CommandType.ADD_CONTROLLER.name();
        }

        return redirect;
    }
}
