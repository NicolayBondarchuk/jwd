package by.training.bnm.command.impl;

import by.training.bnm.builder.EntityBuilder;
import by.training.bnm.builder.EntityValidator;
import by.training.bnm.builder.ValidationResult;
import by.training.bnm.command.AppCommand;
import by.training.bnm.command.CommandException;
import by.training.bnm.command.CommandType;
import by.training.bnm.constant.ApplicationConstants;
import by.training.bnm.entity.user.UserAccount;
import by.training.bnm.builder.UserBuilder;
import by.training.bnm.entity.user.UserField;
import by.training.bnm.entity.user.UserValidator;
import by.training.bnm.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class EditUserCommand implements AppCommand {

    private final UserService userService;

    public EditUserCommand(UserService userService) {
        this.userService = userService;
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {

        Map<String, String> userData = new HashMap<>();
        for (UserField userField : UserField.values()) {
            String parameter = request.getParameter("user." + userField.getFieldName());
            userData.put(userField.getFieldName(), parameter);
        }

        EntityValidator userValidator = new UserValidator();
        EntityBuilder<UserAccount> userBuilder = new UserBuilder();

        ValidationResult validationResult = userValidator.validate(userData);
        UserAccount userAccount = userBuilder.build(userData);

        if (validationResult.isValid()) {
            userService.update(userAccount);
            String redirect = request.getContextPath() + "?" + ApplicationConstants.COMMAND_NAME_PARAM + "=" + CommandType.SHOW_USER;
            try {
                response.sendRedirect(redirect);
            } catch (Exception e) {
                throw new CommandException("Failed to forward view");
            }
        } else {
            List<String> errorsFromParsing = validationResult.getMessages().stream()
                    .map(m -> {
                        String fieldName = m.getFieldName();
                        String errors = String.join(",", m.getErrors());
                        return "Field " + fieldName + " contains following errors: " + errors;
                    }).collect(Collectors.toList());
            request.setAttribute("errors", errorsFromParsing);
            request.setAttribute("user", userAccount);
            try {
                response.sendRedirect("edit_user_page");
            } catch (Exception e) {
                throw new CommandException("Failed to redirect");
            }
        }


        return null;
    }
}
