package by.training.bnm.command.impl;

import by.training.bnm.builder.EntityBuilder;
import by.training.bnm.command.AppCommand;
import by.training.bnm.command.CommandException;
import by.training.bnm.command.CommandType;
import by.training.bnm.constant.ApplicationConstants;
import by.training.bnm.entity.contract.ContractItem;
import by.training.bnm.service.ContractItemService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

public class EditObjectCommand implements AppCommand {

    private final ContractItemService service;

    public EditObjectCommand(ContractItemService service) {
        this.service = service;
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {

        String distance = request.getParameter("contractItem.distance");
        String responsibilityCategory = request.getParameter("contractItem.responsibilityCategory");
        String description = request.getParameter("contractItem.description");



        return "redirect:?" + ApplicationConstants.COMMAND_NAME_PARAM + "=" + CommandType.SHOW_USER.name();
    }
}
