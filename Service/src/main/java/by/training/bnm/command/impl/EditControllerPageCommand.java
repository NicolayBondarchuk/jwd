package by.training.bnm.command.impl;

import by.training.bnm.command.AppCommand;
import by.training.bnm.command.CommandException;
import by.training.bnm.dao.DAOException;
import by.training.bnm.entity.hardware.Controller;
import by.training.bnm.service.ControllerService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class EditControllerPageCommand implements AppCommand {

    private static final Logger LOGGER = LogManager.getLogger(EditControllerCommand.class);

    private final ControllerService controllerService;

    public EditControllerPageCommand(ControllerService controllerService) {
        this.controllerService = controllerService;
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {

        Long controllerId = Long.valueOf(request.getParameter("controller.id"));
        String itemId = request.getParameter("item.id");
        Controller controller = null;
        try {
            controller = controllerService.getById(controllerId);
        } catch (DAOException e) {

            e.printStackTrace();
        }

        String userId = request.getParameter("user.id");
        request.setAttribute("user.id", userId);
        request.setAttribute("item.id", itemId);
        request.setAttribute("controller", controller);

        return "edit_controller_page";
    }
}
