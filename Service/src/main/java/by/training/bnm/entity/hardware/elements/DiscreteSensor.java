package by.training.bnm.entity.hardware.elements;

public class DiscreteSensor extends SimpleElement {
    private Boolean value;

    public DiscreteSensor() {
    }

    public DiscreteSensor(Long id, Long circuitId, String symbol, String name, String description,
                            int registerAddress, ElementType type, Boolean value) {
        super(id, circuitId, symbol, name, description, registerAddress, type);
        this.value = value;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = (Boolean) value;
    }

    @Override
    public String toString() {
        return "DiscreteSensor{" +
                super.toString() +
                "value='" + value + '\'' +
                '}';
    }
}
