package by.training.bnm.entity.user;

import by.training.bnm.mappers.IdentifiedRow;

public class UserDiscount implements IdentifiedRow {

    private Long id;
    private Long userId;
    private UserDiscountType discountType;
    private Integer discountSize;

    public UserDiscount() {
    }

    public UserDiscount(Long id, Long userId, String discountType, Integer discountSize) {
        this.id = id;
        this.userId = userId;
        this.discountType = UserDiscountType.of(discountType);
        this.discountSize = discountSize;
    }

    public Integer getDiscountSize() {
        return discountSize;
    }

    public void setDiscountSize(Integer discountSize) {
        this.discountSize = discountSize;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public UserDiscountType getDiscountType() {
        return discountType;
    }

    public Integer getDiscountTypeInt() {
        return discountType.getDiscount();
    }

    public void setDiscountType(UserDiscountType discountType) {
        this.discountType = discountType;
    }

    public void setDiscountType(Integer number) {
        this.discountType = UserDiscountType.of(number);
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
