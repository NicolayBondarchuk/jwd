package by.training.bnm.entity.user;

import java.util.Optional;
import java.util.stream.Stream;

public enum UserRoleType {

    ADMINISTRATOR(1),
    SERVICE_STUFF(2),
    USER(3);

    private Integer key;

    UserRoleType(Integer key) {
        this.key = key;
    }

    public static UserRoleType of(String name) {
        return Stream.of(UserRoleType.values())
                .filter(type -> type.name().equalsIgnoreCase(name))
                .findFirst()
                .orElse(USER);
    }

    public static Optional<UserRoleType> ofNumber(Integer number) {
        return Stream.of(UserRoleType.values())
                .filter(numb -> numb.key == number)
                .findFirst();
    }
}
