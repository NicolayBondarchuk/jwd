package by.training.bnm.entity.user;

import java.util.stream.Stream;

public enum UserDiscountType {
    NO_DISCOUNT(1),
    LOW_DISCOUNT(2),
    MIDDLE_DISCOUNT(3),
    HIGH_DISCOUNT(4);

    private Integer discount;

    UserDiscountType(Integer discount) {
        this.discount = discount;
    }

    public static UserDiscountType of(String name) {
        return Stream.of(UserDiscountType.values())
                .filter(type -> type.name().equalsIgnoreCase(name))
                .findFirst()
                .orElse(NO_DISCOUNT);
    }

    public static UserDiscountType of(Integer number) {
        return Stream.of(UserDiscountType.values())
                .filter(type -> type.discount.equals(number))
                .findFirst()
                .orElse(NO_DISCOUNT);
    }

    public Integer getDiscount() {
        return discount;
    }
}

