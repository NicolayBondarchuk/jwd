package by.training.bnm.entity.hardware.elements;

import java.util.Optional;
import java.util.stream.Stream;

public enum ElementType {
    ANALOG_SENSOR(1),
    DISCRETE_SENSOR(2),
    ANALOG_ACTUATOR(3),
    DISCRETE_ACTUATOR(4),
    UNKNOWN_ELEMENT(5);

    private final Integer number;

    public Integer getNumber() {
         return number;
    }

    ElementType(Integer number) {
        this.number = number;
    }

    public static ElementType of(String name) {
        return Stream.of(ElementType.values())
                .filter(type -> type.name().equalsIgnoreCase(name))
                .findFirst()
                .orElse(UNKNOWN_ELEMENT);
    }

    public static Optional<ElementType> ofNumber(Integer number) {
        return Stream.of(ElementType.values())
                .filter(numb -> numb.number == number)
                .findFirst();
    }
}
