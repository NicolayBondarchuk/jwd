package by.training.bnm.entity.hardware.elements;

import by.training.bnm.mappers.IdentifiedRow;

public class SimpleElement implements IdentifiedRow {
    private Long id;
    private Long circuitId;
    private String symbol;
    private String name;
    private Integer registerAddress;
    private ElementType type;
    private String description;

    public SimpleElement() {
    }

    public SimpleElement(Long id, Long circuitId, String symbol, String name,
                         String description, Integer registerAddress, ElementType type) {
        this.id = id;
        this.circuitId = circuitId;
        this.symbol = symbol;
        this.name = name;
        this.description = description;
        this.registerAddress = registerAddress;
        this.type = type;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Long getCircuitId() {
        return circuitId;
    }

    public void setCircuitId(Long circuitId) {
        this.circuitId = circuitId;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getRegisterAddress() {
        return registerAddress;
    }

    public void setRegisterAddress(Integer registerAddress) {
        this.registerAddress = registerAddress;
    }

    @Override
    public String toString() {
        return  "\n\tid=" + id +
                ",\n\tname='" + name + '\'' +
                ",\n\tsymbol='" + symbol + '\'' +
                ",\n\tdescription='" + description + '\'' +
                ",\n\tregisterAddress='" + registerAddress + '\'';
    }

    public ElementType getType() {
        return type;
    }

    public Integer getTypeInt() {
        return type.getNumber();
    }

    public void setType(ElementType type) {
        this.type = type;
    }

    public void setTypeInt(Integer type) {
        this.type = ElementType.ofNumber(type).orElse(ElementType.UNKNOWN_ELEMENT);
    }

//    public abstract Object getValue();
//    public abstract void setValue(Object value);
}
