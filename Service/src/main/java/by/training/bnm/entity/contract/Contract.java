package by.training.bnm.entity.contract;

import by.training.bnm.mappers.IdentifiedRow;

import java.math.BigDecimal;
import java.math.BigInteger;

public class Contract implements IdentifiedRow {
    private Long id;
    private Long userAccountId;
    private BigDecimal totalCost;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserAccountId() {
        return userAccountId;
    }

    public void setUserAccountId(Long userAccountId) {
        this.userAccountId = userAccountId;
    }

    public BigDecimal getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(BigDecimal totalCost) {
        this.totalCost = totalCost;
    }
}
