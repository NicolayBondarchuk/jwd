package by.training.bnm.entity.hardware;

import by.training.bnm.connection.ConnectionPoolImpl;
import by.training.bnm.mappers.IdentifiedRow;

import java.util.ArrayList;

public class Controller implements IdentifiedRow {
    private Long id;
    private Long difficultyCategoryId;
    private String controllerName;
    private String macAddress;
    private String description;

    public Controller() {
    }

    public Controller(Long difficultyCategoryId, String controllerName, String macAddress, String description) {
        this.difficultyCategoryId = difficultyCategoryId;
        this.controllerName = controllerName;
        this.macAddress = macAddress;
        this.description = description;
    }

    public Controller(Long id, Long difficultyCategoryId, String controllerName, String macAddress, String description) {
        this.id = id;
        this.difficultyCategoryId = difficultyCategoryId;
        this.controllerName = controllerName;
        this.macAddress = macAddress;
        this.description = description;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Long getDifficultyCategoryId() {
        return difficultyCategoryId;
    }

    public void setDifficultyCategoryId(Long difficultyCategoryId) {
        this.difficultyCategoryId = difficultyCategoryId;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getControllerName() {
        return controllerName;
    }

    public void setControllerName(String controllerName) {
        this.controllerName = controllerName;
    }

}
