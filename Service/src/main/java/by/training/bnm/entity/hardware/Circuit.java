package by.training.bnm.entity.hardware;

import by.training.bnm.mappers.IdentifiedRow;

public class Circuit implements IdentifiedRow {
    private Long id;
    private Long controllerId;
    private String circuitName;
    private double setPoint;
    private double alarm;
    private String description;

    public Circuit() {
    }

    public Circuit(Long id, Long controllerId, String circuitName, double setPoint, double alarm, String description) {
        this.id = id;
        this.controllerId = controllerId;
        this.circuitName = circuitName;
        this.setPoint = setPoint;
        this.alarm = alarm;
        this.description = description;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Long getControllerId() {
        return controllerId;
    }

    public void setControllerId(Long controllerId) {
        this.controllerId = controllerId;
    }

    public double getSetPoint() {
        return setPoint;
    }

    public void setSetPoint(double setPoint) {
        this.setPoint = setPoint;
    }

    public double getAlarm() {
        return alarm;
    }

    public void setAlarm(double alarm) {
        this.alarm = alarm;
    }

    public String getCircuitName() {
        return circuitName;
    }

    public void setCircuitName(String circuitName) {
        this.circuitName = circuitName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
