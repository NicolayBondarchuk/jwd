package by.training.bnm.entity.hardware.elements;

public class AnalogActuator extends SimpleElement {
    private Integer value;

    public AnalogActuator() {
    }

    public AnalogActuator(Long id, Long circuitId, String symbol, String name, String description,
                          int registerAddress, ElementType type, Integer value) {
        super(id, circuitId, symbol, name, description, registerAddress, type);
        this.value = value;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = (Integer) value;
    }

    @Override
    public String toString() {
        return "AnalogActuator{" +
                super.toString() +
                "value='" + value + '\'' +
                '}';
    }
}
