package by.training.bnm.entity.user;

import by.training.bnm.builder.EntityValidator;
import by.training.bnm.builder.ValidationMessage;
import by.training.bnm.builder.ValidationResult;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class UserValidator implements EntityValidator {
    @Override
    public ValidationResult validate(Map<String, String> data) {
        List<ValidationMessage> validationMessages = Stream.of(UserField.values())
                .filter(f -> !data.containsKey(f.getFieldName())
                        || data.get(f.getFieldName()) == null
                        || data.get(f.getFieldName()).trim().length() == 0)
                .map(f -> new ValidationMessage(f.getFieldName(), null,
                        Collections.singletonList("missed required field")))
                .collect(Collectors.toList());

        ValidationResult validationResult = new ValidationResult();
        validationResult.addMessages(validationMessages);
        return validationResult;
    }
}
