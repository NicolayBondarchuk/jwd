package by.training.bnm.entity.user;

import by.training.bnm.mappers.IdentifiedRow;

public class UserAccount implements IdentifiedRow {
    private Long id;
    private String userName;
    private String userPassword;
    private String userEmail;
    private String userDescription;

    public UserAccount() {
    }

    public UserAccount(Long id, String userName, String userPassword,
                       String userEmail, String userDescription) {
        this.id = id;
        this.userDescription = userDescription;
        this.userName = userName;
        this.userPassword = userPassword;
        this.userEmail = userEmail;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserDescription() {
        return userDescription;
    }

    public void setUserDescription(String userDescription) {
        this.userDescription = userDescription;
    }
}
