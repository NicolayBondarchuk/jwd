package by.training.bnm.entity.hardware.elements;

public class AnalogSensor extends SimpleElement {
    private Double value;

    public AnalogSensor() {
    }

    public AnalogSensor(Long id, Long circuitId, String symbol, String name, String description,
                          int registerAddress, ElementType type, Double value) {
        super(id, circuitId, symbol, name, description, registerAddress, type);
        this.value = value;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = (Double) value;
    }

    @Override
    public String toString() {
        return "AnalogSensor{" +
                super.toString() +
                "value='" + value + '\'' +
                '}';
    }
}
