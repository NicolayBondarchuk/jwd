package by.training.bnm.entity.contract;

import java.util.Optional;
import java.util.stream.Stream;

public enum Distance {

    SAME_CITY(1.0),
    CLOSE_DISTANCE(1.2),
    MIDDLE_DISTANCE(1.6),
    FAR_DISTANCE(2.0);

    private Double coefficient;

    Distance(Double coefficient) {
        this.coefficient = coefficient;
    }

    public static Optional<Distance> of(String name) {
        return Stream.of(Distance.values())
                .filter(type -> type.name().equalsIgnoreCase(name))
                .findFirst();
    }

    public static Optional<Double> ofCoefficient(String name) {
        return Stream.of(Distance.values())
                .filter(type -> type.name().equalsIgnoreCase(name))
                .findFirst().map(Distance::getCoefficient);
    }

    public double getCoefficient() {
        return coefficient;
    }
}
