package by.training.bnm.entity.contract;

import java.util.Optional;
import java.util.stream.Stream;

public enum ResponsibilityCategory {

    NO_CATEGORY(1.0),
    LOW_CATEGORY(1.3),
    MIDDLE_CATEGORY(1.6),
    HIGH_CATEGORY(2.2);

    private Double coefficient;

    ResponsibilityCategory(Double coefficient) {
        this.coefficient = coefficient;
    }

    public static Optional<ResponsibilityCategory> of(String name) {
        return Stream.of(ResponsibilityCategory.values())
                .filter(type -> type.name().equalsIgnoreCase(name))
                .findFirst();
    }

    public static Optional<Double> ofCoefficient(String name) {
        return Stream.of(ResponsibilityCategory.values())
                .filter(type -> type.name().equalsIgnoreCase(name))
                .findFirst().map(ResponsibilityCategory::getCoefficient);
    }

    public Double getCoefficient() {
        return coefficient;
    }
}
