package by.training.bnm.entity.user;

import java.util.stream.Stream;

public enum UserRatingType {
    START(1),
    MINIMUM(2),
    AVERAGE(3),
    MAXIMUM(4);

    private Integer key;

    UserRatingType(Integer key) {
        this.key = key;
    }

    public static UserRatingType of(String name) {
        return Stream.of(UserRatingType.values())
                .filter(type -> type.name().equalsIgnoreCase(name))
                .findFirst()
                .orElse(START);
    }

    public static UserRatingType of(Integer key) {
        return Stream.of(UserRatingType.values())
                .filter(type -> type.key.equals(key))
                .findFirst()
                .orElse(START);
    }

    public Integer getKey() {
        return key;
    }
}
