package by.training.bnm.entity.user;

import by.training.bnm.mappers.IdentifiedRow;

import java.util.HashMap;
import java.util.Map;

public class UserRating implements IdentifiedRow {
    private Long id;
    private Long userId;
    private UserRatingType ratingType;
    private Integer starsCounter;
    private Map<UserRatingType, Integer> discount;


    public UserRating() {
        discount = new HashMap<>(UserRatingType.values().length);
        discount.put(UserRatingType.START, 0);
        discount.put(UserRatingType.MINIMUM, 5);
        discount.put(UserRatingType.AVERAGE, 10);
        discount.put(UserRatingType.MAXIMUM, 15);
    }

    public UserRating(Long id, Long userId, int starsCounter) {
        this.id = id;
        this.userId = userId;
        this.starsCounter = starsCounter;
        discount = new HashMap<>(UserRatingType.values().length);
        discount.put(UserRatingType.START, 0);
        discount.put(UserRatingType.MINIMUM, 5);
        discount.put(UserRatingType.AVERAGE, 10);
        discount.put(UserRatingType.MAXIMUM, 15);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserRatingType getRatingType() {
        return ratingType;
    }

    public void setRatingType(UserRatingType ratingType) {
        this.ratingType = ratingType;
    }

    public void setRatingType(Integer type) {
        this.ratingType = UserRatingType.of(type);
    }

    public int getStarsCounter() {
        return starsCounter;
    }

    public void setStarsCounter(int starsCounter) {
        this.starsCounter = starsCounter;
    }

    public void addStar() {
        if (starsCounter <= 20) {
            starsCounter++;
        }
    }

    public void removeStar() {
        if (starsCounter > 0) {
            starsCounter--;
        }
    }

    public Map<UserRatingType, Integer> getDiscount() {
        return discount;
    }

    public UserRatingType getCurrentRating() {
        if (starsCounter < 5) {
            return UserRatingType.START;
        } else if (starsCounter < 10) {
            return UserRatingType.MINIMUM;
        } else if (starsCounter < 15) {
            return UserRatingType.AVERAGE;
        } else {
            return UserRatingType.MAXIMUM;
        }
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public int getRatingTypeInt() {
        return ratingType.getKey();
    }
}
