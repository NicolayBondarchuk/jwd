package by.training.bnm.entity.user;

import by.training.bnm.dao.DAOException;
import by.training.bnm.mappers.IdentifiedRow;

public class UserRole implements IdentifiedRow {

    private Long id;
    private UserRatingType ratingType;
    private Integer starsCounter;

    public UserRole() {
    }

    public UserRole(Long id, UserRatingType ratingType, Integer starsCounter) {
        this.id = id;
        this.ratingType = ratingType;
        this.starsCounter = starsCounter;
    }

    public UserRatingType getRatingType() {
        return ratingType;
    }

    public void setRatingType(UserRatingType ratingType) {
        this.ratingType = ratingType;
    }

    public Integer getStarsCounter() {
        return starsCounter;
    }

    public void setStarsCounter(Integer starsCounter) {
        this.starsCounter = starsCounter;
    }

    @Override
    public Long getId() throws DAOException {
        return id;
    }

    @Override
    public void setId(Long id) throws DAOException {
        this.id = id;
    }
}
