package by.training.bnm.entity.user;

import by.training.bnm.mappers.IdentifiedRow;

import java.math.BigDecimal;

public class UserWallet implements IdentifiedRow {
    private Long id;
    private Long userId;
    private BigDecimal userAmount;

    public UserWallet() {
    }

    public UserWallet(BigDecimal amount) {
        this.userAmount = amount;
    }

    public UserWallet(Long id, Long userId, BigDecimal amount) {
        this.id = id;
        this.userId = userId;
        this.userAmount = amount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getUserAmount() {
        return userAmount;
    }

    public void setUserAmount(BigDecimal userAmount) {
        this.userAmount = userAmount;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
