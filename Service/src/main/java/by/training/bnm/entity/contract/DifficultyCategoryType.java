package by.training.bnm.entity.contract;

import java.util.Optional;
import java.util.stream.Stream;

public enum DifficultyCategoryType {

    OBSERVER(1),
    EASY(2),
    MIDDLE(3),
    DIFFICULT(4);

    private Integer coefficient;

    DifficultyCategoryType(Integer coefficient) {
        this.coefficient = coefficient;
    }

    public Integer getCoefficient() {
        return coefficient;
    }

    public static Optional<Integer> ofCoefficient(String name) {
        return Stream.of(DifficultyCategoryType.values())
                .filter(type -> type.name().equalsIgnoreCase(name))
                .findFirst().map(DifficultyCategoryType::getCoefficient);
    }

    public static Optional<DifficultyCategoryType> ofNumber(Integer number) {
        return Stream.of(DifficultyCategoryType.values())
                .filter(type -> type.coefficient == number)
                .findFirst();
    }

    public static Optional<DifficultyCategoryType> of(String name) {
        return Stream.of(DifficultyCategoryType.values())
                .filter(type -> type.name().equalsIgnoreCase(name))
                .findFirst();
    }
}
