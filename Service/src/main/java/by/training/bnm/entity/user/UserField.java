package by.training.bnm.entity.user;

import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.stream.Stream;

public enum UserField {
    USER_NAME("userName", UserAccount::setUserName),
    USER_PASSWORD("userPassword", UserAccount::setUserPassword),
    USER_EMAIL("userEmail", UserAccount::setUserEmail),
    USER_DESCRIPTION("userDescription", UserAccount::setUserDescription);

    private final String fieldName;
    private final BiConsumer<UserAccount, String> fieldMapper;

    UserField(String fieldName, BiConsumer<UserAccount, String> fieldMapper) {
        this.fieldName = fieldName;
        this.fieldMapper = fieldMapper;
    }

    public static Optional<UserField> of(String fieldName) {
        return Stream.of(UserField.values()).filter(f -> f.fieldName.equals(fieldName)).findFirst();
    }

    public String getFieldName() {
        return fieldName;
    }

    public BiConsumer<UserAccount, String> getFieldMapper() {
        return fieldMapper;
    }
}
