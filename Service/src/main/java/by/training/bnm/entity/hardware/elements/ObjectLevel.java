package by.training.bnm.entity.hardware.elements;

import java.util.stream.Stream;

public enum ObjectLevel {
    OBJECT,
    CONTROLLER,
    CIRCUIT,
    ELEMENT,
    NO_ELEMENT;

    public static ObjectLevel of(String levelName) {
        return Stream.of(ObjectLevel.values())
                .filter(l -> l.name().equalsIgnoreCase(levelName))
                .findFirst()
                .orElse(NO_ELEMENT);
    }
}
