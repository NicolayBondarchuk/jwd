package by.training.bnm.entity.contract;

import by.training.bnm.mappers.IdentifiedRow;

import java.math.BigDecimal;

public class ContractItem implements IdentifiedRow {
    private Long id;
    private Long contractId;
    private String itemName;
    private BigDecimal itemCost;
    private ResponsibilityCategory responsibilityCategory;
    private Distance distance;
    private String description;

    public ContractItem() {
    }

    public ContractItem(String itemName,
                        ResponsibilityCategory responsibilityCategory, Distance distance,
                        String description) {
        this.itemName = itemName;
        this.responsibilityCategory = responsibilityCategory;
        this.distance = distance;
        this.description = description;
    }

    public ContractItem(Long id, Long contractId, String itemName, BigDecimal itemCost,
                        ResponsibilityCategory responsibilityCategory, Distance distance,
                        String description) {
        this.id = id;
        this.contractId = contractId;
        this.itemName = itemName;
        this.itemCost = itemCost;
        this.responsibilityCategory = responsibilityCategory;
        this.distance = distance;
        this.description = description;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Long getContractId() {
        return contractId;
    }

    public void setContractId(Long contractId) {
        this.contractId = contractId;
    }

    public BigDecimal getItemCost() {
        return itemCost;
    }

    public void setItemCost(BigDecimal itemCost) {
        this.itemCost = itemCost;
    }

    public String getResponsibilityCategory() {
        return responsibilityCategory.name();
    }

    public void setResponsibilityCategory(String responsibilityCategory) {
        this.responsibilityCategory = ResponsibilityCategory.of(responsibilityCategory)
                .orElse(ResponsibilityCategory.NO_CATEGORY);
    }

    public String getDistance() {
        return distance.name();
    }

    public void setDistance(String distance) {
        this.distance = Distance.of(distance).orElse(Distance.SAME_CITY);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

}
