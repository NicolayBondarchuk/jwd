package by.training.bnm.entity.hardware.elements;

public class DiscreteActuator extends SimpleElement {
    private Boolean value;

    public DiscreteActuator() {
    }

    public DiscreteActuator(Long id, Long circuitId, String symbol, String name, String description,
                        int registerAddress, ElementType type, Boolean value) {
        super(id, circuitId, symbol, name, description, registerAddress, type);
        this.value = value;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = (Boolean) value;
    }

    @Override
    public String toString() {
        return "DiscreteActuator{" +
                super.toString() +
                "value='" + value + '\'' +
                '}';
    }
}
