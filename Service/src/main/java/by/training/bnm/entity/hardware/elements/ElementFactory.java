package by.training.bnm.entity.hardware.elements;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ElementFactory {
    private static final Map<ElementType, SimpleElement> elements =
            new ConcurrentHashMap<>(ElementType.values().length);

    static {
        elements.put(ElementType.ANALOG_SENSOR, new AnalogSensor());
        elements.put(ElementType.DISCRETE_SENSOR, new DiscreteSensor());
        elements.put(ElementType.ANALOG_ACTUATOR, new AnalogActuator());
        elements.put(ElementType.DISCRETE_ACTUATOR, new DiscreteActuator());
        elements.put(ElementType.UNKNOWN_ELEMENT, new UnknownElement());
    }

    public static SimpleElement getElement(String element) {
        return elements.get(ElementType.of(element));
    }
}
