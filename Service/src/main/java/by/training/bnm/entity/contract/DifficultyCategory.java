package by.training.bnm.entity.contract;

import by.training.bnm.mappers.IdentifiedRow;

public class DifficultyCategory implements IdentifiedRow {
    private Long id;
    private Long contractItemId;
    private DifficultyCategoryType categoryType;

    public DifficultyCategory() {
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Long getContractItemId() {
        return contractItemId;
    }

    public void setContractItemId(Long contractItemId) {
        this.contractItemId = contractItemId;
    }

    public DifficultyCategoryType getCategoryType() {
        return categoryType;
    }

    public Integer getCategoryTypeInt() {
        return categoryType.getCoefficient();
    }

    public void setCategoryType(DifficultyCategoryType categoryType) {
        this.categoryType = categoryType;
    }

    public void setCategoryType(int type) {
        this.categoryType = DifficultyCategoryType.ofNumber(type).orElse(DifficultyCategoryType.OBSERVER);
    }
}
