package by.training.bnm.connection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ConnectionManager {

    private static final Logger LOGGER = LogManager.getLogger(ConnectionManager.class);
    private static final Lock lock = new ReentrantLock();
    private static volatile ConnectionManager INSTANCE;

    private final ConnectionPool connectionPool;
    private ConnectionManager(ConnectionPool connectionPool) {
        this.connectionPool = connectionPool;
    }

    public static ConnectionManager getInstance(ConnectionPool connectionPool) {

        if (INSTANCE == null) {
            lock.lock();
            if (INSTANCE == null) {
                INSTANCE = new ConnectionManager(connectionPool);
                LOGGER.info("Connection manager is created");
            }
            lock.unlock();
        }
        return INSTANCE;
    }

    public Connection getConnection() throws ConnectionException {
        return connectionPool.getConnection();
    }
}
