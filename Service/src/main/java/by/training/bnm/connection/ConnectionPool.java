package by.training.bnm.connection;

import java.sql.Connection;

public interface ConnectionPool {
    Connection getConnection() throws ConnectionException;
}
