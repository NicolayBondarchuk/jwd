package by.training.bnm.connection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.Properties;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static by.training.bnm.constant.ApplicationConstants.*;

public class ConnectionPoolImpl implements ConnectionPool {

    private static final Logger LOGGER = LogManager.getLogger(ConnectionPoolImpl.class);

    private final String driver;
    private final String jdbcUrl;
    private final String userAccount;
    private final String password;
    private final int poolCapacity;

    private final Lock connectionLock = new ReentrantLock();
    private final Condition emptyPool = connectionLock.newCondition();

    private final LinkedList<Connection> avaliableConnection = new LinkedList<>();
    private final LinkedList<Connection> usedConnection = new LinkedList<>();

    public ConnectionPoolImpl() {
        this.poolCapacity = Integer.parseInt(DATASOURCE_POOL_SIZE);

        try (InputStream resource = ConnectionPoolImpl.class.getResourceAsStream(DATASOURCE_PROPERTIES)) {

            Properties properties = new Properties();
            properties.load(resource);
            this.driver = properties.getProperty(DATASOURCE_DRIVER);
            this.jdbcUrl = properties.getProperty(DATASOURCE_URL);
            this.userAccount = properties.getProperty(DATASOURCE_USER_ACCOUNT);
            this.password = properties.getProperty(DATASOURCE_PASSWORD);

            initDriver(this.driver);

        } catch (IOException e) {
            LOGGER.error("Properties was not found");
            throw new IllegalStateException(e.getMessage(), e);
        }
    }

    @Override
    public Connection getConnection() throws ConnectionException {

        connectionLock.lock();
        Connection proxyConnection = null;
        try {
            if (avaliableConnection.isEmpty() && usedConnection.size() == poolCapacity) {
                try {
                    emptyPool.await();
                } catch (InterruptedException e) {
                    LOGGER.error(e.getMessage());
                    throw new IllegalThreadStateException(e.getMessage());
                }

            }
            if (avaliableConnection.isEmpty() && usedConnection.size() < poolCapacity) {
                Connection connection = DriverManager.getConnection(jdbcUrl, userAccount, password);
                avaliableConnection.add(connection);
            } else if (avaliableConnection.isEmpty()) {
                throw new IllegalStateException("Get maximum pool size was reached");
            }
            Connection connection = avaliableConnection.removeFirst();
            usedConnection.add(connection);
            proxyConnection = createProxyConnection(connection);
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new ConnectionException(e);
        } finally {
            connectionLock.unlock();
        }

        return proxyConnection;
    }

    private void initDriver(String driver) {
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException e) {
            LOGGER.error(e);
            throw new IllegalStateException("Driver wasn't found", e);
        }
    }

    private Connection createProxyConnection(Connection connection) {
        return (Connection) Proxy.newProxyInstance(connection.getClass().getClassLoader(),
                new Class[]{Connection.class},
                ((proxy, method, args) -> {
                    if ("close".equals(method.getName())) {
                        releaseConnection(connection);
                        return null;
                    } else if ("hashCode".equals(method.getName())) {
                        return connection.hashCode();
                    } else {
                        return method.invoke(connection, args);
                    }
                }));
    }

    private void releaseConnection(Connection connection) {
        try {
            connectionLock.lock();
            if (avaliableConnection.size() >= poolCapacity) {
                throw new IllegalStateException("Release maximum pool size was reached");
            }
            if (!connection.isClosed()) {
                connection.close();
            }
            usedConnection.remove(connection);
            avaliableConnection.add(connection);
            emptyPool.signal();
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        } finally {
            connectionLock.unlock();
        }
    }
}
