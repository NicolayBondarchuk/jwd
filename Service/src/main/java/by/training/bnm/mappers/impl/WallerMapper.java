package by.training.bnm.mappers.impl;

import by.training.bnm.entity.user.UserWallet;
import by.training.bnm.mappers.IdentifiedRowMapper;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

public class WallerMapper implements IdentifiedRowMapper<UserWallet> {

    @Override
    public UserWallet map(ResultSet resultSet) throws SQLException {

        UserWallet userWallet = new UserWallet();
        userWallet.setUserId(resultSet.getLong("user_account_id"));
        userWallet.setUserAmount(resultSet.getBigDecimal("amount"));

        return userWallet;
    }

    @Override
    public List<String> getColumnNames() {
        return Arrays.asList("user_account_id", "amount");
    }

    @Override
    public void populateStatement(PreparedStatement statement, UserWallet entity) throws SQLException {

        int i = 0;
        statement.setLong(++i, entity.getUserId());
        statement.setBigDecimal(++i, entity.getUserAmount());
    }
}
