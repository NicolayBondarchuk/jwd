package by.training.bnm.mappers;

import by.training.bnm.dao.DAOException;

public interface IdentifiedRow {
    Long getId() throws DAOException;

    void setId(Long id) throws DAOException;
}
