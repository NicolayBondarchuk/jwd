package by.training.bnm.mappers.impl;

import by.training.bnm.entity.contract.ContractItem;
import by.training.bnm.mappers.IdentifiedRowMapper;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

public class ContractItemMapper implements IdentifiedRowMapper<ContractItem> {
    @Override
    public ContractItem map(ResultSet resultSet) throws SQLException {

        ContractItem contractItem = new ContractItem();
        contractItem.setId(resultSet.getLong("id"));
        contractItem.setContractId(resultSet.getLong("contract_id"));
        contractItem.setItemName(resultSet.getString("item_name"));
        contractItem.setItemCost(resultSet.getBigDecimal("item_cost"));
        contractItem.setResponsibilityCategory(resultSet.getString("responsibility_category"));
        contractItem.setDistance(resultSet.getString("distance"));
        contractItem.setDescription(resultSet.getString("description"));

        return contractItem;
    }

    @Override
    public List<String> getColumnNames() {
        return Arrays.asList("contract_id", "item_name", "item_cost", "responsibility_category",
                "distance", "description");
    }

    @Override
    public void populateStatement(PreparedStatement statement, ContractItem entity) throws SQLException {

        int i = 0;

        statement.setLong(++i, entity.getContractId());
        statement.setString(++i, entity.getItemName());
        statement.setBigDecimal(++i, entity.getItemCost());
        statement.setString(++i, entity.getResponsibilityCategory());
        statement.setString(++i, entity.getDistance());
        statement.setString(++i, entity.getDescription());

    }
}
