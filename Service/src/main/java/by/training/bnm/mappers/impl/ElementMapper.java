package by.training.bnm.mappers.impl;

import by.training.bnm.entity.hardware.elements.SimpleElement;
import by.training.bnm.mappers.IdentifiedRowMapper;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

public class ElementMapper implements IdentifiedRowMapper<SimpleElement> {
    @Override
    public SimpleElement map(ResultSet resultSet) throws SQLException {

        SimpleElement simpleElement = new SimpleElement();
        simpleElement.setId(resultSet.getLong("id"));
        simpleElement.setCircuitId(resultSet.getLong("circuit_id"));
        simpleElement.setSymbol(resultSet.getString("symbol"));
        simpleElement.setName(resultSet.getString("name"));
        simpleElement.setRegisterAddress(resultSet.getInt("register_address"));
        simpleElement.setTypeInt(resultSet.getInt("element_type"));
        simpleElement.setName(resultSet.getString("description"));

        return simpleElement;
    }

    @Override
    public List<String> getColumnNames() {

        return Arrays.asList("circuit_id", "symbol", "name", "register_address", "element_type", "description");
    }

    @Override
    public void populateStatement(PreparedStatement statement, SimpleElement entity) throws SQLException {

        int i = 0;
        statement.setLong(++i, entity.getCircuitId());
        statement.setString(++i, entity.getSymbol());
        statement.setString(++i, entity.getName());
        statement.setInt(++i, entity.getRegisterAddress());
        statement.setInt(++i, entity.getTypeInt());
        statement.setString(++i, entity.getDescription());
    }
}
