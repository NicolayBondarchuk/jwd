package by.training.bnm.mappers.impl;

import by.training.bnm.entity.user.UserRating;
import by.training.bnm.mappers.IdentifiedRowMapper;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

public class UserRatingMapper implements IdentifiedRowMapper<UserRating> {

    @Override
    public UserRating map(ResultSet resultSet) throws SQLException {

        UserRating userRating = new UserRating();
        userRating.setId(resultSet.getLong("id"));
        userRating.setUserId(resultSet.getLong("user_account_id"));
        userRating.setRatingType(resultSet.getInt("rating_type"));
        userRating.setStarsCounter(resultSet.getInt("stars_counter"));

        return userRating;
    }

    @Override
    public List<String> getColumnNames() {
        return Arrays.asList("user_account_id", "rating_type", "stars_counter");
    }

    @Override
    public void populateStatement(PreparedStatement statement, UserRating entity) throws SQLException {

        int i = 0;
        statement.setLong(++i, entity.getUserId());
        statement.setInt(++i, entity.getRatingTypeInt());
        statement.setInt(++i, entity.getStarsCounter());
    }
}
