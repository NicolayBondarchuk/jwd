package by.training.bnm.mappers.impl;

import by.training.bnm.entity.hardware.Circuit;
import by.training.bnm.mappers.IdentifiedRowMapper;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

public class CircuitMapper implements IdentifiedRowMapper<Circuit> {
    @Override
    public Circuit map(ResultSet resultSet) throws SQLException {

        Circuit circuit = new Circuit();
        circuit.setId(resultSet.getLong("id"));
        circuit.setSetPoint(resultSet.getDouble("set_point"));
        circuit.setAlarm(resultSet.getDouble("set_alarm"));
        circuit.setDescription(resultSet.getString("description"));
        circuit.setControllerId(resultSet.getLong("controller_id"));

        return circuit;
    }

    @Override
    public List<String> getColumnNames() {
        return Arrays.asList("set_point", "set_alarm", "description", "controller_id");
    }

    @Override
    public void populateStatement(PreparedStatement statement, Circuit entity) throws SQLException {

        int i = 0;
        statement.setDouble(++i, entity.getSetPoint());
        statement.setDouble(++i, entity.getAlarm());
        statement.setString(++i, entity.getDescription());
        statement.setLong(++i, entity.getControllerId());

    }
}
