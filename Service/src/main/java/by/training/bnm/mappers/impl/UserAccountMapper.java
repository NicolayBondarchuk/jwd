package by.training.bnm.mappers.impl;

import by.training.bnm.entity.user.UserAccount;
import by.training.bnm.mappers.IdentifiedRowMapper;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

public class UserAccountMapper implements IdentifiedRowMapper<UserAccount> {

    @Override
    public UserAccount map(ResultSet resultSet) throws SQLException {

        UserAccount userAccount = new UserAccount();
        userAccount.setId(resultSet.getLong("id"));
        userAccount.setUserName(resultSet.getString("user_name"));
        userAccount.setUserPassword(resultSet.getString("user_password"));
        userAccount.setUserEmail(resultSet.getString("user_email"));
        userAccount.setUserDescription(resultSet.getString("user_description"));

        return userAccount;
    }

    @Override
    public List<String> getColumnNames() {
        return Arrays.asList("user_name", "user_password", "user_email", "user_description");
    }

    @Override
    public void populateStatement(PreparedStatement statement, UserAccount entity) throws SQLException {
        int i = 0;
        statement.setString(++i, entity.getUserName());
        statement.setString(++i, entity.getUserPassword());
        statement.setString(++i, entity.getUserEmail());
        statement.setString(++i, entity.getUserDescription());

    }
}
