package by.training.bnm.mappers.impl;

import by.training.bnm.entity.contract.Contract;
import by.training.bnm.mappers.IdentifiedRowMapper;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

public class ContractMapper implements IdentifiedRowMapper<Contract> {

    @Override
    public Contract map(ResultSet resultSet) throws SQLException {

        Contract contract = new Contract();

        contract.setId(resultSet.getLong("id"));
        contract.setUserAccountId(resultSet.getLong("user_account_id"));
        contract.setTotalCost(resultSet.getBigDecimal("total_cost"));

        return contract;
    }

    @Override
    public List<String> getColumnNames() {
        return Arrays.asList("user_account_id", "total_cost");
    }

    @Override
    public void populateStatement(PreparedStatement statement, Contract entity) throws SQLException {

        int i = 0;
        statement.setLong(++i, entity.getUserAccountId());
        statement.setBigDecimal(++i, entity.getTotalCost());

    }
}
