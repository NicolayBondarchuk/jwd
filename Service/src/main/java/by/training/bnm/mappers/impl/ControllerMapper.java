package by.training.bnm.mappers.impl;

import by.training.bnm.entity.hardware.Controller;
import by.training.bnm.mappers.IdentifiedRowMapper;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

public class ControllerMapper implements IdentifiedRowMapper<Controller> {
    @Override
    public Controller map(ResultSet resultSet) throws SQLException {

        Controller controller = new Controller();
        controller.setId(resultSet.getLong("id"));
        controller.setDifficultyCategoryId(resultSet.getLong("difficulty_category_id"));
        controller.setControllerName(resultSet.getString("controller_name"));
        controller.setMacAddress(resultSet.getString("mac_address"));
        controller.setDescription(resultSet.getString("description"));

        return controller;
    }

    @Override
    public List<String> getColumnNames() {
        return Arrays.asList("difficulty_category_id", "controller_name", "mac_address", "description");
    }

    @Override
    public void populateStatement(PreparedStatement statement, Controller entity) throws SQLException {

        int i = 0;
        statement.setLong(++i, entity.getDifficultyCategoryId());
        statement.setString(++i, entity.getControllerName());
        statement.setString(++i, entity.getMacAddress());
        statement.setString(++i, entity.getDescription());

    }
}
