package by.training.bnm.mappers.impl;

import by.training.bnm.entity.contract.DifficultyCategory;
import by.training.bnm.mappers.IdentifiedRowMapper;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

public class DifficultyCategoryMapper implements IdentifiedRowMapper<DifficultyCategory> {
    @Override
    public DifficultyCategory map(ResultSet resultSet) throws SQLException {

        DifficultyCategory difficultyCategory = new DifficultyCategory();
        difficultyCategory.setId(resultSet.getLong("id"));
        difficultyCategory.setContractItemId(resultSet.getLong("contract_item_id"));
        difficultyCategory.setCategoryType(resultSet.getInt("category"));

        return difficultyCategory;
    }

    @Override
    public List<String> getColumnNames() {
        return Arrays.asList("contract_item_id", "category");
    }

    @Override
    public void populateStatement(PreparedStatement statement, DifficultyCategory entity) throws SQLException {

        int i = 0;
        statement.setLong(++i, entity.getContractItemId());
        statement.setInt(++i, entity.getCategoryTypeInt());
    }
}
