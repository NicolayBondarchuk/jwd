package by.training.bnm.service.impl;

import by.training.bnm.dao.DAOException;
import by.training.bnm.dao.ElementDao;
import by.training.bnm.entity.hardware.elements.ElementFactory;
import by.training.bnm.entity.hardware.elements.SimpleElement;
import by.training.bnm.service.ElementService;

import java.util.List;

public class ElementServiceImpl implements ElementService {


    private final ElementDao elementDao;

    public ElementServiceImpl(ElementDao elementDao) {
        this.elementDao = elementDao;
    }

    @Override
    public void create(SimpleElement element) throws DAOException {

        elementDao.create(element);

    }

    @Override
    public void create(Long circuitId, String symbol, String name, String description,
                       String registerAddress, String type) throws DAOException {

        SimpleElement simpleElement = ElementFactory.getElement(type);
        create(simpleElement);

    }

    @Override
    public void update(SimpleElement element) throws DAOException {

        elementDao.create(element);

    }

    @Override
    public void update(Long circuitId, String symbol, String name, String description,
                       String registerAddress, String type) throws DAOException {

        SimpleElement simpleElement = ElementFactory.getElement(type);
        update(simpleElement);

    }

    @Override
    public List<SimpleElement> findAllByCircuitId(Long circuitId) throws DAOException {
        return elementDao.findByCircuitId(circuitId);
    }

    @Override
    public void remove(SimpleElement entity) throws DAOException {

            elementDao.delete(entity);

    }

    @Override
    public void remove(Long id) throws DAOException {

            elementDao.delete(id);

    }

    @Override
    public SimpleElement getById(Long id) throws DAOException {

        SimpleElement element;
        element = elementDao.getById(id);

        return element;
    }

    @Override
    public List<SimpleElement> findAll() throws DAOException {

        return elementDao.findAll();
    }
}
