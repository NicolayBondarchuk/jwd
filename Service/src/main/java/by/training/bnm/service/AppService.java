package by.training.bnm.service;

import by.training.bnm.dao.DAOException;

import java.util.List;

/**
 *
 * @param <N> entity
 */

public interface AppService<N> {

    void create(N entity) throws DAOException;

    void update(N entity) throws DAOException;

    void remove(N entity) throws DAOException;

    void remove(Long id) throws DAOException;

    N getById(Long id) throws DAOException;

    List<N> findAll() throws DAOException;
}
