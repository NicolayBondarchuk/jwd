package by.training.bnm.service.impl;

import by.training.bnm.dao.ControllerDao;
import by.training.bnm.dao.DAOException;
import by.training.bnm.entity.hardware.Controller;
import by.training.bnm.service.ControllerService;

import java.util.List;

public class ControllerServiceImpl implements ControllerService {

    private final ControllerDao controllerDao;

    public ControllerServiceImpl(ControllerDao controllerDao) {
        this.controllerDao = controllerDao;
    }

    @Override
    public void create(Controller controller) throws DAOException {

        controllerDao.create(controller);
    }

    @Override
    public void update(Controller controller) throws DAOException {

        controllerDao.update(controller);
    }

    @Override
    public void remove(Controller entity) throws DAOException {

        controllerDao.delete(entity);
    }

    @Override
    public void remove(Long id) throws DAOException {

        controllerDao.delete(id);
    }

    @Override
    public Controller getById(Long id) throws DAOException {

        return controllerDao.getById(id);
    }

    @Override
    public List<Controller> findAll() throws DAOException {
        return controllerDao.findAll();
    }

    @Override
    public void create(Long contractItemId, String controllerName, String macAddress, String description) throws DAOException {

        Controller controller = new Controller(contractItemId, controllerName, macAddress, description);
        create(controller);

    }

    @Override
    public void update(Long controllerId, Long contractItemId, String controllerName, String macAddress,
                       String description) throws DAOException {

        Controller controller = new Controller(controllerId, contractItemId, controllerName, macAddress, description);
        update(controller);

    }
}
