package by.training.bnm.service;

import by.training.bnm.dao.DAOException;
import by.training.bnm.entity.hardware.Controller;


public interface ControllerService extends AppService<Controller> {

    void create(Long contractItemId, String controllerName, String macAddress, String description) throws DAOException;

    void update(Long controllerId, Long contractItemId, String controllerName, String macAddress,
                   String description) throws DAOException;

}
