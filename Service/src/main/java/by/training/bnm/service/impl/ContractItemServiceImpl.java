package by.training.bnm.service.impl;

import by.training.bnm.dao.ContractItemDao;
import by.training.bnm.dao.DAOException;
import by.training.bnm.entity.contract.ContractItem;
import by.training.bnm.entity.contract.Distance;
import by.training.bnm.entity.contract.ResponsibilityCategory;
import by.training.bnm.service.ContractItemService;

import java.util.List;

public class ContractItemServiceImpl implements ContractItemService {

    private final ContractItemDao contractItemDao;

    public ContractItemServiceImpl(ContractItemDao contractItemDao) {
        this.contractItemDao = contractItemDao;
    }

    @Override
    public void create(Long userId, String objectName, String distance, String responsibilityCategory,
                       String description) throws DAOException {

        ResponsibilityCategory respCategory = ResponsibilityCategory.of(responsibilityCategory)
                .orElse(ResponsibilityCategory.NO_CATEGORY);
        Distance dist = Distance.of(distance).orElse(Distance.SAME_CITY);
        ContractItem contractItem = new ContractItem(userId, objectName, respCategory, dist, description);

        contractItemDao.create(contractItem);
    }

    @Override
    public List<ContractItem> findByUserId(Long userId) throws DAOException {

        return contractItemDao.findByUserId(userId);
    }

    @Override
    public void create(ContractItem circuit) throws DAOException {

        contractItemDao.create(circuit);
    }

    @Override
    public void update(ContractItem circuit) throws DAOException {

        contractItemDao.update(circuit);
    }

    @Override
    public void remove(ContractItem entity) throws DAOException {

        contractItemDao.delete(entity);
    }

    @Override
    public void remove(Long id) throws DAOException {

        contractItemDao.delete(id);
    }

    @Override
    public ContractItem getById(Long id) throws DAOException {

        return contractItemDao.getById(id);
    }

    @Override
    public List<ContractItem> findAll() throws DAOException {

        return contractItemDao.findAll();
    }
}
