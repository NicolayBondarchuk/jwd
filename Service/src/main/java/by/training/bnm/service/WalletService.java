package by.training.bnm.service;

import by.training.bnm.dao.DAOException;
import by.training.bnm.entity.user.UserWallet;

public interface WalletService extends AppService<UserWallet> {

    void fillUp(Long money) throws DAOException;

    void pay(Long money) throws DAOException;
}
