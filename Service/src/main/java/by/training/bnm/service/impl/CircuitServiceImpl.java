package by.training.bnm.service.impl;

import by.training.bnm.dao.CircuitDao;
import by.training.bnm.dao.DAOException;
import by.training.bnm.entity.hardware.Circuit;
import by.training.bnm.service.CircuitService;

import java.util.List;

public class CircuitServiceImpl implements CircuitService {

    private final CircuitDao circuitDao;

    public CircuitServiceImpl(CircuitDao circuitDao) {
        this.circuitDao = circuitDao;
    }


    @Override
    public void create(Circuit circuit) throws DAOException {

        circuitDao.create(circuit);

    }

    @Override
    public void create(Long controllerId, String circuitName, Double setPoint, Double alarm,
                       String description) throws DAOException {

        Circuit circuit = new Circuit(null, controllerId, circuitName, setPoint, alarm, description);
        create(circuit);

    }

    @Override
    public void update(Circuit circuit) throws DAOException {

        circuitDao.update(circuit);

    }

    @Override
    public void remove(Circuit entity) throws DAOException {

        circuitDao.delete(entity);
    }

    @Override
    public void remove(Long id) throws DAOException {

        circuitDao.delete(id);

    }

    @Override
    public Circuit getById(Long id) throws DAOException {

        Circuit circuit;

        circuit = circuitDao.getById(id);

        return circuit;
    }

    @Override
    public List<Circuit> findAll() throws DAOException {

        return circuitDao.findAll();
    }

    @Override
    public void update(Long circuitId, Long controllerId, String circuitName, Double setPoint,
                       Double alarm, String description) throws DAOException {

        Circuit circuit = new Circuit(circuitId, controllerId, circuitName, setPoint, alarm, description);

        update(circuit);

    }

    @Override
    public List<Circuit> findByControllerId(Long controllerId) throws DAOException {
        return circuitDao.findByControllerId(controllerId);
    }
}
