package by.training.bnm.service;

import by.training.bnm.dao.DAOException;
import by.training.bnm.entity.hardware.elements.SimpleElement;

import java.util.List;


public interface ElementService extends AppService<SimpleElement> {

    void create(Long circuitId, String symbol, String name, String description, String registerAddress,
                String type) throws DAOException;

    void update(Long circuitId, String symbol, String name, String description, String registerAddress,
                String type) throws DAOException;

    List<SimpleElement> findAllByCircuitId(Long circuitId) throws DAOException;
}
