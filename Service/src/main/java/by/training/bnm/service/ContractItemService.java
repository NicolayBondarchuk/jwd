package by.training.bnm.service;

import by.training.bnm.dao.DAOException;
import by.training.bnm.entity.contract.ContractItem;
import by.training.bnm.entity.hardware.elements.ElementType;

import java.util.List;

public interface ContractItemService extends AppService<ContractItem> {

    void addElement(ElementType elementType) throws DAOException;

    void removeElement(ElementType elementType) throws DAOException;

    void create(Long userId, String objectName, String distance, String responsibilityCategory,
                String description) throws DAOException;

    List<ContractItem> findByUserId(Long userId) throws DAOException;
}
