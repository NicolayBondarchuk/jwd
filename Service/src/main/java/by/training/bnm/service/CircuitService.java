package by.training.bnm.service;

import by.training.bnm.dao.DAOException;
import by.training.bnm.entity.hardware.Circuit;

import java.util.List;


public interface CircuitService extends AppService<Circuit> {

    void create(Long controllerId, String circuitName, Double setPoint, Double alarm,
                String description) throws DAOException;

    void update(Long circuitId, Long controllerId, String circuitName, Double setPoint, Double alarm,
                String description) throws DAOException;

    List<Circuit> findByControllerId(Long controllerId) throws DAOException;
}
