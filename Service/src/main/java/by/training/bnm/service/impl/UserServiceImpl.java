package by.training.bnm.service.impl;

import by.training.bnm.dao.*;
import by.training.bnm.entity.user.UserAccount;
import by.training.bnm.service.UserService;

import java.util.List;

public class UserServiceImpl implements UserService {

    private final UserDao userDao;
    private final ContractDao contractDao;
    private final WalletDao walletDao;

    public UserServiceImpl(UserDao userDao, ContractDao contractDao, WalletDao walletDao) {
        this.userDao = userDao;
        this.contractDao = contractDao;
        this.walletDao = walletDao;
    }

    @Override
    public void create(UserAccount entity) throws DAOException {

        Long userId = userDao.create(entity);
        walletDao.createByUserId(userId);
        contractDao.createByUserId(userId);
    }

    @Override
    public void update(UserAccount entity) throws DAOException {

        userDao.update(entity);
    }

    @Override
    public void remove(UserAccount entity) throws DAOException {

        Long userId = entity.getId();
        userDao.delete(userId);
        walletDao.delete(userId);
        contractDao.delete(userId);
    }

    @Override
    public void remove(Long userId) throws DAOException {

        userDao.delete(userId);
        walletDao.delete(userId);
        contractDao.delete(userId);
    }

    @Override
    public UserAccount getById(Long id) throws DAOException {

        return userDao.getById(id);
    }

    @Override
    public List<UserAccount> findAll() throws DAOException {

        return userDao.findAll();
    }
}
