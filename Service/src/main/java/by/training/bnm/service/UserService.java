package by.training.bnm.service;

import by.training.bnm.dao.DAOException;
import by.training.bnm.entity.user.UserAccount;


public interface UserService extends AppService<UserAccount> {

    UserAccount getById(Long id) throws DAOException;

    void removeObject(Long id) throws DAOException;

}
