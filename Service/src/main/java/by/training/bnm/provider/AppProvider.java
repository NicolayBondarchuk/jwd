package by.training.bnm.provider;

/**
 * @param <K> key
 * @param <T> type of an item
 */

public interface AppProvider<K, T> {

    T getItem(String key);

    void registerItem(K key, T item);

    void removeItem(K key);
}
