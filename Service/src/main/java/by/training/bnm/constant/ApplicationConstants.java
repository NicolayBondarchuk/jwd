package by.training.bnm.constant;

public class ApplicationConstants {
    public static final String COMMAND_NAME_PARAM = "commandName";
    public static final String DATASOURCE_PROPERTIES = "datasource/datasource.properties";
    public static final String DATASOURCE_DRIVER = "datasource.driver";
    public static final String DATASOURCE_URL = "datasource.url";
    public static final String DATASOURCE_USER_ACCOUNT = "datasource.userAccount";
    public static final String DATASOURCE_PASSWORD = "datasource.password";
    public static final String DATASOURCE_POOL_SIZE = "datasource.poolSize";
}
