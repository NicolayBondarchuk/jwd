package by.training.bnm.controller;

import by.training.bnm.command.AppCommand;
import by.training.bnm.command.CommandProvider;
import by.training.bnm.command.CommandUtil;
import by.training.bnm.context.ApplicationContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns="/", name="index")
public class ServiceController extends HttpServlet {
    private static final Logger LOGGER = LogManager.getLogger(ServiceController.class);

    @Override
    public void init() throws ServletException {
        ApplicationContext.getInstance().initialize();
        LOGGER.info("service controller was initialized");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String attr = CommandUtil.getCommandFromRequest(req);

        AppCommand command = ApplicationContext.getInstance().getBean(CommandProvider.class).getItem(attr);
        String partName = command.apply(req, resp);
        if (partName.startsWith("redirect:")) {
            String redirect = partName.replace("redirect:", "");
            resp.sendRedirect(redirect);
        } else {
            req.setAttribute("partName", partName);
            req.getRequestDispatcher("/jsp/main_view.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

    @Override
    public void destroy() {
        ApplicationContext.getInstance().destroy();
        LOGGER.info("service controller was destroyed");
    }
}

