package by.training.bnm.builder;

import by.training.bnm.entity.user.UserAccount;
import by.training.bnm.entity.user.UserField;

import java.util.Map;

public class UserBuilder implements EntityBuilder<UserAccount> {
    @Override
    public UserAccount build(Map<String, String> data) {
        UserAccount userAccount = new UserAccount();
        for (UserField userField : UserField.values()) {
            userField.getFieldMapper().accept(userAccount, data.get(userField.getFieldName()));
        }
        return userAccount;
    }
}

