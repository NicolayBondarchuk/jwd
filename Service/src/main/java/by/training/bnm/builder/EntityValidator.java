package by.training.bnm.builder;

import java.util.Map;

public interface EntityValidator {
    ValidationResult validate(Map<String, String> data);
}
