package by.training.bnm.builder;

import java.util.Map;

public interface EntityBuilder<T> {

    T build(Map<String, String> data);
}
