package by.training.bnm.context;

import by.training.bnm.command.CommandProvider;
import by.training.bnm.command.CommandType;
import by.training.bnm.command.ServiceCommandProvider;
import by.training.bnm.command.impl.*;
import by.training.bnm.dao.*;
import by.training.bnm.dao.impl.*;
import by.training.bnm.service.*;
import by.training.bnm.service.impl.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

public class ApplicationContext {
    private static final Logger LOGGER = LogManager.getLogger(ApplicationContext.class);

    private static ApplicationContext INSTANCE;

    private ApplicationContext() {}

    private final Map<Class<?>, Object> beans = new HashMap<>();

    public static ApplicationContext getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ApplicationContext();

            LOGGER.info("context was create");

        }
        return INSTANCE;
    }

    public void initialize() {

    // dao
        UserDao userDao = new UserAccountDaoImpl();
        ContractItemDao contractItemDao = new ContractItemDaoImpl(genericDao);
        ControllerDao controllerDao = new ControllerDaoImpl();
        CircuitDao circuitDao = new CircuitDaoImpl(genericDao);
        ElementDao elementDao = new ElementDaoImpl();

    // services
        UserService userService = new UserServiceImpl(userDao, contractDao, walletDao, userRating);
        ContractItemService contractItemService = new ContractItemServiceImpl(contractItemDao);
        ControllerService controllerService = new ControllerServiceImpl(controllerDao);
        CircuitService circuitService = new CircuitServiceImpl(circuitDao);
        ElementService elementService = new ElementServiceImpl(elementDao);


    // command provider
        CommandProvider commandProvider = new ServiceCommandProvider();
        commandProvider.registerItem(CommandType.INDEX_PAGE, ((request, response) -> "index_page"));
        commandProvider.registerItem(CommandType.LOGIN_PAGE, ((request, response) -> "login_page"));
        commandProvider.registerItem(CommandType.SHOW_USER, ((request, response) -> "user_page"));
        commandProvider.registerItem(CommandType.REGISTER_PAGE, ((request, response) -> "register_page"));
        commandProvider.registerItem(CommandType.REGISTER_USER, new RegisterUserCommand(userService));
        commandProvider.registerItem(CommandType.EDIT_USER_PAGE, new EditUserPageCommand(userService));
        commandProvider.registerItem(CommandType.EDIT_USER, new EditUserCommand(userService));
        commandProvider.registerItem(CommandType.ADD_OBJECT_PAGE, new AddObjectPageCommand());
        commandProvider.registerItem(CommandType.ADD_OBJECT, new AddObjectCommand(contractItemService));
        commandProvider.registerItem(CommandType.EDIT_OBJECT_PAGE, new EditObjectPageCommand(contractItemService));
        commandProvider.registerItem(CommandType.EDIT_OBJECT, new EditObjectCommand(contractItemService));
        commandProvider.registerItem(CommandType.DELETE_OBJECT, new DeleteObjectCommand(contractItemService));
        commandProvider.registerItem(CommandType.ADD_CONTROLLER, new AddControllerCommand(controllerService));
        commandProvider.registerItem(CommandType.ADD_CONTROLLER_PAGE, new AddControllerPageCommand());
        commandProvider.registerItem(CommandType.EDIT_CONTROLLER, new EditControllerCommand(controllerService));
        commandProvider.registerItem(CommandType.EDIT_CONTROLLER_PAGE, new EditControllerPageCommand(controllerService));
        commandProvider.registerItem(CommandType.DELETE_CONTROLLER, new DeleteControllerCommand(controllerService));
        commandProvider.registerItem(CommandType.ADD_CIRCUIT_PAGE, new AddCircuitPageCommand());
        commandProvider.registerItem(CommandType.ADD_CIRCUIT, new AddCircuitCommand(circuitService));
        commandProvider.registerItem(CommandType.EDIT_CIRCUIT_PAGE, new EditCircuitPageCommand(circuitService));
        commandProvider.registerItem(CommandType.EDIT_CIRCUIT, new EditCircuitCommand(circuitService));
        commandProvider.registerItem(CommandType.DELETE_CIRCUIT, new DeleteCircuitCommand(circuitService));
        commandProvider.registerItem(CommandType.ADD_ELEMENT_PAGE, new AddElementPageCommand());
        commandProvider.registerItem(CommandType.ADD_ELEMENT, new AddElementCommand(elementService));
        commandProvider.registerItem(CommandType.EDIT_ELEMENT_PAGE, new EditElementPageCommand(elementService));
        commandProvider.registerItem(CommandType.EDIT_ELEMENT, new EditElementCommand(elementService));



        beans.put(CommandProvider.class, commandProvider);

        LOGGER.info("Application context was created");
    }

    public void destroy() {
        beans.clear();
        LOGGER.info("Application context was destroyed");
    }

    public <T> T getBean(Class<T> clazz) {
        return (T) this.beans.get(clazz);
    }
}
