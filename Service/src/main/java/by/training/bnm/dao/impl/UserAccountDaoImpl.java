package by.training.bnm.dao.impl;

import by.training.bnm.connection.ConnectionPool;
import by.training.bnm.dao.DAOException;
import by.training.bnm.dao.GenericDao;
import by.training.bnm.dao.UserDao;
import by.training.bnm.entity.user.UserAccount;
import by.training.bnm.mappers.IdentifiedRowMapper;

import java.util.List;

public class UserAccountDaoImpl implements UserDao {

    private static final String TABLE_NAME = "user_account";
    private final GenericDao<UserAccount> genericDao;

    public UserAccountDaoImpl(ConnectionPool connectionPool, IdentifiedRowMapper<UserAccount> rowMapper) {
        this.genericDao = new GenericDao<>(TABLE_NAME, connectionPool, rowMapper);
    }


    @Override
    public Long create(UserAccount entity) throws DAOException {
        return genericDao.create(entity);
    }

    @Override
    public void update(UserAccount entity) throws DAOException {
        genericDao.update(entity);
    }

    @Override
    public UserAccount getById(Long id) throws DAOException {
        return genericDao.getById(id);
    }

    @Override
    public List<UserAccount> findAll() throws DAOException {
        return genericDao.findAll();
    }

    @Override
    public void delete(Long id) throws DAOException {
        genericDao.delete(id);
    }
}
