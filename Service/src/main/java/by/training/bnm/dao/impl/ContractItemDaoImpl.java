package by.training.bnm.dao.impl;

import by.training.bnm.connection.ConnectionException;
import by.training.bnm.connection.ConnectionManager;
import by.training.bnm.connection.ConnectionPool;
import by.training.bnm.dao.ContractItemDao;
import by.training.bnm.dao.DAOException;
import by.training.bnm.dao.GenericDao;
import by.training.bnm.entity.contract.ContractItem;
import by.training.bnm.mappers.IdentifiedRowMapper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;


public class ContractItemDaoImpl implements ContractItemDao {

    private static final String TABLE_NAME = "contract_item";
    private static final String FOREIGN_KEY = "contract_id";
    private final GenericDao<ContractItem> genericDao;
    private final ConnectionPool connectionPool;

    public ContractItemDaoImpl(ConnectionPool connectionPool, IdentifiedRowMapper<ContractItem> rowMapper) {
        this.connectionPool = connectionPool;
        this.genericDao = new GenericDao<>(TABLE_NAME, connectionPool, rowMapper);
    }

    @Override
    public List<ContractItem> findByUserId(Long userId) throws DAOException {

        Long contractId = getContractId(userId);
        return genericDao.findAllByForeignId(FOREIGN_KEY, contractId);
    }

    @Override
    public Long create(ContractItem entity) throws DAOException {
        return genericDao.create(entity);
    }

    @Override
    public void update(ContractItem entity) throws DAOException {
        genericDao.update(entity);
    }

    @Override
    public ContractItem getById(Long id) throws DAOException {
        return genericDao.getById(id);
    }

    @Override
    public List<ContractItem> findAll() throws DAOException {
        return genericDao.findAll();
    }

    @Override
    public void delete(Long id) throws DAOException {
        genericDao.delete(id);
    }

    private Long getContractId(Long userId) throws DAOException {
        Long id = null;
        String query = "SELECT id FROM contract WHERE user_account_id=" + userId;
        try (Connection connection = ConnectionManager.getInstance(connectionPool).getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.executeUpdate();
            ResultSet generatedKeys = statement.getGeneratedKeys();
            id = generatedKeys.getLong(1);
        } catch (ConnectionException | SQLException e) {
            throw new DAOException(e);
        }
        return id;
    }

}
