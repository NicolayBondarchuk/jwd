package by.training.bnm.dao;

import by.training.bnm.entity.hardware.elements.SimpleElement;

import java.util.List;

public interface ElementDao extends CRUDDao<SimpleElement> {
    List<SimpleElement> findByCircuitId(Long circuitId) throws DAOException;
}
