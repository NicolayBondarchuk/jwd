package by.training.bnm.dao.impl;

import by.training.bnm.connection.ConnectionPool;
import by.training.bnm.dao.CircuitDao;
import by.training.bnm.dao.DAOException;
import by.training.bnm.dao.GenericDao;
import by.training.bnm.entity.hardware.Circuit;
import by.training.bnm.mappers.IdentifiedRowMapper;

import java.util.List;

public class CircuitDaoImpl implements CircuitDao {

    private static final String TABLE_NAME = "circuit";
    private static final String FOREIGN_KEY = "controller_id";
    private final GenericDao<Circuit> genericDao;

    public CircuitDaoImpl(ConnectionPool connectionPool, IdentifiedRowMapper<Circuit> rowMapper) {
        this.genericDao = new GenericDao<>(TABLE_NAME, connectionPool, rowMapper);
    }


    @Override
    public List<Circuit> findByControllerId(Long controllerId) throws DAOException {
        return genericDao.findAllByForeignId(FOREIGN_KEY, controllerId);
    }

    @Override
    public Long create(Circuit entity) throws DAOException {
        return genericDao.create(entity);
    }

    @Override
    public void update(Circuit entity) throws DAOException {
        genericDao.update(entity);
    }

    @Override
    public Circuit getById(Long id) throws DAOException {
        return genericDao.getById(id);
    }

    @Override
    public List<Circuit> findAll() throws DAOException {
        return genericDao.findAll();
    }

    @Override
    public void delete(Long id) throws DAOException {
        genericDao.delete(id);
    }
}
