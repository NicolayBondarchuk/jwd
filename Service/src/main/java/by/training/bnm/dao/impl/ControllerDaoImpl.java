package by.training.bnm.dao.impl;

import by.training.bnm.connection.ConnectionPool;
import by.training.bnm.dao.ControllerDao;
import by.training.bnm.dao.DAOException;
import by.training.bnm.dao.GenericDao;
import by.training.bnm.entity.hardware.Controller;
import by.training.bnm.mappers.IdentifiedRowMapper;

import java.util.List;

public class ControllerDaoImpl implements ControllerDao {

    private static final String TABLE_NAME = "controller";
    private final GenericDao<Controller> genericDao;

    public ControllerDaoImpl(ConnectionPool connectionPool, IdentifiedRowMapper<Controller> rowMapper) {
        this.genericDao = new GenericDao<>(TABLE_NAME, connectionPool, rowMapper);
    }

    @Override
    public Long create(Controller entity) throws DAOException {
        return genericDao.create(entity);
    }

    @Override
    public void update(Controller entity) throws DAOException {
        genericDao.update(entity);
    }

    @Override
    public Controller getById(Long id) throws DAOException {
        return genericDao.getById(id);
    }

    @Override
    public List<Controller> findAll() throws DAOException {
        return genericDao.findAll();
    }

    @Override
    public void delete(Long id) throws DAOException {
        genericDao.delete(id);
    }
}
