package by.training.bnm.dao.impl;

import by.training.bnm.connection.ConnectionPool;
import by.training.bnm.dao.DAOException;
import by.training.bnm.dao.GenericDao;
import by.training.bnm.dao.UserDiscountDao;
import by.training.bnm.entity.user.UserDiscountType;
import by.training.bnm.mappers.IdentifiedRowMapper;

import java.util.List;

public class UserDiscountDaoImpl implements UserDiscountDao {

    private static final String TABLE_NAME = "user_discount";
    private final GenericDao<UserDiscountType> genericDao;

    public UserDiscountDaoImpl(ConnectionPool connectionPool, IdentifiedRowMapper<UserDiscountType> rowMapper) {
        this.genericDao = new GenericDao<>(TABLE_NAME, connectionPool, rowMapper);
    }

    @Override
    public Long create(UserDiscountType entity) throws DAOException {
        return genericDao.create(entity);
    }

    @Override
    public void update(UserDiscountType entity) throws DAOException {
        genericDao.update(entity);
    }

    @Override
    public UserDiscountType getById(Long id) throws DAOException {
        return genericDao.getById(id);
    }

    @Override
    public List<UserDiscountType> findAll() throws DAOException {
        return genericDao.findAll();
    }

    @Override
    public void delete(Long id) throws DAOException {
        genericDao.delete(id);
    }
}
