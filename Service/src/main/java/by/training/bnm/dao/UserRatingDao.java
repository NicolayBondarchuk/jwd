package by.training.bnm.dao;

import by.training.bnm.entity.user.UserRating;

public interface UserRatingDao extends CRUDDao<UserRating> {

}
