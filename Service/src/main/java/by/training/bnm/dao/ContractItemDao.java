package by.training.bnm.dao;

import by.training.bnm.entity.contract.ContractItem;

import java.util.List;

public interface ContractItemDao extends CRUDDao<ContractItem>{
    List<ContractItem> findByUserId(Long userId) throws DAOException;
}
