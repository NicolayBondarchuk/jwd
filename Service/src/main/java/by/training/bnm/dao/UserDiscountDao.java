package by.training.bnm.dao;

import by.training.bnm.entity.user.UserDiscountType;

public interface UserDiscountDao extends CRUDDao<UserDiscountType> {

}
