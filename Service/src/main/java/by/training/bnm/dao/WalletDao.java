package by.training.bnm.dao;

import by.training.bnm.entity.user.UserWallet;

public interface WalletDao extends CRUDDao<UserWallet> {

    void createByUserId(Long userId) throws DAOException;
}
