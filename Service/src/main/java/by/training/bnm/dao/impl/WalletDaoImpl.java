package by.training.bnm.dao.impl;

import by.training.bnm.connection.ConnectionPool;
import by.training.bnm.dao.DAOException;
import by.training.bnm.dao.GenericDao;
import by.training.bnm.dao.WalletDao;
import by.training.bnm.entity.user.UserWallet;
import by.training.bnm.mappers.IdentifiedRowMapper;

import java.util.List;

public class WalletDaoImpl implements WalletDao {

    private static final String TABLE_NAME = "user_wallet";
    private final GenericDao<UserWallet> genericDao;

    public WalletDaoImpl(ConnectionPool connectionPool, IdentifiedRowMapper<UserWallet> rowMapper) {

        this.genericDao = new GenericDao<>(TABLE_NAME, connectionPool, rowMapper);
    }

    @Override
    public void createByUserId(Long userId) throws DAOException {

        UserWallet userWallet = new UserWallet();
        userWallet.setUserId(userId);
        genericDao.create(userWallet);
    }

    @Override
    public Long create(UserWallet entity) throws DAOException {

        return genericDao.create(entity);
    }

    @Override
    public void update(UserWallet entity) throws DAOException {
        genericDao.update(entity);
    }

    @Override
    public UserWallet getById(Long id) throws DAOException {
        return genericDao.getById(id);
    }

    @Override
    public List<UserWallet> findAll() throws DAOException {
        return genericDao.findAll();
    }

    @Override
    public void delete(Long id) throws DAOException {
        genericDao.delete(id);
    }
}

