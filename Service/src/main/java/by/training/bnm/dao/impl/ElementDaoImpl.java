package by.training.bnm.dao.impl;

import by.training.bnm.connection.ConnectionPool;
import by.training.bnm.dao.DAOException;
import by.training.bnm.dao.ElementDao;
import by.training.bnm.dao.GenericDao;
import by.training.bnm.entity.hardware.elements.SimpleElement;
import by.training.bnm.mappers.IdentifiedRowMapper;

import java.util.List;

public class ElementDaoImpl implements ElementDao {

    private static final String TABLE_NAME = "simple_element";
    private static final String FOREIGN_KEY = "circuit_id";
    private final GenericDao<SimpleElement> genericDao;

    public ElementDaoImpl(ConnectionPool connectionPool, IdentifiedRowMapper<SimpleElement> rowMapper) {
        this.genericDao = new GenericDao<>(TABLE_NAME, connectionPool, rowMapper);
    }

    @Override
    public List<SimpleElement> findByCircuitId(Long circuitId) throws DAOException {
        return genericDao.findAllByForeignId(FOREIGN_KEY, circuitId);
    }

    @Override
    public Long create(SimpleElement entity) throws DAOException {
        return genericDao.create(entity);
    }

    @Override
    public void update(SimpleElement entity) throws DAOException {
        genericDao.update(entity);
    }

    @Override
    public SimpleElement getById(Long id) throws DAOException {
        return genericDao.getById(id);
    }

    @Override
    public List<SimpleElement> findAll() throws DAOException {
        return genericDao.findAll();
    }

    @Override
    public void delete(Long id) throws DAOException {
        genericDao.delete(id);
    }
}
