package by.training.bnm.dao;

import by.training.bnm.connection.ConnectionException;
import by.training.bnm.connection.ConnectionManager;
import by.training.bnm.connection.ConnectionPool;
import by.training.bnm.mappers.IdentifiedRow;
import by.training.bnm.mappers.IdentifiedRowMapper;

import java.sql.*;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class GenericDao<N extends IdentifiedRow> {

    private static final String SELECT_ALL_QUERY = "SELECT {0} FROM {1}";
    private static final String SELECT_BY_ID_QUERY = "SELECT {0} FROM {1} WHERE id=?";
    private static final String SELECT_BY_FOREIGN_ID_QUERY = "SELECT {0} FROM {1} WHERE {2}=?";
    private static final String INSERT_QUERY = "INSERT INTO {0} ({1}) VALUES ({2})";
    private static final String UPDATE_QUERY = "UPDATE {0} SET {1} WHERE id=?";
    private static final String DELETE_QUERY = "DELETE FROM {0} WHERE id=?";

    private final String tableName;
    private final ConnectionPool connectionPool;
    private final IdentifiedRowMapper<N> rowMapper;

    public GenericDao(String tableName, ConnectionPool connectionPool, IdentifiedRowMapper<N> rowMapper) {
        this.tableName = tableName;
        this.connectionPool = connectionPool;
        this.rowMapper = rowMapper;
    }

    public Long create(N entity) throws DAOException {

        Long id = -1L;
        List<String> columnNames = rowMapper.getColumnNames();
        String columns = String.join(", ", columnNames);
        String wildcards = columnNames.stream().map(column -> "?").collect(Collectors.joining(", "));

        String sqlQuery = MessageFormat.format(INSERT_QUERY, tableName, columns, wildcards);

        try (Connection connection = ConnectionManager.getInstance(connectionPool).getConnection();
             PreparedStatement statement = connection.prepareStatement(sqlQuery,
                     Statement.RETURN_GENERATED_KEYS)) {
            rowMapper.populateStatement(statement, entity);
            statement.executeUpdate();
            ResultSet generatedKeys = statement.getGeneratedKeys();
            id = generatedKeys.getLong(1);
            generatedKeys.close();
        } catch (ConnectionException | SQLException e) {
            throw new DAOException(e);
        }
        return id;
    }

    public void update(N entity) throws DAOException {

        List<String> columnNames = rowMapper.getColumnNames();
        String columns = columnNames.stream().map(column -> column + "=?").collect(Collectors.joining(", "));

        String sqlQuery = MessageFormat.format(UPDATE_QUERY, tableName, columns);
        try (Connection connection = ConnectionManager.getInstance(connectionPool).getConnection();
             PreparedStatement statement = connection.prepareStatement(sqlQuery)) {
            rowMapper.populateStatement(statement, entity);
            statement.setLong(columnNames.size() + 1, entity.getId());
            statement.executeUpdate();
        } catch (ConnectionException | SQLException e) {
            throw new DAOException(e);
        }
    }

    public N getById(Long id) throws DAOException {

        N result = null;

        List<String> columnNames = rowMapper.getColumnNames();
        String columns = String.join(", ", columnNames);
        String sqlQuery = MessageFormat.format(SELECT_BY_ID_QUERY, columns, tableName);
        try (Connection connection = ConnectionManager.getInstance(connectionPool).getConnection();
             PreparedStatement statement = connection.prepareStatement(sqlQuery)) {
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                result = rowMapper.map(resultSet);
                result.setId(resultSet.getLong(1));
            }
            resultSet.close();
        } catch (ConnectionException | SQLException e) {
            throw new DAOException(e);
        }

        return result;
    }

    public List<N> findAllByForeignId(String keyName, Long id) throws DAOException {

        List<N> result = new LinkedList<>();

        List<String> columnNames = rowMapper.getColumnNames();
        String columns = String.join(", ", columnNames);
        String sqlQuery = MessageFormat.format(SELECT_BY_FOREIGN_ID_QUERY, columns, tableName, keyName);
        try (Connection connection = ConnectionManager.getInstance(connectionPool).getConnection();
             PreparedStatement statement = connection.prepareStatement(sqlQuery)) {
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                N map = rowMapper.map(resultSet);
                map.setId(resultSet.getLong(1));
                result.add(map);
            }
            resultSet.close();
        } catch (ConnectionException | SQLException e) {
            throw new DAOException(e);
        }
        return result;
    }

    public List<N> findAll() throws DAOException {

        List<N> objects = new ArrayList<>();

        List<String> columnNames = rowMapper.getColumnNames();
        String columns = String.join(", ", columnNames);
        String sqlQuery = MessageFormat.format(SELECT_ALL_QUERY, columns, tableName);

        try (Connection connection = ConnectionManager.getInstance(connectionPool).getConnection();
             PreparedStatement statement = connection.prepareStatement(sqlQuery)) {
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                N map = rowMapper.map(resultSet);
                map.setId(resultSet.getLong(1));
                objects.add(map);
            }
            resultSet.close();
        } catch (ConnectionException | SQLException e) {
            throw new DAOException(e);
        }

        return objects;
    }

    public void delete(Long id) throws DAOException {

        String sqlQuery = MessageFormat.format(DELETE_QUERY, tableName);
        try (Connection connection = ConnectionManager.getInstance(connectionPool).getConnection();
             PreparedStatement statement = connection.prepareStatement(sqlQuery)) {

            statement.setLong(1, id);
            statement.executeUpdate();

        } catch (ConnectionException | SQLException e) {
            throw new DAOException(e);
        }
    }
}
