package by.training.bnm.dao;

import by.training.bnm.entity.contract.Contract;

import java.util.List;

public interface ContractDao extends CRUDDao<Contract> {

    List<Contract> findByUserId(Long userId) throws DAOException;

    Long createByUserId(Long userId) throws DAOException;
}
