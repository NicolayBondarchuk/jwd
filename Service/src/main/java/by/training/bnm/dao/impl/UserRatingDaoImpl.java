package by.training.bnm.dao.impl;

import by.training.bnm.connection.ConnectionPool;
import by.training.bnm.dao.DAOException;
import by.training.bnm.dao.GenericDao;
import by.training.bnm.dao.UserRatingDao;
import by.training.bnm.entity.user.UserRating;
import by.training.bnm.mappers.IdentifiedRowMapper;

import java.util.List;

public class UserRatingDaoImpl implements UserRatingDao {


    private static final String TABLE_NAME = "user_rating";
    private final GenericDao<UserRating> genericDao;

    public UserRatingDaoImpl(ConnectionPool connectionPool, IdentifiedRowMapper<UserRating> rowMapper) {
        this.genericDao = new GenericDao<>(TABLE_NAME, connectionPool, rowMapper);
    }

    @Override
    public Long create(UserRating entity) throws DAOException {
        return genericDao.create(entity);
    }

    @Override
    public void update(UserRating entity) throws DAOException {
        genericDao.update(entity);
    }

    @Override
    public UserRating getById(Long id) throws DAOException {
        return genericDao.getById(id);
    }

    @Override
    public List<UserRating> findAll() throws DAOException {
        return genericDao.findAll();
    }

    @Override
    public void delete(Long id) throws DAOException {
        genericDao.delete(id);
    }
}
