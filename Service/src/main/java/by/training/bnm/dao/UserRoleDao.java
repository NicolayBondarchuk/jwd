package by.training.bnm.dao;

import by.training.bnm.entity.user.UserRoleType;

public interface UserRoleDao extends CRUDDao<UserRoleType> {
}
