package by.training.bnm.dao;

import by.training.bnm.entity.user.UserAccount;

public interface UserDao extends CRUDDao<UserAccount> {

}
