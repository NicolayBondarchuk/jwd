package by.training.bnm.dao;

import by.training.bnm.mappers.IdentifiedRow;

import java.util.List;

/**
 *
 * @param <N> entity
 */

public interface CRUDDao<N extends IdentifiedRow> {

    Long create(N entity) throws DAOException;

    void update(N entity) throws DAOException;

    N getById(Long id) throws DAOException;

    List<N> findAll() throws DAOException;

    void delete(Long id) throws DAOException;
}
