package by.training.bnm.dao;

import by.training.bnm.entity.contract.DifficultyCategory;

public interface DifficultyCategoryDao extends CRUDDao<DifficultyCategory> {
}
