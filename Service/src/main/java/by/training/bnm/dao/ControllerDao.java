package by.training.bnm.dao;

import by.training.bnm.entity.hardware.Controller;

public interface ControllerDao extends CRUDDao<Controller> {
}
