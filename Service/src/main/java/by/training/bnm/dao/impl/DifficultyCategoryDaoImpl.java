package by.training.bnm.dao.impl;

import by.training.bnm.connection.ConnectionPool;
import by.training.bnm.dao.DAOException;
import by.training.bnm.dao.DifficultyCategoryDao;
import by.training.bnm.dao.GenericDao;
import by.training.bnm.entity.contract.DifficultyCategory;
import by.training.bnm.entity.hardware.Controller;
import by.training.bnm.mappers.IdentifiedRowMapper;

import java.util.List;

public class DifficultyCategoryDaoImpl implements DifficultyCategoryDao {

    private static final String TABLE_NAME = "difficulty_category";
    private final GenericDao<DifficultyCategory> genericDao;

    public DifficultyCategoryDaoImpl(ConnectionPool connectionPool, IdentifiedRowMapper<DifficultyCategory> rowMapper) {
        this.genericDao = new GenericDao<>(TABLE_NAME, connectionPool, rowMapper);
    }

    @Override
    public Long create(DifficultyCategory entity) throws DAOException {
        return genericDao.create(entity);
    }

    @Override
    public void update(DifficultyCategory entity) throws DAOException {
        genericDao.update(entity);
    }

    @Override
    public DifficultyCategory getById(Long id) throws DAOException {
        return genericDao.getById(id);
    }

    @Override
    public List<DifficultyCategory> findAll() throws DAOException {
        return genericDao.findAll();
    }

    @Override
    public void delete(Long id) throws DAOException {
        genericDao.delete(id);
    }
}
