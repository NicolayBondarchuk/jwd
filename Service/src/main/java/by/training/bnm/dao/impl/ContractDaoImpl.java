package by.training.bnm.dao.impl;

import by.training.bnm.connection.ConnectionPool;
import by.training.bnm.dao.ContractDao;
import by.training.bnm.dao.DAOException;
import by.training.bnm.dao.GenericDao;
import by.training.bnm.entity.contract.Contract;
import by.training.bnm.mappers.IdentifiedRowMapper;

import java.util.List;

public class ContractDaoImpl implements ContractDao {

    private static final String TABLE_NAME = "contract";
    private static final String FOREIGN_KEY = "user_account_id";
    private final GenericDao<Contract> genericDao;

    public ContractDaoImpl(ConnectionPool connectionPool, IdentifiedRowMapper<Contract> rowMapper) {
        this.genericDao = new GenericDao<>(TABLE_NAME, connectionPool, rowMapper);
    }

    @Override
    public Long create(Contract entity) throws DAOException {
        return genericDao.create(entity);
    }

    @Override
    public void update(Contract entity) throws DAOException {
        genericDao.update(entity);
    }

    @Override
    public Contract getById(Long id) throws DAOException {
        return genericDao.getById(id);
    }

    @Override
    public List<Contract> findAll() throws DAOException {
        return genericDao.findAll();
    }

    @Override
    public void delete(Long id) throws DAOException {
        genericDao.delete(id);
    }

    @Override
    public List<Contract> findByUserId(Long userId) throws DAOException {
        return genericDao.findAllByForeignId(FOREIGN_KEY, userId);
    }

    @Override
    public Long createByUserId(Long userId) throws DAOException {

        Contract contract = new Contract();
        contract.setId(userId);
        return genericDao.create(contract);
    }
}
