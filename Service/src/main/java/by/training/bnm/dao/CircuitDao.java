package by.training.bnm.dao;

import by.training.bnm.entity.hardware.Circuit;

import java.util.List;

public interface CircuitDao extends CRUDDao<Circuit> {
    List<Circuit> findByControllerId(Long controllerId) throws DAOException;
}
