package by.training.bnm.dao.impl;

import by.training.bnm.connection.ConnectionPool;
import by.training.bnm.dao.DAOException;
import by.training.bnm.dao.GenericDao;
import by.training.bnm.dao.UserRoleDao;
import by.training.bnm.entity.user.UserRoleType;
import by.training.bnm.mappers.IdentifiedRowMapper;

import java.util.List;

public class UserRoleDaoImpl implements UserRoleDao {


    private static final String TABLE_NAME = "user_rating";
    private final GenericDao<UserRoleType> genericDao;

    public UserRoleDaoImpl(ConnectionPool connectionPool, IdentifiedRowMapper<UserRoleType> rowMapper) {
        this.genericDao = new GenericDao<>(TABLE_NAME, connectionPool, rowMapper);
    }

    @Override
    public Long create(UserRoleType entity) throws DAOException {
        return genericDao.create(entity);
    }

    @Override
    public void update(UserRoleType entity) throws DAOException {
        genericDao.update(entity);
    }

    @Override
    public UserRoleType getById(Long id) throws DAOException {
        return genericDao.getById(id);
    }

    @Override
    public List<UserRoleType> findAll() throws DAOException {
        return genericDao.findAll();
    }

    @Override
    public void delete(Long id) throws DAOException {
        genericDao.delete(id);
    }
}
